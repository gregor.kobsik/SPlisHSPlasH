#include "FoamManager.h"

#include "FoamFactory.h"
#include "FoamFluidModel.h"
#include "Postprocessing\Foam\Kobsik18\FoamFluidModel_Kobsik18.h"

#include "Abstract\FoamBehaviourController.h"
#include "Abstract\FoamParameters.h"
#include "Abstract\FoamParticleCreator.h"

#include "Utilities/PartioReaderWriter.h"
#include "Utilities/FileSystem.h"

#define EXPORT_FLUID true

FoamManager::FoamManager(unsigned int p_method, Real p_fluidRadius)
{
	this->setFoamCreationMethod(p_method);
	this->init(p_fluidRadius);
}

FoamManager::~FoamManager() 
{

}

void FoamManager::processNextTimestep() 
{
	this->performNeighbourhoodSearch();
	this->m_foamBehaviourController->progress(this->m_foamFluidModel.get(), this->m_foamParameters.get());
	this->m_foamParticleCreator->createParticles(this->m_foamFluidModel.get(), this->m_foamParameters.get());
	this->m_currentParticleSet++;
}

void FoamManager::reset() 
{
	init(this->m_foamFluidModel->getParticleRadius());
}

void FoamManager::exportParticleSet()
{
	bool creationMethodAbleToCreateMist = false;

	unsigned int fluidNumber = this->m_foamFluidModel->numActiveParticles();
	unsigned int foamNumber = this->m_foamFluidModel->numFoamParticles();
	unsigned int mistNumber = 0;

	Real fluidRadius = this->m_foamFluidModel->getParticleRadius();
	Real foamRadius = this->m_foamFluidModel->getFoamRadius();
	Real mistRadius = 0;

	switch (this->getFoamCreationMethode())
	{
	case FoamCreationMethod::Ihmsen12:
		creationMethodAbleToCreateMist = false;
		break;
	case FoamCreationMethod::Kobsik18:
		creationMethodAbleToCreateMist = true;
		FoamFluidModel_Kobsik18* model_Kobsik18 = reinterpret_cast<FoamFluidModel_Kobsik18*>(this->m_foamFluidModel.get());
		mistNumber = model_Kobsik18->numMistParticles();
		mistRadius = model_Kobsik18->getMistRadius();
		break;
	}

	if (this->getExportMist() && !creationMethodAbleToCreateMist)
		std::cout << "Foam creation method not able to create mist." << std::endl;

	#if EXPORT_FLUID
		
		std::vector<unsigned int> fluidIdentifier(fluidNumber);
		std::vector<SPH::Vector3r> fluidPosition(fluidNumber);
		/*
		std::vector<SPH::Vector3r> fluidVelocity(fluidNumber);
		std::vector<SPH::Vector3r> fluidRelativeVelocity(fluidNumber);
		std::vector<SPH::Vector3r> fluidScaledVelocity(fluidNumber);

		std::vector<SPH::Vector3r> fluidAcceleration(fluidNumber);
		std::vector<SPH::Vector3r> fluidRelativeAcceleration(fluidNumber);
		std::vector<SPH::Vector3r> fluidScaledAcceleration(fluidNumber);

		std::vector<SPH::Vector3r> fluidVelocitySqr(fluidNumber);
		std::vector<SPH::Vector3r> fluidRelativeVelocitySqr(fluidNumber);
		std::vector<SPH::Vector3r> fluidScaledVelocitySqr(fluidNumber);

		std::vector<SPH::Vector3r> fluidAccelerationSqr(fluidNumber);
		std::vector<SPH::Vector3r> fluidRelativeAccelerationSqr(fluidNumber);
		std::vector<SPH::Vector3r> fluidScaledAccelerationSqr(fluidNumber);

		std::vector<SPH::Vector3r> fluidColorField(fluidNumber);
		std::vector<SPH::Vector3r> fluidGradColorField(fluidNumber);
		std::vector<SPH::Vector3r> fluidWeberNumber(fluidNumber);
		std::vector<SPH::Vector3r> fluidCurvature(fluidNumber);
		*/
		std::vector<SPH::Vector3r> fluidNormal(fluidNumber);
		/*
		std::vector<SPH::Vector3r> fluidWaveCrest(fluidNumber);
		*/
		std::vector<SPH::Vector3r> fluidCombined(fluidNumber);
	#endif // EXPORT_FLUID

	std::vector<unsigned int> foamIdentifier(foamNumber);
	std::vector<SPH::Vector3r> foamPosition(foamNumber);
	std::vector<SPH::Vector3r> foamVelocity(foamNumber);

	std::vector<unsigned int> mistIdentifier(mistNumber);
	std::vector<SPH::Vector3r> mistPosition(mistNumber);
	std::vector<SPH::Vector3r> mistVelocity(mistNumber);

	#pragma omp parallel default(shared)
	{
		#if EXPORT_FLUID
			FoamFluidModel_Kobsik18* model = reinterpret_cast<FoamFluidModel_Kobsik18*>(this->m_foamFluidModel.get());
			#pragma omp for schedule(static)
			for (int i = 0; i < (int)fluidNumber; i++)
			{
				fluidIdentifier[i] = this->m_foamFluidModel->getId(FLUID_PARTICLE_SET, i);
				fluidPosition[i] = this->m_foamFluidModel->getPosition(FLUID_PARTICLE_SET, i);
				//fluidVelocity[i] = this->m_foamFluidModel->getVelocity(FLUID_PARTICLE_SET, i);
				if (this->m_currentFoamCreationMethod == FoamCreationMethod::Kobsik18)
				{	
					/*
					fluidRelativeVelocity[i]			= model->getRelativeVelocity(i);
					fluidScaledVelocity[i]				= Vector3r(model->getScaledVelocityDifference(i), 0, 0);

					fluidAcceleration[i]				= model->getAcceleration(i);
					fluidRelativeAcceleration[i]		= model->getRelativeAcceleration(i);
					fluidScaledAcceleration[i]			= Vector3r(model->getScaledAccelerationDifference(i), 0, 0);

					fluidVelocitySqr[i] = this->m_foamFluidModel->getVelocity(FLUID_PARTICLE_SET, i).cwiseProduct(this->m_foamFluidModel->getVelocity(FLUID_PARTICLE_SET, i));
					fluidRelativeVelocitySqr[i] = model->getRelativeVelocity(i).cwiseProduct(model->getRelativeVelocity(i));
					fluidScaledVelocitySqr[i] = Vector3r(model->getScaledVelocityDifference(i) * model->getScaledVelocityDifference(i), 0, 0);

					fluidAccelerationSqr[i] = model->getAcceleration(i).cwiseProduct(model->getAcceleration(i));
					fluidRelativeAccelerationSqr[i] = model->getRelativeAcceleration(i).cwiseProduct(model->getRelativeAcceleration(i));
					fluidScaledAccelerationSqr[i] = Vector3r(model->getScaledAccelerationDifference(i) * model->getScaledAccelerationDifference(i), 0, 0);

					fluidColorField[i]					= Vector3r(model->getColorField(i), 0, 0);
					fluidGradColorField[i]				= model->getGradColorField(i);
					fluidWeberNumber[i]					= Vector3r(model->getWeberNumber(i), 0, 0);
					fluidCurvature[i]					= Vector3r(model->getSurfaceCurvature(i), 0, 0);
					*/
					fluidNormal[i]						= model->getFluidNormal(i);
					/*
					fluidWaveCrest[i]					= Vector3r(model->getWaveCrest(i), 0, 0);
					*/
					Real relativeVel					= model->getRelativeVelocity(i).norm();
					Real absoluteVel					= this->m_foamFluidModel->getVelocity(FLUID_PARTICLE_SET, i).norm();
					fluidCombined[i]					= Vector3r(sqrt(relativeVel * relativeVel * absoluteVel * absoluteVel),0,0);
				}

			}
		#endif // EXPORT_FLUID

		if (this->getExportFoam())
		{
			#pragma omp for schedule(static)
			for (int i = 0; i < (int)foamNumber; i++)
			{
				foamIdentifier[i] = this->m_foamFluidModel->getId(FOAM_PARTICLE_SET, i);
				foamPosition[i] = this->m_foamFluidModel->getPosition(FOAM_PARTICLE_SET, i);
				//foamVelocity[i] = this->m_foamFluidModel->getVelocity(FOAM_PARTICLE_SET, i);
				
				FoamFluidModel_Ihmsen12::FoamType type = reinterpret_cast<FoamFluidModel_Kobsik18*>(this->m_foamFluidModel.get())->getFoamType(i);
				if (type == FoamFluidModel_Ihmsen12::BUBBLE)
					foamVelocity[i] = Vector3r(1.0, 0.0, 0.0);
				if (type == FoamFluidModel_Ihmsen12::FOAM)
					foamVelocity[i] = Vector3r(1.0, 1.0, 1.0);
				if (type == FoamFluidModel_Ihmsen12::SPRAY)
					foamVelocity[i] = Vector3r(0.0, 1.0, 0.0);
				
			}
		}

		if (this->getExportMist() && creationMethodAbleToCreateMist)
		{
			#pragma omp for schedule(static)
			for (int i = 0; i < (int)mistNumber; i++)
			{
				mistIdentifier[i] = this->m_foamFluidModel->getId(MIST_PARTICLE_SET, i);
				mistPosition[i] = this->m_foamFluidModel->getPosition(MIST_PARTICLE_SET, i);
				mistVelocity[i] = this->m_foamFluidModel->getVelocity(MIST_PARTICLE_SET, i);
			}
		}

	}

	#if EXPORT_FLUID
		//saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidVelocity[0], fluidRadius, "Fluid/VelocityAbsolute");
		if (this->m_currentFoamCreationMethod == FoamCreationMethod::Kobsik18)
		{
			/*
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidRelativeVelocity[0], fluidRadius, "Fluid/VelocityRelative");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidScaledVelocity[0], fluidRadius, "Fluid/VelocityScaled");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidAcceleration[0], fluidRadius, "Fluid/AccelerationAbsolute");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidRelativeAcceleration[0], fluidRadius, "Fluid/AccelerationRelative");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidScaledAcceleration[0], fluidRadius, "Fluid/AccelerationScaled");
			*/
			//saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidColorField[0], fluidRadius, "Fluid/ColorField");
			//saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidGradColorField[0], fluidRadius, "Fluid/GradColorField");
			//saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidWeberNumber[0], fluidRadius, "Fluid/WeberNumber");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidNormal[0], fluidRadius, "Fluid/Normal");
			//saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidCurvature[0], fluidRadius, "Fluid/Curvature");
			
			//saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidWaveCrest[0], fluidRadius, "Fluid/WaveCrest");
			/*
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidVelocitySqr[0], fluidRadius, "Fluid/VelocityAbsoluteSquared");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidRelativeVelocitySqr[0], fluidRadius, "Fluid/VelocityRelativeSquared");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidScaledVelocitySqr[0], fluidRadius, "Fluid/VelocityScaledSquared");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidAccelerationSqr[0], fluidRadius, "Fluid/AccelerationAbsoluteSquared");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidRelativeAccelerationSqr[0], fluidRadius, "Fluid/AccelerationRelativeSquared");
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidScaledAccelerationSqr[0], fluidRadius, "Fluid/AccelerationAcaledSquared");
			*/
			saveParticleData(fluidNumber, &fluidIdentifier[0], &fluidPosition[0], &fluidCombined[0], fluidRadius, "Fluid/Combined");
		}
	#endif // EXPORT_FLUID
	if (this->getExportFoam())
		saveParticleData(foamNumber, &foamIdentifier[0], &foamPosition[0], &foamVelocity[0], foamRadius, "Foam");
	if (this->getExportMist() && creationMethodAbleToCreateMist)
		saveParticleData(mistNumber, &mistIdentifier[0], &mistPosition[0], &mistVelocity[0], mistRadius, "Mist");

}

void FoamManager::init(Real p_fluidRadius) 
{
	this->m_foamBehaviourController = FoamFactory::createFoamBehaviourController(this->m_currentFoamCreationMethod);
	this->m_foamFluidModel = FoamFactory::createFoamFluidModel(this->m_currentFoamCreationMethod);
	this->m_foamFluidModel->setParticleRadius(p_fluidRadius);
	std::vector<Vector3r> initVector;
	initVector.push_back(Vector3r());
	this->m_foamFluidModel->initModel(1, &initVector[0], &initVector[0], 0, 1, &initVector[0], &initVector[0]);
	this->m_foamParameters = FoamFactory::createFoamParameters(this->m_currentFoamCreationMethod);
	this->m_foamParticleCreator = FoamFactory::createFoamParticleCreator(this->m_currentFoamCreationMethod);
	this->m_currentParticleSet = 0;
}

void FoamManager::performNeighbourhoodSearch() 
{
	this->m_foamFluidModel->performNeighborhoodSearchSort();
	this->m_foamFluidModel->getNeighborhoodSearch()->find_neighbors();
}

void FoamManager::saveParticleData(unsigned int p_size, unsigned int *p_id, Vector3r *p_pos, Vector3r *p_data, Real p_radius, std::string p_name)
{
	std::string exportPath = FileSystem::normalizePath(FileSystem::getProgramPath() + "/PartioExport/" + p_name);

	FileSystem::makeDirs(exportPath);

	std::string fileName = "ParticleData";
	fileName = fileName + std::to_string(this->m_currentParticleSet) + ".bgeo";

	std::string exportFileName = FileSystem::normalizePath(exportPath + "/" + fileName);
	PartioReaderWriter::writeParticles(exportFileName, p_size, p_pos, p_data, p_radius, p_id);
}

void FoamManager::setFluidParticleRadius(Real p_radius)
{
	init(p_radius);
}

Real FoamManager::getFluidParticleRadius()
{
	return this->m_foamFluidModel->getParticleRadius();
}

void FoamManager::setFoamCreationMethod(unsigned int p_method) 
{
	switch (p_method) {
	case 0:
		this->m_currentFoamCreationMethod = FoamCreationMethod::Ihmsen12;
		break;
	case 1:
		this->m_currentFoamCreationMethod = FoamCreationMethod::Kim16;
		break;
	case 2:
		this->m_currentFoamCreationMethod = FoamCreationMethod::Kobsik18;
		break;
	default:
		this->m_currentFoamCreationMethod = FoamCreationMethod::Ihmsen12;
		break;
	}

}

void FoamManager::setFluidParticleSet(unsigned int *p_identifier, SPH::Vector3r *p_position, SPH::Vector3r *p_velocity, const unsigned int p_number) 
{
	this->m_foamFluidModel->changeFluidParticles(p_number, p_identifier, p_position, p_velocity, 0);	
}

void FoamManager::getFoamParticleSet(std::vector<SPH::Vector3r> &p_position, std::vector<SPH::Vector3r> &p_velocity, unsigned int &p_number)
{
	p_number = this->m_foamFluidModel->getNumFoamParticles0();

	p_position.resize(p_number);
	p_velocity.resize(p_number);

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int) p_number; i++)
		{
			p_position[i] = this->m_foamFluidModel->getPosition(FOAM_PARTICLE_SET, i);
			p_velocity[i] = this->m_foamFluidModel->getVelocity(FOAM_PARTICLE_SET, i);
		}
	}
}

void FoamManager::getMistParticleSet(std::vector<SPH::Vector3r> &p_position, std::vector<SPH::Vector3r> &p_velocity, unsigned int &p_number)
{
	if (this->m_currentFoamCreationMethod == FoamCreationMethod::Kobsik18)
	{
		p_number = reinterpret_cast<FoamFluidModel_Kobsik18*>(this->m_foamFluidModel.get())->numMistParticles();

		p_position.resize(p_number);
		p_velocity.resize(p_number);

		#pragma omp parallel default(shared)
		{
			#pragma omp for schedule(static)  
			for (int i = 0; i < (int)p_number; i++)
			{
				p_position[i] = this->m_foamFluidModel->getPosition(MIST_PARTICLE_SET, i);
				p_velocity[i] = this->m_foamFluidModel->getVelocity(MIST_PARTICLE_SET, i);
			}
		}
	}
	else 
	{
		std::cout << "ERROR: Mist particles only available with FoamCreationMethod - Kobsik18." << std::endl;
	}
	
}

void FoamManager::getFoamParticleSet(std::vector<unsigned int> &p_identifier, std::vector<SPH::Vector3r> &p_position, std::vector<SPH::Vector3r> &p_velocity, unsigned int &p_number)
{
	FoamFluidModel_Kobsik18 *model = reinterpret_cast<FoamFluidModel_Kobsik18*>(this->m_foamFluidModel.get());

	p_number = this->m_foamFluidModel->getNumFoamParticles0();

	p_position.resize(p_number);
	p_velocity.resize(p_number);
	p_identifier.resize(p_number);

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)p_number; i++)
		{
			p_position[i] = this->m_foamFluidModel->getPosition(FOAM_PARTICLE_SET, i);
			p_velocity[i] = this->m_foamFluidModel->getVelocity(FOAM_PARTICLE_SET, i);
			p_identifier[i] = this->m_foamFluidModel->getId(FOAM_PARTICLE_SET, i);
		}
	}

}