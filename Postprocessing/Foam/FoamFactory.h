#pragma once

#ifndef __FoamFactory_h__
#define __FoamFactory_h__

// type: static singleton

#include <memory>

class FoamBehaviourController;
class FoamFluidModel;
class FoamParameters;
class FoamParticleCreator;

enum FoamCreationMethod { Ihmsen12, Kim16, Kobsik18 };

class FoamFactory {

public: // methods
	static std::unique_ptr<FoamBehaviourController> createFoamBehaviourController(FoamCreationMethod p_method);
	static std::unique_ptr<FoamFluidModel> createFoamFluidModel(FoamCreationMethod p_method);
	static std::unique_ptr<FoamParameters> createFoamParameters(FoamCreationMethod p_method);
	static std::unique_ptr<FoamParticleCreator> createFoamParticleCreator(FoamCreationMethod p_method);
};


#endif // !__FoamFactory_h__