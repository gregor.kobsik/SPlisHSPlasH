#pragma once

#include "FoamFluidModel.h"

using namespace SPH;

FoamFluidModel::FoamFluidModel() :
	FluidModel(),
	m_vFoam0()
{
	ParticleObject *foamParticles = new ParticleObject();
	m_particleObjects.push_back(foamParticles);

	this->RBO_POSITION_POINTER = m_particleObjects.size();

	this->m_particleRadius = 0.025;
	this->m_foamRadius = 0.0025;							//TODO: choose good value here, 1/10 particleRadius is proposed by Ihmsen
	setFoamMass(1.0);										//TODO: choose realistic value here
}

FoamFluidModel::~FoamFluidModel(void)
{
	cleanupModel();
}

void FoamFluidModel::cleanupModel()
{
	releaseFoamParticles();
	// delete foam particles and rbo's, fluid will be deleted in FluidModel destructor.
	for (unsigned int i = 1; i < m_particleObjects.size(); i++)
	{
		if (i >= RBO_POSITION_POINTER)
		{
			RigidBodyParticleObject *rbpo = ((RigidBodyParticleObject*)m_particleObjects[i]);
			rbpo->m_boundaryPsi.clear();
			rbpo->m_f.clear();
			delete rbpo->m_rigidBody;
			delete rbpo;
		}
		else
			delete m_particleObjects[i];
	}
	m_particleObjects.resize(1); //only fluid particles left to be deleted by ~FluidModel();
}

void FoamFluidModel::reset()
{
	m_emitterSystem.reset();
	setNumActiveParticles(m_numActiveParticles0);
	setNumFoamParticles(m_numFoamParticles0);
	const unsigned int nPoints = numActiveParticles();
	const unsigned int nFoam = numFoamParticles();

	// reset velocities and accelerations
	for (unsigned int i = RBO_POSITION_POINTER; i < m_particleObjects.size(); i++)
	{
		for (int j = 0; j < (int)m_particleObjects[i]->m_x.size(); j++)
		{
			RigidBodyParticleObject *rbpo = ((RigidBodyParticleObject*)m_particleObjects[i]);
			rbpo->m_f[j].setZero();
			rbpo->m_v[j].setZero();
		}
	}

	// Fluid
	if (m_neighborhoodSearch->point_set(FLUID_PARTICLE_SET).n_points() != nPoints)

		if (nPoints == 0) // runtime errors in debug mode, if initializing with empty variables
		{
			std::vector<Vector3r> fakeFluid;
			fakeFluid.push_back(Vector3r());
			m_neighborhoodSearch->resize_point_set(FLUID_PARTICLE_SET, &fakeFluid[0][0], nPoints);
		}
		else
			m_neighborhoodSearch->resize_point_set(FLUID_PARTICLE_SET, &getPosition(FLUID_PARTICLE_SET, 0)[0], nPoints);

	for (unsigned int i = 0; i < nPoints; i++)
	{
		const Vector3r& x0 = getPosition0(FLUID_PARTICLE_SET, i);
		getPosition(FLUID_PARTICLE_SET, i) = x0;
		getVelocity(FLUID_PARTICLE_SET, i) = getVelocity0(i);
		getAcceleration(i).setZero();
		m_density[i] = 0.0;
	}

	// Foam
	if (m_neighborhoodSearch->point_set(FOAM_PARTICLE_SET).n_points() != nFoam)

		if (nFoam == 0) // runtime errors in debug mode, if initializing with empty variables
		{
			std::vector<Vector3r> fakeFoam;
			fakeFoam.push_back(Vector3r());
			m_neighborhoodSearch->resize_point_set(FOAM_PARTICLE_SET, &fakeFoam[0][0], nFoam);
		}
		else
			m_neighborhoodSearch->resize_point_set(FOAM_PARTICLE_SET, &getPosition(FOAM_PARTICLE_SET, 0)[0], nFoam);

	for (unsigned int i = 0; i < nFoam; i++) {
		const Vector3r& x0 = getPosition0(FOAM_PARTICLE_SET, i);
		getPosition(FOAM_PARTICLE_SET, i) = x0;
		getVelocity(FOAM_PARTICLE_SET, i) = getFoamVelocity0(i);
	}

	updateBoundaryPsi();	

	m_neighborhoodSearch->set_active(false);
	m_neighborhoodSearch->set_active(FLUID_PARTICLE_SET, FLUID_PARTICLE_SET, true);
	m_neighborhoodSearch->set_active(FOAM_PARTICLE_SET, FLUID_PARTICLE_SET, true);	// only fluid particles are found when searching for neighbors of foam particles
	for (unsigned int i = RBO_POSITION_POINTER; i < m_neighborhoodSearch->point_sets().size(); i++)										
		m_neighborhoodSearch->set_active(FLUID_PARTICLE_SET, i, true);
}

void FoamFluidModel::releaseFluidParticles()
{
	m_particleObjects[FLUID_PARTICLE_SET]->m_x0.clear();
	m_particleObjects[FLUID_PARTICLE_SET]->m_x.clear();
	m_particleObjects[FLUID_PARTICLE_SET]->m_v.clear();
	m_particleObjects[FLUID_PARTICLE_SET]->m_id.clear();
	m_v0.clear();
	m_a.clear();
	m_masses.clear();
	m_density.clear();
}

void FoamFluidModel::releaseFoamParticles()
{
	m_particleObjects[FOAM_PARTICLE_SET]->m_x0.clear();
	m_particleObjects[FOAM_PARTICLE_SET]->m_x.clear();
	m_particleObjects[FOAM_PARTICLE_SET]->m_v.clear();
	m_particleObjects[FOAM_PARTICLE_SET]->m_id.clear();
	m_vFoam0.clear();
}

void FoamFluidModel::initModel(const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles, unsigned const int nFoamParticles, Vector3r* foamParticles, Vector3r* foamVelocities)
{
	releaseFluidParticles();
	resizeFluidParticles(nFluidParticles + nMaxEmitterParticles);

	releaseFoamParticles();
	resizeFoamParticles(nFoamParticles);

	// init kernel
	setParticleRadius(m_particleRadius);

	// make sure that m_W_zero is set correctly for the new particle radius
	setKernel(getKernel());
	// copy fluid positions
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)nFluidParticles; i++)
		{
			getPosition0(FLUID_PARTICLE_SET, i) = fluidParticles[i];
			getVelocity0(i) = fluidVelocities[i];
		}
	}

	// copy foam positions
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)nFoamParticles; i++)
		{
			getPosition0(FOAM_PARTICLE_SET, i) = foamParticles[i];
			getFoamVelocity0(i) = foamVelocities[i];
		}
	}

	// initialize masses
	initMasses();

	// Initialize neighborhood search
	if (m_neighborhoodSearch == NULL)
		m_neighborhoodSearch = new CompactNSearch::NeighborhoodSearch(m_supportRadius, false);
	else
	{
		delete m_neighborhoodSearch;
		m_neighborhoodSearch = new CompactNSearch::NeighborhoodSearch(m_supportRadius, false);
	}
	m_neighborhoodSearch->set_radius(m_supportRadius);

	// Fluids 
	m_neighborhoodSearch->add_point_set(&getPosition(FLUID_PARTICLE_SET, 0)[0], nFluidParticles, true, true); 

	// Foam
	m_neighborhoodSearch->add_point_set(&getPosition(FOAM_PARTICLE_SET, 0)[0], nFoamParticles, true, true);

	// Boundary
	for (unsigned int i = 0; i < numberOfRigidBodyParticleObjects(); i++)
	{
		RigidBodyParticleObject *rb = getRigidBodyParticleObject(i);
		m_neighborhoodSearch->add_point_set(&rb->m_x[0][0], rb->m_x.size(), rb->m_rigidBody->isDynamic(), false);
	}

	m_numActiveParticles0 = nFluidParticles;
	m_numActiveParticles = m_numActiveParticles0;

	m_numFoamParticles0 = nFoamParticles;
	m_numFoamParticles = m_numFoamParticles0;

	reset();
}

void FoamFluidModel::changeFluidParticles(const unsigned int nFluidParticles, unsigned int *p_identifier, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles)
{

	releaseFluidParticles();
	resizeFluidParticles(nFluidParticles + nMaxEmitterParticles);

	// init kernel
	setParticleRadius(m_particleRadius);

	// copy fluid positions
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)nFluidParticles; i++)
		{
			getId(FLUID_PARTICLE_SET, i) = p_identifier[i];
			getPosition0(0, i) = fluidParticles[i];
			getVelocity0(i) = fluidVelocities[i];
		}
	}

	// initialize masses
	initMasses();

	// Fluids 
	m_neighborhoodSearch->resize_point_set(FLUID_PARTICLE_SET, &getPosition(FLUID_PARTICLE_SET, 0)[0], nFluidParticles);

	m_numActiveParticles0 = nFluidParticles;
	m_numActiveParticles = m_numActiveParticles0;

	reset();
}

void FoamFluidModel::resizeFluidParticles(const unsigned int newSize)
{
	m_particleObjects[FLUID_PARTICLE_SET]->m_x0.resize(newSize);
	m_particleObjects[FLUID_PARTICLE_SET]->m_x.resize(newSize);
	m_particleObjects[FLUID_PARTICLE_SET]->m_v.resize(newSize);
	m_particleObjects[FLUID_PARTICLE_SET]->m_id.resize(newSize);
	m_v0.resize(newSize);
	m_a.resize(newSize);
	m_masses.resize(newSize);
	m_density.resize(newSize);
}

void FoamFluidModel::resizeFoamParticles(const unsigned int newSize)
{
	this->m_numFoamParticles = newSize;
	this->m_numFoamParticles0 = newSize;

	m_particleObjects[FOAM_PARTICLE_SET]->m_x0.resize(newSize);
	m_particleObjects[FOAM_PARTICLE_SET]->m_x.resize(newSize);
	m_particleObjects[FOAM_PARTICLE_SET]->m_v.resize(newSize);
	m_particleObjects[FOAM_PARTICLE_SET]->m_id.resize(newSize);
	m_vFoam0.resize(newSize);
}

void FoamFluidModel::deleteFluidParticles(std::vector<unsigned int> indizies)
{
	unsigned int lastElement = numActiveParticles() - 1;

	for (unsigned int i = 0; i < indizies.size(); i++) {
		if (indizies[i] <= lastElement) 
		{
			unsigned int currentElement = indizies[i];

			m_particleObjects[FLUID_PARTICLE_SET]->m_x0[currentElement] = m_particleObjects[FLUID_PARTICLE_SET]->m_x0[lastElement];
			m_particleObjects[FLUID_PARTICLE_SET]->m_x[currentElement] = m_particleObjects[FLUID_PARTICLE_SET]->m_x[lastElement];
			m_particleObjects[FLUID_PARTICLE_SET]->m_v[currentElement] = m_particleObjects[FLUID_PARTICLE_SET]->m_v[lastElement];
			m_v0[currentElement] = m_v0[lastElement];
			m_a[currentElement] = m_a[lastElement];
			m_masses[currentElement] = m_masses[lastElement];
			m_density[currentElement] = m_density[lastElement];

			lastElement--;
		}	
	}

	resizeFluidParticles(lastElement + 1);
}

void FoamFluidModel::deleteFoamParticles(std::vector<unsigned int> indizies)
{
	unsigned int lastElement = getNumFoamParticles0() - 1;

	for (unsigned int i = 0; i < indizies.size(); i++) {
		if (indizies[i] <= lastElement)
		{
			unsigned int currentElement = indizies[i];

			m_particleObjects[FOAM_PARTICLE_SET]->m_x0[currentElement] = m_particleObjects[FOAM_PARTICLE_SET]->m_x0[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_x[currentElement] = m_particleObjects[FOAM_PARTICLE_SET]->m_x[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_v[currentElement] = m_particleObjects[FOAM_PARTICLE_SET]->m_v[lastElement];
			m_vFoam0[currentElement] = m_vFoam0[lastElement];

			lastElement--;
		}		
	}

	resizeFoamParticles(lastElement + 1);
}

void FoamFluidModel::updateBoundaryPsi()
{
	//////////////////////////////////////////////////////////////////////////
	// Compute value psi for boundary particles (boundary handling)
	// (see Akinci et al. "Versatile rigid - fluid coupling for incompressible SPH", Siggraph 2012
	//////////////////////////////////////////////////////////////////////////

	// Search boundary neighborhood

	// Activate only static boundaries
	//std::cout << "Initialize boundary psi\n";
	m_neighborhoodSearch->set_active(false);
	for (unsigned int i = 0; i < numberOfRigidBodyParticleObjects(); i++)
	{
		if (!getRigidBodyParticleObject(i)->m_rigidBody->isDynamic())
			m_neighborhoodSearch->set_active(i + 2, true, true);														//changed + 1 to + 2.
	}

	m_neighborhoodSearch->find_neighbors();

	// Boundary objects
	for (unsigned int body = 0; body < numberOfRigidBodyParticleObjects(); body++)
	{
		if (!getRigidBodyParticleObject(body)->m_rigidBody->isDynamic())
			computeBoundaryPsi(body);
	}

	////////////////////////////////////////////////////////////////////////// 
	// Compute boundary psi for all dynamic bodies
	//////////////////////////////////////////////////////////////////////////
	for (unsigned int body = 0; body < numberOfRigidBodyParticleObjects(); body++)
	{
		// Deactivate all
		m_neighborhoodSearch->set_active(false);

		// Only activate next dynamic body
		if (getRigidBodyParticleObject(body)->m_rigidBody->isDynamic())
		{
			m_neighborhoodSearch->set_active(body + 2, true, true);														//changed + 1 to + 2.
			m_neighborhoodSearch->find_neighbors();
			computeBoundaryPsi(body);
		}
	}

}

void FoamFluidModel::computeBoundaryPsi(const unsigned int body)
{
	const Real density0 = getDensity0();

	RigidBodyParticleObject *rb = getRigidBodyParticleObject(body);
	const unsigned int numBoundaryParticles = rb->numberOfParticles();

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)numBoundaryParticles; i++)
		{
			Real delta = m_W_zero;
			for (unsigned int pid = 2; pid < numberOfPointSets(); pid++)												// changed pid = 1 to pid = 2
			{
				for (unsigned int j = 0; j < m_neighborhoodSearch->point_set(body + 2).n_neighbors(pid, i); j++)		// changed + 1 to + 2
				{
					const unsigned int neighborIndex = m_neighborhoodSearch->point_set(body + 2).neighbor(pid, i, j);	// changed + 1 to + 2
					delta += W(getPosition(body + 2, i) - getPosition(pid, neighborIndex));								// changed + 1 to + 2
				}
			}
			const Real volume = 1.0 / delta;
			rb->m_boundaryPsi[i] = density0 * volume;
		}
	}
}

void FoamFluidModel::performNeighborhoodSearchSort()
{
	const unsigned int numPart = numActiveParticles();
	if (numPart == 0)
		return;

	m_neighborhoodSearch->z_sort();

	// Fluid
	auto const& d = m_neighborhoodSearch->point_set(FLUID_PARTICLE_SET);
	if (d.n_points() > 0) {
		d.sort_field(&m_particleObjects[FLUID_PARTICLE_SET]->m_x[0]);
		d.sort_field(&m_particleObjects[FLUID_PARTICLE_SET]->m_v[0]);
		d.sort_field(&m_particleObjects[FLUID_PARTICLE_SET]->m_id[0]);
		d.sort_field(&m_a[0]);
		d.sort_field(&m_masses[0]);
		d.sort_field(&m_density[0]);
	}

	// Foam
	auto const& e = m_neighborhoodSearch->point_set(FOAM_PARTICLE_SET);
	if (e.n_points() > 0) {
		e.sort_field(&m_particleObjects[FOAM_PARTICLE_SET]->m_x[0]);
		e.sort_field(&m_particleObjects[FOAM_PARTICLE_SET]->m_v[0]);
		e.sort_field(&m_particleObjects[FOAM_PARTICLE_SET]->m_id[0]);
		e.sort_field(&m_vFoam0[0]);
	}

	//////////////////////////////////////////////////////////////////////////
	// Boundary
	//////////////////////////////////////////////////////////////////////////
	for (unsigned int i = 2; i < m_neighborhoodSearch->point_sets().size(); i++)
	{
		RigidBodyParticleObject *rb = getRigidBodyParticleObject(i - 1);
		if (rb->m_rigidBody->isDynamic())	// sort only dynamic boundaries
		{
			auto const& d = m_neighborhoodSearch->point_set(i);
			d.sort_field(&rb->m_x[0]);
			d.sort_field(&rb->m_v[0]);
			d.sort_field(&rb->m_f[0]);
			d.sort_field(&rb->m_boundaryPsi[0]);
		}
	}
}