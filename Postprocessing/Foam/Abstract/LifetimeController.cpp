#include "LifetimeController.h"

#include "Postprocessing\Foam\FoamFluidModel.h"
#include "Postprocessing\Foam\Abstract\FoamParameters.h"

LifetimeController::LifetimeController() 
{
	this->m_numberDestroyedParticles = 0;
}

LifetimeController::~LifetimeController()
{

}