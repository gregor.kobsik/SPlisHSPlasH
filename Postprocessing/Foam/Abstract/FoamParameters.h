#pragma once

#ifndef __FoamParameters_h__
#define __FoamParameters_h__

// type: ADO

class FoamParameters {
public: // constructors, destructors
	FoamParameters();
	virtual ~FoamParameters() = 0;

public: // setter, getter
	virtual void setDefaultParameters() = 0;

protected: // variables

};

#endif // !__FoamParameters_h__