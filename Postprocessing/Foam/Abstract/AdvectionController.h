#pragma once

#ifndef __AdvectionController_h__
#define __AdvectionController_h__

// type: ABO

class FoamFluidModel;
class FoamParameters;

class AdvectionController {


public: // constructors, destructors
	AdvectionController();
	virtual ~AdvectionController() = 0;

public: // methods
	virtual void advectParticles(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;

protected: // methods

protected: // variables



};


#endif // !__AdvectionController_h__