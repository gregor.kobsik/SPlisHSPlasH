#include "FoamBehaviourController.h"

#include "Postprocessing\Foam\FoamFluidModel.h"
#include "Postprocessing\Foam\Abstract\FoamParameters.h"

#include "AdvectionController.h"
#include "LifetimeController.h"
#include "OutOfRangeController.h"
#include "RigidBodyInteractionController.h"

FoamBehaviourController::FoamBehaviourController()
{
	
}

FoamBehaviourController::~FoamBehaviourController() 
{
	
}

void FoamBehaviourController::progress(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	this->progressAdvection(p_model, p_parameters);
	this->progressRigidBodyInteraction(p_model, p_parameters);
	this->progressLifetime(p_model, p_parameters);
	this->progressOutOfRange(p_model, p_parameters);
}