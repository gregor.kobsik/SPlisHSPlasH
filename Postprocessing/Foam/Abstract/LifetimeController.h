#pragma once

#ifndef __LifetimeController_h__
#define __LifetimeController_h__

// type: ABO

class FoamFluidModel;
class FoamParameters;

class LifetimeController {

public: // constructors, destructors
	LifetimeController();
	virtual ~LifetimeController() = 0;

public: // methods
	virtual void processLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;

public: // setter, getter
	unsigned int getNumberOfDestroyedParticles() { return m_numberDestroyedParticles; }

protected: // methods

protected: // variables
	unsigned int										m_numberDestroyedParticles = 0;
};

#endif // !__LifetimeController_h__