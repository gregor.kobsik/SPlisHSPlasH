#pragma once

#ifndef __OutOfRangeController_h__
#define __OutOfRangeController_h__

// type: ABO

class FoamFluidModel;
class FoamParameters;

class OutOfRangeController {

public: // constructors, destructors
	OutOfRangeController();
	virtual ~OutOfRangeController() = 0;

public: // methods
	virtual void processOutOfRangeParticles(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;

public: // setter, getter
	unsigned int getNumberOfOutOfRangeParticles() { return m_numberOutOfRangeParticles; }

protected: // methods

protected: // variables
	unsigned int										m_numberOutOfRangeParticles = 0;
};

#endif // !__OutOfRangeController_h__