#include "FoamParticleCreator.h"

#include "Postprocessing\Foam\FoamFluidModel.h"
#include "FoamNumberComputer.h"
#include "FoamParameters.h"


FoamParticleCreator::FoamParticleCreator() :
	m_foamNumberComputer()
{
	this->reset();
}

FoamParticleCreator::~FoamParticleCreator()
{

}

void FoamParticleCreator::reset()
{
	this->numberOfCreatedParticles = 0;
}