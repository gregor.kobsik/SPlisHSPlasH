#pragma once

#ifndef __FoamBehaviourController_h__
#define __FoamBehaviourController_h__

// type: ABO

#include <memory>

class AdvectionController;
class FoamFluidModel;
class FoamParameters;
class LifetimeController;
class OutOfRangeController;
class RigidBodyInteractionController;

class FoamBehaviourController {

public: // constructors, destructors
	FoamBehaviourController();
	virtual ~FoamBehaviourController();

public: // methods
	virtual void progress(FoamFluidModel *p_model, FoamParameters *p_parameters);

protected: // methods

	virtual void progressAdvection(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;
	virtual void progressLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;
	virtual void progressOutOfRange(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;
	virtual void progressRigidBodyInteraction(FoamFluidModel *p_model, FoamParameters *sp_parameters) = 0;

protected: //variables

	std::unique_ptr<AdvectionController>							m_advectionController = nullptr;
	std::unique_ptr<LifetimeController>								m_lifetimeController = nullptr;
	std::unique_ptr<OutOfRangeController>							m_outOfRangeController = nullptr;
	std::unique_ptr<RigidBodyInteractionController>					m_rigidBodyInteractionController = nullptr;

};

#endif // !__FoamBehaviourController_h__