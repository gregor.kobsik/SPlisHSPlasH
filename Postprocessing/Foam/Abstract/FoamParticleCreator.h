#pragma once

#ifndef __FoamParticleCreator_h__
#define __FoamParticleCreator_h__

// type: ABO

#include <memory>

class FoamFluidModel;
class FoamParameters;
class FoamNumberComputer;

class FoamParticleCreator {
public: // constructor, destructor
	FoamParticleCreator();
	virtual ~FoamParticleCreator() = 0;

public: // methods
	virtual void createParticles(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;

public: // setter, getter
	unsigned int getNumberOfNewParticles() { return numberOfCreatedParticles;  }

protected: //methods
	virtual void reset();

protected: // variables
	std::unique_ptr<FoamNumberComputer>					m_foamNumberComputer = nullptr;
	unsigned int										numberOfCreatedParticles;
};

#endif // !__FoamParticleCreator_h__