#pragma once

#ifndef __RigidBodyInteractionController_h__
#define __RigidBodyInteractionController_h__

// type: ABO

class FoamFluidModel;
class FoamParameters;

class RigidBodyInteractionController {

public: // constructors, destructors
	RigidBodyInteractionController();
	virtual ~RigidBodyInteractionController() = 0;

public: // methods
	virtual void processRigidBodyInteraction(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;

public: // setter, getter
	unsigned int getNumberOfCollidingParticles() { return numberCollidingParticles; }

protected: // methods

protected: // variables
	unsigned int										numberCollidingParticles = 0;
};

#endif // !__RigidBodyInteractionController_h__