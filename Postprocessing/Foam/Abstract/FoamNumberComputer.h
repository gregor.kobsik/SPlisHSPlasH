#pragma once

#ifndef __FoamNumberComputer_h__
#define __FoamNumberComputer_h__

// type: ABO

#include "SPlisHSPlasH\Common.h"
#include <vector>

#include "Postprocessing\Foam\FoamFluidModel.h"
class FoamParameters;

class FoamNumberComputer {
public: // constructor, destructor
	FoamNumberComputer();
	virtual ~FoamNumberComputer() = 0;

public: // methods
	virtual void computeNumberOfNewParticles(FoamFluidModel *p_model, FoamParameters *p_parameters) = 0;

public: // setter, getter
	unsigned int getNumberOfNewFoamParticles() { return m_numberOfNewFoamParticles;  }
	std::vector<Real> getNumberOfNewFoamParticlesPerFluidParticle() { return m_numberOfNewFoamParticlesPerFluidParticle; }

protected: //methods
	virtual void reset();

protected: // variables
	unsigned int										m_numberOfNewFoamParticles;
	std::vector<Real>									m_numberOfNewFoamParticlesPerFluidParticle;
};

#endif // !__FoamNumberComputer_h__