#include "FoamNumberComputer.h"

#include "Postprocessing/Foam/FoamFluidModel.h"
#include "FoamParameters.h"

FoamNumberComputer::FoamNumberComputer()
{
	this->m_numberOfNewFoamParticles = 0;
}

FoamNumberComputer::~FoamNumberComputer()
{
}

void FoamNumberComputer::reset()
{
	this->m_numberOfNewFoamParticles = 0;
	this->m_numberOfNewFoamParticlesPerFluidParticle.clear();
}