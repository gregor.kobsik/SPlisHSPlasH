#include "RigidBodyInteractionController.h"

#include "Postprocessing\Foam\FoamFluidModel.h"
#include "Postprocessing\Foam\Abstract\FoamParameters.h"


RigidBodyInteractionController::RigidBodyInteractionController() 
{
	this->numberCollidingParticles = 0;
}

RigidBodyInteractionController::~RigidBodyInteractionController()
{

}