#include "FoamFactory.h"

#include "Abstract\FoamBehaviourController.h"
#include "FoamFluidModel.h"
#include "Abstract\FoamParameters.h"
#include "Abstract\FoamParticleCreator.h"

#include "Ihmsen12\FoamBehaviourController_Ihmsen12.h"
#include "Ihmsen12\FoamFluidModel_Ihmsen12.h"
#include "Ihmsen12\FoamParameters_Ihmsen12.h"
#include "Ihmsen12\FoamParticleCreator_Ihmsen12.h"

#include "Kobsik18\FoamBehaviourController_Kobsik18.h"
#include "Kobsik18\FoamFluidModel_Kobsik18.h"
#include "Kobsik18\FoamParameters_Kobsik18.h"
#include "Kobsik18\FoamParticleCreator_Kobsik18.h"

std::unique_ptr<FoamBehaviourController> FoamFactory::createFoamBehaviourController(FoamCreationMethod p_method)
{
	switch (p_method) {
	case FoamCreationMethod::Ihmsen12:
		return std::unique_ptr<FoamBehaviourController_Ihmsen12>(new FoamBehaviourController_Ihmsen12());
	case FoamCreationMethod::Kim16:
		return std::unique_ptr<FoamBehaviourController_Ihmsen12>(new FoamBehaviourController_Ihmsen12());
	case FoamCreationMethod::Kobsik18:
		return std::unique_ptr<FoamBehaviourController_Kobsik18>(new FoamBehaviourController_Kobsik18());
	default:
		return std::unique_ptr<FoamBehaviourController_Ihmsen12>(new FoamBehaviourController_Ihmsen12());
	}
}

std::unique_ptr<FoamFluidModel> FoamFactory::createFoamFluidModel(FoamCreationMethod p_method)
{
	switch (p_method) {
	case FoamCreationMethod::Ihmsen12:
		return std::unique_ptr<FoamFluidModel_Ihmsen12>(new FoamFluidModel_Ihmsen12());
	case FoamCreationMethod::Kim16:
		return std::unique_ptr<FoamFluidModel_Ihmsen12>(new FoamFluidModel_Ihmsen12());
	case FoamCreationMethod::Kobsik18:
		return std::unique_ptr<FoamFluidModel_Kobsik18>(new FoamFluidModel_Kobsik18());
	default:
		return std::unique_ptr<FoamFluidModel_Ihmsen12>(new FoamFluidModel_Ihmsen12());
	}
}

std::unique_ptr<FoamParameters> FoamFactory::createFoamParameters(FoamCreationMethod p_method)
{
	switch (p_method) {
	case FoamCreationMethod::Ihmsen12:
		return std::unique_ptr<FoamParameters_Ihmsen12>(new FoamParameters_Ihmsen12());
	case FoamCreationMethod::Kim16:
		return std::unique_ptr<FoamParameters_Ihmsen12>(new FoamParameters_Ihmsen12());
	case FoamCreationMethod::Kobsik18:
		return std::unique_ptr<FoamParameters_Kobsik18>(new FoamParameters_Kobsik18());
	default:
		return std::unique_ptr<FoamParameters_Ihmsen12>(new FoamParameters_Ihmsen12());
	}
}


std::unique_ptr<FoamParticleCreator> FoamFactory::createFoamParticleCreator(FoamCreationMethod p_method)
{
	switch (p_method) {
	case FoamCreationMethod::Ihmsen12:
		return std::unique_ptr<FoamParticleCreator_Ihmsen12>(new FoamParticleCreator_Ihmsen12());
	case FoamCreationMethod::Kim16:
		return std::unique_ptr<FoamParticleCreator_Ihmsen12>(new FoamParticleCreator_Ihmsen12());
	case FoamCreationMethod::Kobsik18:
		return std::unique_ptr<FoamParticleCreator_Kobsik18>(new FoamParticleCreator_Kobsik18());
	default:
		return std::unique_ptr<FoamParticleCreator_Ihmsen12>(new FoamParticleCreator_Ihmsen12());
	}
}