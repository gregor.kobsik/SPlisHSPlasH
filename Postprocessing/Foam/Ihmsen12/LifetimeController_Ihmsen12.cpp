#include "LifetimeController_Ihmsen12.h"

#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"

LifetimeController_Ihmsen12::LifetimeController_Ihmsen12() : LifetimeController()
{

}

LifetimeController_Ihmsen12::~LifetimeController_Ihmsen12()
{

}

void LifetimeController_Ihmsen12::processLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	FoamFluidModel_Ihmsen12 *model = reinterpret_cast<FoamFluidModel_Ihmsen12*>(p_model);
	FoamParameters_Ihmsen12 *parameters = reinterpret_cast<FoamParameters_Ihmsen12*>(p_parameters);

	std::vector<unsigned int> destroyedIndizies;


	#pragma omp parallel default(shared)
	{
		std::vector<unsigned int> destroyedIndiziesThread;

		#pragma omp for schedule(static)  
		for (int i = 0; (unsigned int)i < p_model->numFoamParticles(); i++)
		{
			if (model->getFoamType(i) == FoamFluidModel_Ihmsen12::FoamType::FOAM)
			{
				Real newFoamLifetime = model->getFoamLifetime(i) - parameters->getFrameDurationTime();
				model->setFoamLifetime(i, newFoamLifetime);
			}

			if (model->getFoamLifetime(i) <= 0.0)
			{
				destroyedIndiziesThread.push_back(i);
			}
		}

		#pragma omp critical
		{
			destroyedIndizies.insert(destroyedIndizies.end(), destroyedIndiziesThread.begin(), destroyedIndiziesThread.end());
		}
	}

	this->m_numberDestroyedParticles = (unsigned int)destroyedIndizies.size();
	model->deleteFoamParticles(destroyedIndizies);
}