#pragma once

#ifndef __PotentialFluid_Ihmsen12_h__
#define __PotentialFluid_Ihmsen12_h__

#include "SPlisHSPlasH\Common.h"
#include <vector>

// type: DO

class PotentialFluid_Ihmsen12
{
public: //enums, structs

public: // constructors, destructors
	PotentialFluid_Ihmsen12();
	virtual ~PotentialFluid_Ihmsen12();

public: // methods
	virtual void reset();
	virtual void resize(unsigned int p_newSize);

public: // setter, getter
	unsigned int getPotentialSize() { return m_currentPotentialSize; }

protected: // methods

protected: // variables
	unsigned int														m_currentPotentialSize;

	std::vector<Real>													m_trappedAirPotential;
	std::vector<Real>													m_waveCrestPotential;
	std::vector<Real>													m_kineticEnergyPotential;

public: // inline methods
	FORCE_INLINE const Real getTrappedAirPotential(const unsigned int i) const
	{
		return m_trappedAirPotential[i];
	}

	FORCE_INLINE Real& getTrappedAirPotential(const unsigned int i)
	{
		return m_trappedAirPotential[i];
	}

	FORCE_INLINE void setTrappedAirPotential(const unsigned int i, const Real &val)
	{
		m_trappedAirPotential[i] = val;
	}

	FORCE_INLINE const Real getWaveCrestPotential(const unsigned int i) const
	{
		return m_waveCrestPotential[i];
	}

	FORCE_INLINE Real& getWaveCrestPotential(const unsigned int i)
	{
		return m_waveCrestPotential[i];
	}

	FORCE_INLINE void setWaveCrestPotential(const unsigned int i, const Real &val)
	{
		m_waveCrestPotential[i] = val;
	}

	FORCE_INLINE const Real getKineticEnergyPotential(const unsigned int i) const
	{
		return m_kineticEnergyPotential[i];
	}

	FORCE_INLINE Real& getKineticEnergyPotential(const unsigned int i)
	{
		return m_kineticEnergyPotential[i];
	}

	FORCE_INLINE void setKineticEnergyPotential(const unsigned int i, const Real &val)
	{
		m_kineticEnergyPotential[i] = val;
	}
};

#endif // !__PotentialFluid_Ihmsen12_h__