#pragma once

#ifndef __OutOfRangeController_Ihmsen12_h__
#define __OutOfRangeController_Ihmsen12_h__

// type: BO

#include "Postprocessing\Foam\Abstract\OutOfRangeController.h"

#include "SPlisHSPlasH\Common.h"

class OutOfRangeController_Ihmsen12 : public OutOfRangeController 
{

public: // constructors, destructors
	OutOfRangeController_Ihmsen12();
	virtual ~OutOfRangeController_Ihmsen12();

public: // methods
	virtual void processOutOfRangeParticles(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter
	virtual void setDefaultValues();
	virtual void setDefaultValues(Real p_foamRadius);

	virtual void setMinXvalue(Real p_val) { minXvalue = p_val; }
	virtual void setMinYvalue(Real p_val) { minYvalue = p_val; }
	virtual void setMinZvalue(Real p_val) { minZvalue = p_val; }
	virtual void setMaxXvalue(Real p_val) { maxXvalue = p_val; }
	virtual void setMaxYvalue(Real p_val) { maxYvalue = p_val; }
	virtual void setMaxZvalue(Real p_val) { maxZvalue = p_val; }

	virtual Real getMinXvalue() { return minXvalue; }
	virtual Real getMinYvalue() { return minYvalue; }
	virtual Real getMinZvalue() { return minZvalue; }
	virtual Real getMaxXvalue() { return maxXvalue; }
	virtual Real getMaxYvalue() { return maxYvalue; }
	virtual Real getMaxZvalue() { return maxZvalue; }

protected: // methods

protected: // variables
	
	Real								minXvalue;
	Real								minYvalue;
	Real								minZvalue;

	Real								maxXvalue;
	Real								maxYvalue;
	Real								maxZvalue;
};

#endif // !__OutOfRangeController_Ihmsen12_h__