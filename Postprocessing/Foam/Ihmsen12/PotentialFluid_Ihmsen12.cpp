#include "PotentialFluid_Ihmsen12.h"

PotentialFluid_Ihmsen12::PotentialFluid_Ihmsen12() :
	m_trappedAirPotential(),
	m_waveCrestPotential(),
	m_kineticEnergyPotential()
{
	this->m_currentPotentialSize = 0;
}

PotentialFluid_Ihmsen12::~PotentialFluid_Ihmsen12()
{
	this->reset();
}

void PotentialFluid_Ihmsen12::reset()
{
	this->m_currentPotentialSize = 0;
	this->m_trappedAirPotential.clear();
	this->m_waveCrestPotential.clear();
	this->m_kineticEnergyPotential.clear();
}

void PotentialFluid_Ihmsen12::resize(unsigned int p_newSize)
{
	this->m_currentPotentialSize = p_newSize;
	this->m_trappedAirPotential.resize(p_newSize);
	this->m_waveCrestPotential.resize(p_newSize);
	this->m_kineticEnergyPotential.resize(p_newSize);
}