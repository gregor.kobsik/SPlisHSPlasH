#pragma once

#ifndef __AdvectionController_Ihmsen12_h__
#define __AdvectionController_Ihmsen12_h__

// type: BO

#include "Postprocessing\Foam\Abstract\AdvectionController.h"

#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"

class AdvectionController_Ihmsen12 : public AdvectionController
{
public: // constructors, destructors
	AdvectionController_Ihmsen12();
	virtual ~AdvectionController_Ihmsen12();

public: // methods
	virtual void advectParticles(FoamFluidModel *p_model, FoamParameters *p_parameters);

protected: // methods
	virtual void computeFoamType(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);
	virtual void computeFoamVelocities(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);
	virtual void computeFoamPositions(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);

	virtual Vector3r computeFoamVelocitySpray(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);
	virtual Vector3r computeFoamVelocityFoam(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);
	virtual Vector3r computeFoamVelocityBubbles(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);

	virtual Vector3r computeAveragedLocalFluidVelocity(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);
protected: // variables



};


#endif // !__AdvectionController_Ihmsen12_h__