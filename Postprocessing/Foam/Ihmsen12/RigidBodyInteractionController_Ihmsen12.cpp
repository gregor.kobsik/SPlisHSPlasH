#include "RigidBodyInteractionController_Ihmsen12.h"

#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"

RigidBodyInteractionController_Ihmsen12::RigidBodyInteractionController_Ihmsen12() : RigidBodyInteractionController()
{

}

RigidBodyInteractionController_Ihmsen12::~RigidBodyInteractionController_Ihmsen12()
{

}

void RigidBodyInteractionController_Ihmsen12::processRigidBodyInteraction(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	FoamFluidModel_Ihmsen12 *model = reinterpret_cast<FoamFluidModel_Ihmsen12*>(p_model);
	FoamParameters_Ihmsen12 *parameters = reinterpret_cast<FoamParameters_Ihmsen12*>(p_parameters);
}