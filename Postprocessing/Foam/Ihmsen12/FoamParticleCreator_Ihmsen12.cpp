#include "FoamParticleCreator_Ihmsen12.h"

#include "FoamNumberComputer_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"

FoamParticleCreator_Ihmsen12::FoamParticleCreator_Ihmsen12()
{
	this->m_foamNumberComputer = std::unique_ptr<FoamNumberComputer_Ihmsen12>(new FoamNumberComputer_Ihmsen12());
	this->reset();
}

FoamParticleCreator_Ihmsen12::~FoamParticleCreator_Ihmsen12()
{
	this->reset();
}

void FoamParticleCreator_Ihmsen12::createParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	FoamFluidModel_Ihmsen12 *model = reinterpret_cast<FoamFluidModel_Ihmsen12*>(p_model);
	FoamParameters_Ihmsen12 *parameters = reinterpret_cast<FoamParameters_Ihmsen12*>(p_parameters);

	this->reset();
	
	this->m_foamNumberComputer->computeNumberOfNewParticles(model, parameters);
	if (parameters->getCreateFoam())
	{
		this->createNewFoamParticles(model, parameters, this->m_foamNumberComputer.get());
	}
}

void FoamParticleCreator_Ihmsen12::reset()
{
	FoamParticleCreator::reset();

	this->m_newFoamParticlesPosition.clear();
	this->m_newFoamParticlesVelocity.clear();
	this->m_newFoamParticlesLifetime.clear();
	this->m_newFoamParticlesType.clear();
	this->m_newFoamParticlesNeighbourNumber.clear();
}

void FoamParticleCreator_Ihmsen12::createNewFoamParticles(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, FoamNumberComputer_Ihmsen12* p_number)
{
	int numberOfNewParticles = p_number->getNumberOfNewFoamParticles();
	std::vector<Real> numberOfNewParticlesPerFluidParticle = p_number->getNumberOfNewFoamParticlesPerFluidParticle();

	std::vector<unsigned int> parentIndex;

	#pragma omp parallel default(shared)
	{
		std::vector<unsigned int> parendIndexThread;

		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int)p_model->numActiveParticles(); indexFluid++)
		{
			for (unsigned int currentNumberOfNewParticlesPerFluid = 0; currentNumberOfNewParticlesPerFluid < (unsigned int) numberOfNewParticlesPerFluidParticle[indexFluid]; currentNumberOfNewParticlesPerFluid++)
			{
				parendIndexThread.push_back(indexFluid);
			}
		}

		#pragma omp critical
		{
			parentIndex.insert(parentIndex.end(), parendIndexThread.begin(), parendIndexThread.end());
		}
	}

	numberOfNewParticles = (int) parentIndex.size();

	this->m_newFoamParticlesPosition.resize(numberOfNewParticles);
	this->m_newFoamParticlesVelocity.resize(numberOfNewParticles);
	this->m_newFoamParticlesLifetime.resize(numberOfNewParticles);
	this->m_newFoamParticlesType.resize(numberOfNewParticles);
	this->m_newFoamParticlesNeighbourNumber.resize(numberOfNewParticles);

	unsigned int oldNumberOfFoamParticles = p_model->numFoamParticles();
	p_model->resizeFoamParticles(oldNumberOfFoamParticles + numberOfNewParticles);
	
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFoam = 0; indexFoam < (int) numberOfNewParticles; indexFoam++)
		{
			this->createNewRandomizedFoamParticle(indexFoam, &parentIndex[0], p_model, p_parameters);

			unsigned int currentFoamIndex = oldNumberOfFoamParticles + indexFoam;
			p_model->setId(FOAM_PARTICLE_SET, currentFoamIndex, currentFoamIndex);
			p_model->setPosition(FOAM_PARTICLE_SET, currentFoamIndex, this->m_newFoamParticlesPosition[indexFoam]);
			p_model->setPosition0(FOAM_PARTICLE_SET, currentFoamIndex, this->m_newFoamParticlesPosition[indexFoam]);
			p_model->setVelocity(FOAM_PARTICLE_SET, currentFoamIndex, this->m_newFoamParticlesVelocity[indexFoam]);
			p_model->setFoamVelocity0(currentFoamIndex, this->m_newFoamParticlesVelocity[indexFoam]);
			p_model->setFoamLifetime(currentFoamIndex, this->m_newFoamParticlesLifetime[indexFoam]);
			p_model->setFoamType(currentFoamIndex, this->m_newFoamParticlesType[indexFoam]);
			p_model->setFoamNeighbourNumber(currentFoamIndex, this->m_newFoamParticlesNeighbourNumber[indexFoam]);
		}
	}
}


void FoamParticleCreator_Ihmsen12::createNewRandomizedFoamParticle(unsigned int p_indexFoam, unsigned int *p_parentIndizes, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	unsigned int indexFluid = p_parentIndizes[p_indexFoam];

	Vector3r parentPosition = p_model->getPosition(FLUID_PARTICLE_SET, indexFluid);
	Vector3r parentVelocity = p_model->getPosition(FLUID_PARTICLE_SET, indexFluid);
	unsigned int parentNeighbours = p_model->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid);
	
	Vector3r supportVector = Vector3r(1, 0, 0);
	Vector3r topCoordinateVector = parentVelocity.normalized();
	if (supportVector == topCoordinateVector)
		supportVector = Vector3r(0, 1, 0);

	Vector3r rightCoordinateVector = topCoordinateVector.cross(supportVector);
	Vector3r frontCoordinateVector = topCoordinateVector.cross(rightCoordinateVector);

	topCoordinateVector.normalize();
	rightCoordinateVector.normalize();
	frontCoordinateVector.normalize();

	Real randomVar1 = (Real)rand() / (Real)RAND_MAX;
	Real randomVar2 = (Real)rand() / (Real)RAND_MAX;
	Real randomVar3 = (Real)rand() / (Real)RAND_MAX;
	Real randomVar4 = (Real)rand() / (Real)RAND_MAX;

	Real cylinderRadius = p_model->getParticleRadius() * std::sqrt(randomVar1);
	Real azimuth = 2.0 * M_PI * randomVar2;
	Real cylinderHeight = parentVelocity.norm() * randomVar3 * p_parameters->getFrameDurationTime();

	Vector3r randomizedFoamPosition = 
		parentPosition +
		cylinderRadius * std::cos(azimuth) * frontCoordinateVector +
		cylinderRadius * std::sin(azimuth) * rightCoordinateVector +
		cylinderHeight * topCoordinateVector;

	Vector3r randomizedFoamVelocity =
		parentVelocity +
		cylinderRadius * std::cos(azimuth) * frontCoordinateVector +
		cylinderRadius * std::sin(azimuth) * rightCoordinateVector;

	
	FoamFluidModel_Ihmsen12::FoamType foamType = FoamFluidModel_Ihmsen12::BUBBLE;
	if (parentNeighbours < p_parameters->getTypeFoamNeighborhoodTreshhold())
		foamType = FoamFluidModel_Ihmsen12::FOAM;
	if (parentNeighbours < p_parameters->getTypeSprayNeighborhoodTreshhold())
		foamType = FoamFluidModel_Ihmsen12::SPRAY;

	Real randomizedFoamLifetime = p_parameters->getMinimumLifetime() + (p_parameters->getMaximumLifetime() - p_parameters->getMinimumLifetime()) * randomVar4;

	this->m_newFoamParticlesPosition[p_indexFoam] = randomizedFoamPosition;
	this->m_newFoamParticlesVelocity[p_indexFoam] = randomizedFoamVelocity;
	this->m_newFoamParticlesLifetime[p_indexFoam] = randomizedFoamLifetime;
	this->m_newFoamParticlesType[p_indexFoam] = foamType;
	this->m_newFoamParticlesNeighbourNumber[p_indexFoam] = parentNeighbours;

}