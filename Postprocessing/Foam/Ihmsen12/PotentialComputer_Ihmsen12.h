#pragma once

#ifndef __PotentialComputer_Ihmsen12_h__
#define __PotentialComputer_Ihmsen12_h__

// type: BO

#include "SPlisHSPlasH\Common.h"

class FoamFluidModel_Ihmsen12;
class FoamParameters_Ihmsen12;
class PotentialFluid_Ihmsen12;

class PotentialComputer_Ihmsen12
{
public: // constructor, destructor
	PotentialComputer_Ihmsen12();
	virtual ~PotentialComputer_Ihmsen12();

public: // methods
	virtual void computePotential(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, PotentialFluid_Ihmsen12 *p_potential);
	

public: // setter, getter

protected: // methods
	virtual void computePotentialTrappedAir(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, PotentialFluid_Ihmsen12 *p_potential);
	virtual void computePotentialWaveCrest(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, PotentialFluid_Ihmsen12 *p_potential);
	virtual void computePotentialKineticEnergy(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, PotentialFluid_Ihmsen12 *p_potential);

	virtual Real clampFunction(Real p_value, Real p_mininum, Real p_maximum);

protected: // variables


};

#endif // !__PotentialComputer_Ihmsen12_h__