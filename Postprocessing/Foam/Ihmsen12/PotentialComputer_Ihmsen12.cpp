#include "PotentialComputer_Ihmsen12.h"

#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"
#include "PotentialFluid_Ihmsen12.h"

PotentialComputer_Ihmsen12::PotentialComputer_Ihmsen12()
{

}

PotentialComputer_Ihmsen12::~PotentialComputer_Ihmsen12()
{

}

void PotentialComputer_Ihmsen12::computePotential(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 * p_parameters, PotentialFluid_Ihmsen12 *p_potential)
{
	p_potential->resize(p_model->numActiveParticles());

	this->computePotentialTrappedAir(p_model, p_parameters, p_potential);
	this->computePotentialWaveCrest(p_model, p_parameters, p_potential);
	this->computePotentialKineticEnergy(p_model, p_parameters, p_potential);
}

void PotentialComputer_Ihmsen12::computePotentialTrappedAir(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, PotentialFluid_Ihmsen12 *p_potential)
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) p_model->numActiveParticles(); indexFluid++)
		{
			Real scaledVelocityDifference = p_model->getScaledVelocityDifference(indexFluid);
			Real trappedAirPotential = this->clampFunction(scaledVelocityDifference, p_parameters->getTrappedAirMinimumTreshhold(), p_parameters->getTrappedAirMaximumTreshhold());
			p_potential->setTrappedAirPotential(indexFluid, trappedAirPotential);
		}
	}
}

void PotentialComputer_Ihmsen12::computePotentialWaveCrest(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, PotentialFluid_Ihmsen12 *p_potential)
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) p_model->numActiveParticles(); indexFluid++)
		{
			Vector3r particleVelocity = p_model->getVelocity(FLUID_PARTICLE_SET, indexFluid);
			Vector3r normalizedParticleVelocity = particleVelocity.normalized();
			Vector3r particleNormal = p_model->getFluidNormal(indexFluid);

			Real movementInNormalDirection = normalizedParticleVelocity.dot(particleNormal);
			Real waveCrest = p_model->getWaveCrest(indexFluid);
			Real waveCrestPotential = this->clampFunction(waveCrest * movementInNormalDirection, p_parameters->getWaveCrestMinimumTreshhold(), p_parameters->getWaveCrestMaximumTreshhold());
			p_potential->setTrappedAirPotential(indexFluid, waveCrestPotential);
		}
	}
}

void PotentialComputer_Ihmsen12::computePotentialKineticEnergy(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, PotentialFluid_Ihmsen12 *p_potential)
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) p_model->numActiveParticles(); indexFluid++)
		{
			Vector3r particleVelocity = p_model->getVelocity(FLUID_PARTICLE_SET, indexFluid);
			#pragma message ("WARNING: Set mass to 1.0, otherwise does not compute correct results. Fix the issue later. Maybe mass is not initialized properly.")
			Real particleMass = 1.0; // p_model->getMass(indexFluid); ERROR: needs 1.0 as mass, otherwise creates no particles within the proposed kinetic energy treshhold
			Real kineticEnergy = 0.5 * particleMass * particleVelocity.squaredNorm();
			Real kineticEnergyPotential = this->clampFunction(kineticEnergy, p_parameters->getKineticEnergyMinimumTreshhold(), p_parameters->getKineticEnergyMaximumTreshhold());
			p_potential->setKineticEnergyPotential(indexFluid, kineticEnergyPotential);
		}
	}
}


Real PotentialComputer_Ihmsen12::clampFunction(Real p_value, Real p_minimum, Real p_maximum)
{
	Real differenceMaximumMinimum = p_maximum - p_minimum;
	Real minimumValueMinimum = std::min(p_value, p_minimum);
	Real minimumValueMaximum = std::min(p_value, p_maximum);
	return (minimumValueMaximum - minimumValueMinimum) / differenceMaximumMinimum;
}