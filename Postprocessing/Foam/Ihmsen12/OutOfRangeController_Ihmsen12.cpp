#include "OutOfRangeController_Ihmsen12.h"

#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"


OutOfRangeController_Ihmsen12::OutOfRangeController_Ihmsen12() : OutOfRangeController()
{
	this->setDefaultValues();
}

OutOfRangeController_Ihmsen12::~OutOfRangeController_Ihmsen12()
{

}

void OutOfRangeController_Ihmsen12::processOutOfRangeParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	FoamFluidModel_Ihmsen12 *model = reinterpret_cast<FoamFluidModel_Ihmsen12*>(p_model);
	FoamParameters_Ihmsen12 *parameters = reinterpret_cast<FoamParameters_Ihmsen12*>(p_parameters);

	std::vector<unsigned int> outOfRangeIndizies;
	#pragma omp parallel default(shared)
	{
		std::vector<unsigned int> outOfRangeIndiziesThread;
		#pragma omp for schedule(static)  
		for (int indexFoam = 0; indexFoam < (int)model->numFoamParticles(); indexFoam++)
		{
			SPH::Vector3r foamPosition = model->getPosition(FOAM_PARTICLE_SET, indexFoam);

			if (foamPosition.y() < this->getMinYvalue())
			{
				outOfRangeIndiziesThread.push_back(indexFoam);
			}
		}

		#pragma omp critical
		{
			outOfRangeIndizies.insert(outOfRangeIndizies.end(), outOfRangeIndiziesThread.begin(), outOfRangeIndiziesThread.end());
		}
	}

	this->m_numberOutOfRangeParticles = (unsigned int)outOfRangeIndizies.size();
	model->deleteFoamParticles(outOfRangeIndizies);
	outOfRangeIndizies.clear();

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int indexFoam = 0; indexFoam < (int)model->numFoamParticles(); indexFoam++)
		{
			Vector3r foamPosition = model->getPosition(FOAM_PARTICLE_SET, indexFoam);
			Vector3r foamVelocity = model->getVelocity(FOAM_PARTICLE_SET, indexFoam);
			bool reassignFoam = false;

			if (foamPosition.x() < this->getMinXvalue())
			{
				foamPosition.x() = this->getMinXvalue();
				foamVelocity.x() = 0;
				reassignFoam = true;
			}

			if (foamPosition.x() > this->getMaxXvalue())
			{
				foamPosition.x() = this->getMaxXvalue();
				foamVelocity.x() = 0;
				reassignFoam = true;
			}

			if (foamPosition.y() < this->getMinYvalue())
			{
				foamPosition.y() = this->getMinYvalue();
				foamVelocity.y() = 0;
				reassignFoam = true;
			}

			if (foamPosition.y() > this->getMaxYvalue())
			{
				foamPosition.y() = this->getMaxYvalue();
				foamVelocity.y() = 0;
				reassignFoam = true;
			}

			if (foamPosition.z() < this->getMinZvalue())
			{
				foamPosition.z() = this->getMinZvalue();
				foamVelocity.z() = 0;
				reassignFoam = true;
			}

			if (foamPosition.z() > this->getMaxZvalue())
			{
				foamPosition.z() = this->getMaxZvalue();
				foamVelocity.z() = 0;
				reassignFoam = true;
			}

			if (reassignFoam)
			{
				model->setPosition(FOAM_PARTICLE_SET, indexFoam, foamPosition);
				model->setPosition0(FOAM_PARTICLE_SET, indexFoam, foamPosition);
				model->setVelocity(FOAM_PARTICLE_SET, indexFoam, foamVelocity);
				model->setFoamVelocity0(indexFoam, foamVelocity);
			}
		}
	}
}

void OutOfRangeController_Ihmsen12::setDefaultValues()
{
	Vector3r defaultUnitBox = Vector3r(4, 6, 1.5);

	this->setMinXvalue(-defaultUnitBox.x() / 2.0);
	this->setMinYvalue(0.0);
	this->setMinZvalue(-defaultUnitBox.z() / 2.0);

	this->setMaxXvalue(defaultUnitBox.x() / 2.0);
	this->setMaxYvalue(10.0);
	this->setMaxZvalue(defaultUnitBox.z() / 2.0);
}

void OutOfRangeController_Ihmsen12::setDefaultValues(Real p_foamRadius)
{
	this->setDefaultValues();

	this->setMinXvalue(this->getMinXvalue() + 2*p_foamRadius);
	this->setMinYvalue(this->getMinYvalue() + 2*p_foamRadius);
	this->setMinZvalue(this->getMinZvalue() + 2*p_foamRadius);

	this->setMaxXvalue(this->getMaxXvalue() - 2*p_foamRadius);
	this->setMaxYvalue(this->getMaxYvalue() - 2*p_foamRadius);
	this->setMaxZvalue(this->getMaxZvalue() - 2*p_foamRadius);
}
