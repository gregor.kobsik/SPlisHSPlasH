#pragma once

#ifndef __LifetimeController_Ihmsen12_h__
#define __LifetimeController_Ihmsen12_h__

// type: BO

#include "Postprocessing\Foam\Abstract\LifetimeController.h"

class LifetimeController_Ihmsen12 : public LifetimeController {

public: // constructors, destructors
	LifetimeController_Ihmsen12();
	virtual ~LifetimeController_Ihmsen12();

public: // methods
	virtual void processLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter

protected: // methods

protected: // variables

};

#endif // !__LifetimeController_Ihmsen12_h__