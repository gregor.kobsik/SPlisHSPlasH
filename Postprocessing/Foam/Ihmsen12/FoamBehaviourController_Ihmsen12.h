#pragma once

#ifndef __FoamBehaviourController_Ihmsen12_h__
#define __FoamBehaviourController_Ihmsen12_h__

// type: BO

#include "Postprocessing\Foam\Abstract\FoamBehaviourController.h"

class FoamBehaviourController_Ihmsen12 : public FoamBehaviourController
{

public: // constructors, destructors
	FoamBehaviourController_Ihmsen12();
	virtual ~FoamBehaviourController_Ihmsen12();

protected: // methods
	virtual void progressAdvection(FoamFluidModel *p_model, FoamParameters *p_parameters);
	virtual void progressLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters);
	virtual void progressOutOfRange(FoamFluidModel *p_model, FoamParameters *p_parameters);
	virtual void progressRigidBodyInteraction(FoamFluidModel *p_model, FoamParameters *p_parameters);

protected: // methods

protected: //variables

};

#endif // !__FoamBehaviourController_Ihmsen12_h__