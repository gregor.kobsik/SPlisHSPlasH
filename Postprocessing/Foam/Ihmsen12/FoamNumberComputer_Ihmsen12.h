#pragma once

#ifndef __FoamNumberComputer_Ihmsen12_h__
#define __FoamNumberComputer_Ihmsen12_h__

// type: BO

#include "SPlisHSPlasH\Common.h"
#include <vector>
#include <memory>

#include "Postprocessing\Foam\Abstract\FoamNumberComputer.h"

class FoamFluidModel_Ihmsen12;
class FoamParameters_Ihmsen12;
class PotentialComputer_Ihmsen12;
class PotentialFluid_Ihmsen12;

class FoamNumberComputer_Ihmsen12 : public FoamNumberComputer
{
public: // constructor, destructor
	FoamNumberComputer_Ihmsen12();
	virtual ~FoamNumberComputer_Ihmsen12();

public: // methods
	virtual void computeNumberOfNewParticles(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter
	

protected: //methods
	virtual void reset();

	void evaluatePotential(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);

protected: // variables
	std::unique_ptr<PotentialComputer_Ihmsen12>										m_potentialComputer = nullptr;
	std::unique_ptr<PotentialFluid_Ihmsen12>										m_potential = nullptr;

};

#endif // !__FoamNumberComputer_Ihmsen12_h__