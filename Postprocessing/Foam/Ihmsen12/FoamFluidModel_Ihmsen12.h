#pragma once
#ifndef __FoamFluidModel_Ihmsen12_h__
#define __FoamFluidModel_Ihmsen12_h__

// type: DO

#include "Postprocessing\Foam\FoamFluidModel.h"

class FoamFluidModel_Ihmsen12 : public FoamFluidModel
{
public: // enums, structs
	enum FoamType { SPRAY, FOAM, BUBBLE};

public: // constructors, destructors
	FoamFluidModel_Ihmsen12();
	virtual ~FoamFluidModel_Ihmsen12();

public: // methods
	virtual void deleteFoamParticles(std::vector<unsigned int> indizies);
	virtual void resizeFluidParticles(const unsigned int newSize);
	virtual void resizeFoamParticles(const unsigned int newSize);

	virtual void performNeighborhoodSearchSort();

	void computeColorField();
	void computeFluidNormals();
	void computeSurfaceCurvature();
	void computeWaveCrest();
	void computeScaledVelocityDifference();

	unsigned int getNumFoamParticles0() { return m_numFoamParticles0; }
	unsigned int getNumFoamParticles0(FoamType p_type);

protected: // methods
	virtual void releaseFluidParticles();	
	virtual void releaseFoamParticles();

	Real radialSymmetricWeightingFunction(Vector3r p_vector);

protected: //variables
	std::vector<Real>			m_colorField;
	std::vector<Vector3r>		m_gradColorField;
	std::vector<Vector3r>		m_fluidNormal;
	std::vector<Real>			m_surfaceCurvature;
	std::vector<Real>			m_waveCrest;
	std::vector<Real>			m_scaledVelocityDifference;

	std::vector<Real>			m_foamLifetime;
	std::vector<FoamType>		m_foamType;
	std::vector<unsigned int>	m_foamNumberOfNeighbours;

public: // inline methods
	FORCE_INLINE const Real getColorField(const unsigned int i) const
	{
		return m_colorField[i];
	}

	FORCE_INLINE Real& getColorField(const unsigned int i)
	{
		return m_colorField[i];
	}

	FORCE_INLINE void setColorField(const unsigned int i, const Real &val)
	{
		m_colorField[i] = val;
	}

	FORCE_INLINE const Vector3r getGradColorField(const unsigned int i) const
	{
		return m_gradColorField[i];
	}

	FORCE_INLINE Vector3r& getGradColorField(const unsigned int i)
	{
		return m_gradColorField[i];
	}

	FORCE_INLINE void setGradColorField(const unsigned int i, const Vector3r &val)
	{
		m_gradColorField[i] = val;
	}

	FORCE_INLINE const Vector3r getFluidNormal(const unsigned int i) const
	{
		return m_fluidNormal[i];
	}

	FORCE_INLINE Vector3r& getFluidNormal(const unsigned int i)
	{
		return m_fluidNormal[i];
	}

	FORCE_INLINE void setFluidNormal(const unsigned int i, const Vector3r &val)
	{
		m_fluidNormal[i] = val;
	}

	FORCE_INLINE const Real getSurfaceCurvature(const unsigned int i) const
	{
		return m_surfaceCurvature[i];
	}

	FORCE_INLINE Real& getSurfaceCurvature(const unsigned int i)
	{
		return m_surfaceCurvature[i];
	}

	FORCE_INLINE void setSurfaceCurvature(const unsigned int i, const Real &val)
	{
		m_surfaceCurvature[i] = val;
	}

	FORCE_INLINE const Real getWaveCrest(const unsigned int i) const
	{
		return m_waveCrest[i];
	}

	FORCE_INLINE Real& getWaveCrest(const unsigned int i)
	{
		return m_waveCrest[i];
	}

	FORCE_INLINE void setWaveCrest(const unsigned int i, const Real &val)
	{
		m_waveCrest[i] = val;
	}

	FORCE_INLINE const Real getScaledVelocityDifference(const unsigned int i) const
	{
		return m_scaledVelocityDifference[i];
	}

	FORCE_INLINE Real& getScaledVelocityDifference(const unsigned int i)
	{
		return m_scaledVelocityDifference[i];
	}

	FORCE_INLINE void setScaledVelocityDifference(const unsigned int i, const Real &val)
	{
		m_scaledVelocityDifference[i] = val;
	}

	FORCE_INLINE const Real getFoamLifetime(const unsigned int i) const
	{
		return m_foamLifetime[i];
	}

	FORCE_INLINE Real& getFoamLifetime(const unsigned int i)
	{
		return m_foamLifetime[i];
	}

	FORCE_INLINE void setFoamLifetime(const unsigned int i, const Real &val)
	{
		m_foamLifetime[i] = val;
	}

	FORCE_INLINE const FoamType getFoamType(const unsigned int i) const
	{
		return m_foamType[i];
	}

	FORCE_INLINE FoamType& getFoamType(const unsigned int i)
	{
		return m_foamType[i];
	}

	FORCE_INLINE void setFoamType(const unsigned int i, const FoamType &val)
	{
		m_foamType[i] = val;
	}

	FORCE_INLINE const unsigned int getFoamNeighbourNumber(const unsigned int i) const
	{
		return m_foamNumberOfNeighbours[i];
	}

	FORCE_INLINE unsigned int& getFoamNeighbourNumber(const unsigned int i)
	{
		return m_foamNumberOfNeighbours[i];
	}

	FORCE_INLINE void setFoamNeighbourNumber(const unsigned int i, const unsigned int &val)
	{
		m_foamNumberOfNeighbours[i] = val;
	}
};

#endif // !__FoamFluidModel_Ihmsen12_h__