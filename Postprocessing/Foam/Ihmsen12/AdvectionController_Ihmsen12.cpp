#include "AdvectionController_Ihmsen12.h"

#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"

AdvectionController_Ihmsen12::AdvectionController_Ihmsen12() : AdvectionController()
{

}

AdvectionController_Ihmsen12::~AdvectionController_Ihmsen12()
{

}

void AdvectionController_Ihmsen12::advectParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	FoamFluidModel_Ihmsen12 *model = reinterpret_cast<FoamFluidModel_Ihmsen12*>(p_model);
	FoamParameters_Ihmsen12 *parameters = reinterpret_cast<FoamParameters_Ihmsen12*>(p_parameters);

	this->computeFoamType(model, parameters);
	this->computeFoamVelocities(model, parameters);
	this->computeFoamPositions(model, parameters);
}

void AdvectionController_Ihmsen12::computeFoamType(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	CompactNSearch::NeighborhoodSearch* nSearch = p_model->getNeighborhoodSearch();
	CompactNSearch::PointSet& psFoam = nSearch->point_set(FOAM_PARTICLE_SET);

	int sprayNum = 0;
	int foamNum = 0;
	int bubbleNum = 0;

	#pragma omp parallel default(shared)
	{		

		#pragma omp for schedule(static) 
		for (int i = 0; i < (int) p_model->numFoamParticles(); i++)
		{
			
			unsigned int numberOfNeighbours = (unsigned int) psFoam.n_neighbors(FLUID_PARTICLE_SET, i);
			p_model->setFoamNeighbourNumber(i, numberOfNeighbours);
			if (numberOfNeighbours <= p_parameters->getTypeSprayNeighborhoodTreshhold())
			{
				p_model->setFoamType(i, FoamFluidModel_Ihmsen12::SPRAY);
			}
			else if (numberOfNeighbours <= p_parameters->getTypeFoamNeighborhoodTreshhold())
			{
				p_model->setFoamType(i, FoamFluidModel_Ihmsen12::FOAM);
			}
			else
			{
				p_model->setFoamType(i, FoamFluidModel_Ihmsen12::BUBBLE);
			}
		}
	}
}

void AdvectionController_Ihmsen12::computeFoamVelocities(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	std::vector<Vector3r> newFoamVelocities;
	newFoamVelocities.resize(p_model->numFoamParticles());

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int i = 0; (unsigned int)i < p_model->numFoamParticles(); i++)
		{
			switch (p_model->getFoamType(i))
			{
			case FoamFluidModel_Ihmsen12::SPRAY:
				newFoamVelocities[i] = this->computeFoamVelocitySpray(i, p_model, p_parameters);
				break;
			case FoamFluidModel_Ihmsen12::FOAM:
				newFoamVelocities[i] = this->computeFoamVelocityFoam(i, p_model, p_parameters);
				break;
			case FoamFluidModel_Ihmsen12::BUBBLE:
				newFoamVelocities[i] = this->computeFoamVelocityBubbles(i, p_model, p_parameters);
				break;
			}
		}

		#pragma omp for schedule(static)
		for (int i = 0; (unsigned int)i < p_model->numFoamParticles(); i++)
		{
			p_model->setVelocity(FOAM_PARTICLE_SET, i, newFoamVelocities[i]);
			p_model->setFoamVelocity0(i, newFoamVelocities[i]);
		}

	}
}

void AdvectionController_Ihmsen12::computeFoamPositions(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int i = 0; (unsigned int) i < p_model->numFoamParticles(); i++)
		{
			Vector3r oldFoamPosition = p_model->getPosition(FOAM_PARTICLE_SET, i);
			Vector3r newFoamPosition = oldFoamPosition + p_model->getVelocity(FOAM_PARTICLE_SET, i) * p_parameters->getFrameDurationTime();
			p_model->setPosition(FOAM_PARTICLE_SET, i, newFoamPosition);
			p_model->setPosition0(FOAM_PARTICLE_SET, i, newFoamPosition);
		}
	}
}

Vector3r AdvectionController_Ihmsen12::computeFoamVelocitySpray(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	return  p_model->getVelocity(FOAM_PARTICLE_SET, p_index) + p_model->getGravitation() * p_parameters->getFrameDurationTime();
}

Vector3r AdvectionController_Ihmsen12::computeFoamVelocityFoam(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	return this->computeAveragedLocalFluidVelocity(p_index, p_model, p_parameters);
}

Vector3r AdvectionController_Ihmsen12::computeFoamVelocityBubbles(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	Vector3r particleVelocity = p_model->getVelocity(FOAM_PARTICLE_SET, p_index);
	Vector3r averagedLocalFluidVelocity = this->computeAveragedLocalFluidVelocity(p_index, p_model, p_parameters);
	Vector3r localVelocityDifference = averagedLocalFluidVelocity - particleVelocity;
	Vector3r dragVelocityDifference = localVelocityDifference * p_parameters->getBubbleDragCoefficient();
	Vector3r boyouncyVelocity = -p_model->getGravitation() * p_parameters->getBubbleBuoyancyCoefficient() * p_parameters->getFrameDurationTime();
	Vector3r newParticleVelocity = particleVelocity + boyouncyVelocity + dragVelocityDifference;
	return newParticleVelocity;
}

Vector3r AdvectionController_Ihmsen12::computeAveragedLocalFluidVelocity(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	CompactNSearch::PointSet& psFoam = p_model->getNeighborhoodSearch()->point_set(FOAM_PARTICLE_SET);
	unsigned int nNeighbors = (unsigned int) psFoam.n_neighbors(FLUID_PARTICLE_SET, p_index);

	Real kernelSum = 0.0;
	Vector3r kernelVelocitySum = Vector3r(0.0, 0.0, 0.0);

	Vector3r particlePosition = p_model->getPosition(FOAM_PARTICLE_SET, p_index);

	for (unsigned int i = 0; i < nNeighbors; i++)
	{
		Vector3r neighbourPosition = p_model->getPosition(FLUID_PARTICLE_SET, psFoam.neighbor(FLUID_PARTICLE_SET, p_index, i));
		Vector3r neighbourVelocity = p_model->getVelocity(FLUID_PARTICLE_SET, psFoam.neighbor(FLUID_PARTICLE_SET, p_index, i));
		Vector3r positionDifference = particlePosition - neighbourPosition;
		Real w = p_model->W(positionDifference);

		kernelSum += w;
		kernelVelocitySum += neighbourVelocity * w;
	}

	Vector3r averagedLocalFluidVelocity = kernelVelocitySum / kernelSum;
	return averagedLocalFluidVelocity;
}