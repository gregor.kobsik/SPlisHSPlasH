#include "FoamFluidModel_Ihmsen12.h"

FoamFluidModel_Ihmsen12::FoamFluidModel_Ihmsen12() :
	FoamFluidModel(),
	m_colorField(),
	m_gradColorField(),
	m_fluidNormal(),
	m_surfaceCurvature(),
	m_waveCrest(),
	m_scaledVelocityDifference(),
	m_foamLifetime(),
	m_foamType(),
	m_foamNumberOfNeighbours()
{

}

FoamFluidModel_Ihmsen12::~FoamFluidModel_Ihmsen12()
{

}

void FoamFluidModel_Ihmsen12::deleteFoamParticles(std::vector<unsigned int> indizies)
{
	unsigned int lastElement = getNumFoamParticles0() - 1;

	for (unsigned int i = 0; i < indizies.size(); i++) {
		unsigned int indexFoam = indizies[i];
		if (indexFoam <= lastElement)
		{
			m_particleObjects[FOAM_PARTICLE_SET]->m_x0[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_x0[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_x[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_x[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_v[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_v[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_id[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_id[lastElement];
			m_vFoam0[indexFoam] = m_vFoam0[lastElement];
			m_foamLifetime[indexFoam] = m_foamLifetime[lastElement];
			m_foamType[indexFoam] = m_foamType[lastElement];
			m_foamNumberOfNeighbours[indexFoam] = m_foamNumberOfNeighbours[lastElement];

			lastElement--;
		}
	}
	resizeFoamParticles(lastElement + 1);
}

void FoamFluidModel_Ihmsen12::resizeFluidParticles(const unsigned int newSize)
{
	FoamFluidModel::resizeFluidParticles(newSize);
	this->m_colorField.resize(newSize);
	this->m_gradColorField.resize(newSize);
	this->m_fluidNormal.resize(newSize);
	this->m_surfaceCurvature.resize(newSize);
	this->m_waveCrest.resize(newSize);
	this->m_scaledVelocityDifference.resize(newSize);
}

void FoamFluidModel_Ihmsen12::resizeFoamParticles(const unsigned int newSize)
{
	FoamFluidModel::resizeFoamParticles(newSize);
	this->m_foamLifetime.resize(newSize);
	this->m_foamType.resize(newSize);
	this->m_foamNumberOfNeighbours.resize(newSize);
}

void FoamFluidModel_Ihmsen12::performNeighborhoodSearchSort()
{
	FoamFluidModel::performNeighborhoodSearchSort();
	// Fluid
	auto const& d = this->m_neighborhoodSearch->point_set(FLUID_PARTICLE_SET);
	if (d.n_points() > 0) 
	{
		d.sort_field(&this->m_fluidNormal[0]);
		d.sort_field(&this->m_surfaceCurvature[0]);
		d.sort_field(&this->m_waveCrest[0]);
		d.sort_field(&this->m_scaledVelocityDifference[0]);
	}

	// Foam
	auto const& e = this->m_neighborhoodSearch->point_set(FOAM_PARTICLE_SET);
	if (e.n_points() > 0) 
	{
		e.sort_field(&this->m_foamLifetime[0]);
		e.sort_field(&this->m_foamType[0]);
		e.sort_field(&this->m_foamNumberOfNeighbours[0]);
	}
}

void FoamFluidModel_Ihmsen12::computeColorField() 
{
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int indexFluid = 0; indexFluid < (int)numActiveParticles(); indexFluid++) {
			Real fluidColor = 0;
			Vector3r fluidGradColor = Vector3r(0, 0, 0);
			Vector3r particlePosition = getPosition(FLUID_PARTICLE_SET, indexFluid);
			for (int counterNeighbour = 0; counterNeighbour < (int) this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++) {

				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Real weightedPositionDifference = this->getMass(indexNeighbour) * this->W(particlePosition - neighbourPosition);
				Vector3r weightedPositionDifferenceGrad = this->getMass(indexNeighbour) * this->gradW(particlePosition - neighbourPosition);

				fluidColor -= fluidColor;
				fluidGradColor -= weightedPositionDifferenceGrad;
			}
			this->setColorField(indexFluid, fluidColor);
			this->setGradColorField(indexFluid, fluidGradColor);
		}
	}
}

void FoamFluidModel_Ihmsen12::computeFluidNormals() 
{
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int indexFluid = 0; indexFluid < (int)numActiveParticles(); indexFluid++) {
			Vector3r fluidNormal = Vector3r(0, 0, 0);
			Vector3r particlePosition = getPosition(FLUID_PARTICLE_SET, indexFluid);
			for (int counterNeighbour = 0; counterNeighbour < (int) this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++) {

				// ERROR: density = 0 at beginning of simulation.
				// correct formula: 
				// Vector3r c_ij = getMass(k) / getDensity(k) * gradW(getPosition(0, i) - getPosition(0, k));

				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Vector3r weightedPositionDifference = this->getMass(indexNeighbour) * this->gradW(particlePosition - neighbourPosition);
				fluidNormal -= weightedPositionDifference;
			}
			fluidNormal.normalize();
			this->setFluidNormal(indexFluid, fluidNormal);
		}
	}
}

void FoamFluidModel_Ihmsen12::computeSurfaceCurvature()
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) this->numActiveParticles(); indexFluid++)
		{
			Real surfaceCurvature = 0;
			Vector3r particlePosition = this->getPosition(FLUID_PARTICLE_SET, indexFluid);
			Vector3r particleNormal = this->getFluidNormal(indexFluid);

			for (int counterNeighbour = 0; counterNeighbour < (int) this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++)
			{
				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Vector3r positionDifference = particlePosition - neighbourPosition;
				Vector3r neighbourNormal = this->getFluidNormal(indexNeighbour);

				surfaceCurvature += (1.0 - particleNormal.dot(neighbourNormal)) * this->W(positionDifference);
			}

			this->setSurfaceCurvature(indexFluid, surfaceCurvature);
		}
	}
}

void FoamFluidModel_Ihmsen12::computeWaveCrest()
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) this->numActiveParticles(); indexFluid++)
		{
			Real waveCrest = 0;
			Vector3r particlePosition = getPosition(FLUID_PARTICLE_SET, indexFluid);
			Vector3r particleNormal = getFluidNormal(indexFluid);

			for (int counterNeighbour = 0; counterNeighbour < (int) this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++)
			{
				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Vector3r positionDifference = particlePosition - neighbourPosition;
				Vector3r normalizedPositonDifference = positionDifference.normalized();
				Vector3r neighbourNormal = this->getFluidNormal(indexNeighbour);

				if ((-normalizedPositonDifference).dot(particleNormal) < 0.0)
				{
					waveCrest += (1.0 - particleNormal.dot(neighbourNormal)) * this->W(positionDifference);
				}
			}

			this->setWaveCrest(indexFluid, waveCrest);
		}
	}
}

void FoamFluidModel_Ihmsen12::computeScaledVelocityDifference()
{
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) this->numActiveParticles(); indexFluid++)
		{
			Vector3r particlePosition = this->getPosition(FLUID_PARTICLE_SET, indexFluid);
			Vector3r particleVelocity = this->getVelocity(FLUID_PARTICLE_SET, indexFluid);
			Real scaledVelocityDifference = 0;

			for (unsigned int counterNeighbour = 0; counterNeighbour < this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++)
			{
				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Vector3r neighbourVelocity = this->getVelocity(FLUID_PARTICLE_SET, indexNeighbour);

				Vector3r particleNeighbourPositionDifference = particlePosition - neighbourPosition;
				Vector3r particleNeighbourVelocityDifference = particleVelocity - neighbourVelocity;

				Vector3r normalizedPositionDifference = particleNeighbourPositionDifference.normalized();
				Vector3r normalizedVelocityDifference = particleNeighbourVelocityDifference.normalized();
				Real magnitudeVelocityDifference = particleNeighbourVelocityDifference.norm();

				scaledVelocityDifference += magnitudeVelocityDifference * (1.0 - normalizedVelocityDifference.dot(normalizedPositionDifference))
					* this->radialSymmetricWeightingFunction(particleNeighbourPositionDifference);
			}

			this->setScaledVelocityDifference(indexFluid, scaledVelocityDifference);
		}
	}
}

unsigned int FoamFluidModel_Ihmsen12::getNumFoamParticles0(FoamType p_type)
{
	unsigned int sum = 0;
	#pragma omp parallel default(shared) 
	{
		unsigned int sumThread = 0;
		#pragma omp for schedule(static)
		for (int i = 0; i < (int) this->getNumFoamParticles0(); i++)
		{
			if (m_foamType[i] == p_type)
			{
				sumThread++;
			}
		}

		#pragma omp critical
		{
			sum += sumThread;
		}
	}

	return sum;
}

void FoamFluidModel_Ihmsen12::releaseFluidParticles()
{
	FoamFluidModel::releaseFluidParticles();
	this->m_colorField.clear();
	this->m_gradColorField.clear();
	this->m_fluidNormal.clear();
	this->m_surfaceCurvature.clear();
	this->m_waveCrest.clear();
	this->m_scaledVelocityDifference.clear();
}

void FoamFluidModel_Ihmsen12::releaseFoamParticles()
{
	FoamFluidModel::releaseFoamParticles();
	this->m_foamLifetime.clear();
	this->m_foamType.clear();
	this->m_foamNumberOfNeighbours.clear();
}

Real FoamFluidModel_Ihmsen12::radialSymmetricWeightingFunction(Vector3r p_vector)
{
	if (p_vector.norm() <= this->m_supportRadius)
	{
		return (1.0 - p_vector.norm() / this->m_supportRadius);
	}
	else
	{
		return 0.0;
	}
}