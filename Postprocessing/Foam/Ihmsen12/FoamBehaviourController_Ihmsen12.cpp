#include "FoamBehaviourController_Ihmsen12.h"

#include "AdvectionController_Ihmsen12.h"
#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"
#include "LifetimeController_Ihmsen12.h"
#include "OutOfRangeController_Ihmsen12.h"
#include "RigidBodyInteractionController_Ihmsen12.h"

FoamBehaviourController_Ihmsen12::FoamBehaviourController_Ihmsen12() : FoamBehaviourController()
{
	this->m_advectionController				= std::make_unique<AdvectionController_Ihmsen12>(AdvectionController_Ihmsen12());
	this->m_lifetimeController				= std::make_unique<LifetimeController_Ihmsen12>(LifetimeController_Ihmsen12());
	this->m_outOfRangeController			= std::make_unique<OutOfRangeController_Ihmsen12>(OutOfRangeController_Ihmsen12());
	this->m_rigidBodyInteractionController	= std::make_unique<RigidBodyInteractionController_Ihmsen12>(RigidBodyInteractionController_Ihmsen12());
}

FoamBehaviourController_Ihmsen12::~FoamBehaviourController_Ihmsen12()
{

}

void FoamBehaviourController_Ihmsen12::progressAdvection(FoamFluidModel *p_model, FoamParameters *p_parameters) 
{
	this->m_advectionController->advectParticles(p_model, p_parameters);
}

void FoamBehaviourController_Ihmsen12::progressLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	this->m_lifetimeController->processLifetime(p_model, p_parameters);
}

void FoamBehaviourController_Ihmsen12::progressOutOfRange(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	Real foamRadius = p_model->getFoamRadius();
	reinterpret_cast<OutOfRangeController_Ihmsen12*>(this->m_outOfRangeController.get())->setDefaultValues(foamRadius);;
	this->m_outOfRangeController->processOutOfRangeParticles(p_model, p_parameters);
}

void FoamBehaviourController_Ihmsen12::progressRigidBodyInteraction(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	this->m_rigidBodyInteractionController->processRigidBodyInteraction(p_model, p_parameters);
}