#include "FoamNumberComputer_Ihmsen12.h"

#include "FoamFluidModel_Ihmsen12.h"
#include "FoamParameters_Ihmsen12.h"
#include "PotentialComputer_Ihmsen12.h"
#include "PotentialFluid_Ihmsen12.h"

FoamNumberComputer_Ihmsen12::FoamNumberComputer_Ihmsen12() :
	FoamNumberComputer()
{
	this->m_potentialComputer = std::make_unique<PotentialComputer_Ihmsen12>(PotentialComputer_Ihmsen12());
	this->m_potential = std::make_unique<PotentialFluid_Ihmsen12>(PotentialFluid_Ihmsen12());
	reset();
}

FoamNumberComputer_Ihmsen12::~FoamNumberComputer_Ihmsen12()
{
	reset();
}

void FoamNumberComputer_Ihmsen12::computeNumberOfNewParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	FoamFluidModel_Ihmsen12 *model = reinterpret_cast<FoamFluidModel_Ihmsen12*>(p_model);
	FoamParameters_Ihmsen12 *parameters = reinterpret_cast<FoamParameters_Ihmsen12*>(p_parameters);

	this->reset();

	model->computeFluidNormals();
	model->computeSurfaceCurvature();
	model->computeWaveCrest();
	model->computeScaledVelocityDifference();

	this->m_potentialComputer->computePotential(model, parameters, this->m_potential.get());
	
	if (parameters->getCreateFoam())
	{
		this->evaluatePotential(model, parameters);
	}
	
}

void FoamNumberComputer_Ihmsen12::reset()
{
	FoamNumberComputer::reset();
	this->m_potential->reset();
}

void FoamNumberComputer_Ihmsen12::evaluatePotential(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	this->m_numberOfNewFoamParticlesPerFluidParticle.resize(p_model->numActiveParticles());

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int)p_model->numActiveParticles(); indexFluid++)
		{
			Real numberOfNewFoamParticles = 0;
			numberOfNewFoamParticles += p_parameters->getTrappedAirCoefficient() * this->m_potential->getTrappedAirPotential(indexFluid);
			numberOfNewFoamParticles += p_parameters->getWaveCrestCoefficient() * this->m_potential->getWaveCrestPotential(indexFluid);
			numberOfNewFoamParticles *= this->m_potential->getKineticEnergyPotential(indexFluid);
			numberOfNewFoamParticles *= p_parameters->getFrameDurationTime();

			this->m_numberOfNewFoamParticlesPerFluidParticle[indexFluid] = numberOfNewFoamParticles;
		}
	}

	for (unsigned int indexFluid = 0; indexFluid < this->m_numberOfNewFoamParticlesPerFluidParticle.size(); indexFluid++)
	{
		this->m_numberOfNewFoamParticles += (unsigned int) this->m_numberOfNewFoamParticlesPerFluidParticle[indexFluid];
	}

}