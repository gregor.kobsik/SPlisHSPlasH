#include "FoamParameters_Ihmsen12.h"

FoamParameters_Ihmsen12::FoamParameters_Ihmsen12() : FoamParameters()
{
	setDefaultParameters();
}

FoamParameters_Ihmsen12::~FoamParameters_Ihmsen12()
{

}

void FoamParameters_Ihmsen12::setDefaultParameters()
{
	this->setCreateFoam(true);
	this->setFramesPerSecond(50.0);

	this->setTrappedAirMinimumTreshhold(5.0);
	this->setTrappedAirMaximumTreshhold(20.0);

	this->setWaveCrestMinimumTreshhold(2.0);
	this->setWaveCrestMaximumTreshhold(8.0);

	this->setKineticEnergyMinimumTreshhold(5.0);
	this->setKineticEnergyMaximumTreshhold(50.0);

	this->setTrappedAirCoefficient(1000.0);
	this->setWaveCrestCoefficient(1000.0);
	this->setBubbleBuoyancyCoefficient(0.5);
	this->setBubbleDragCoefficient(0.5);

	this->setTypeSprayNeighborhoodTreshhold(6u);
	this->setTypeFoamNeighborhoodTreshhold(20u);

	this->setMinimumLifetime(1.0);
	this->setMaximumLifetime(7.0);
}