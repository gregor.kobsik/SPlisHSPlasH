#pragma once

#ifndef __RigidBodyInteractionController_Ihmsen12_h__
#define __RigidBodyInteractionController_Ihmsen12_h__

// type: BO

#include "Postprocessing/Foam/Abstract/RigidBodyInteractionController.h"

class RigidBodyInteractionController_Ihmsen12 : public RigidBodyInteractionController {

public: // constructors, destructors
	RigidBodyInteractionController_Ihmsen12();
	virtual ~RigidBodyInteractionController_Ihmsen12();

public: // methods
	virtual void processRigidBodyInteraction(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter

protected: // methods

protected: // variables

};

#endif // !__RigidBodyInteractionController_Ihmsen12_h__