#pragma once

#ifndef __FoamParticleCreator_Ihmsen12_h__
#define __FoamParticleCreator_Ihmsen12_h__

// type: BO

#include "Postprocessing\Foam\Abstract\FoamParticleCreator.h"
#include "FoamFluidModel_Ihmsen12.h"

class FoamNumberComputer_Ihmsen12;
class FoamParameters_Ihmsen12;

class FoamParticleCreator_Ihmsen12 : public FoamParticleCreator 
{
public: // constructor, destructor
	FoamParticleCreator_Ihmsen12();
	virtual ~FoamParticleCreator_Ihmsen12();

public: // methods
	virtual void createParticles(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter

protected: //methods
	virtual void reset();
	
	virtual void createNewFoamParticles(FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters, FoamNumberComputer_Ihmsen12 *p_number);
	virtual void createNewRandomizedFoamParticle(unsigned int p_indexFoam, unsigned int *p_parentIndizes, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);

protected: // variables
	std::unique_ptr<FoamNumberComputer_Ihmsen12>						m_foamNumberComputer = nullptr;

	std::vector<Vector3r>												m_newFoamParticlesPosition;
	std::vector<Vector3r>												m_newFoamParticlesVelocity;
	std::vector<Real>													m_newFoamParticlesLifetime;
	std::vector<FoamFluidModel_Ihmsen12::FoamType>						m_newFoamParticlesType;
	std::vector<unsigned int>											m_newFoamParticlesNeighbourNumber;
	

};

#endif // !__FoamParticleCreator_Ihmsen12_h__