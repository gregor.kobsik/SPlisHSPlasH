#pragma once

#ifndef __FoamParameters_Ihmsen12_h__
#define __FoamParameters_Ihmsen12_h__

// type: DO

#include "SPlisHSPlasH\Common.h"
#include "Postprocessing\Foam\Abstract\FoamParameters.h"

class FoamParameters_Ihmsen12 : public FoamParameters 
{
public: // constructors, destructors
	FoamParameters_Ihmsen12();
	virtual ~FoamParameters_Ihmsen12();

public: // setter, getter
	void setCreateFoam(bool p_val) { m_createFoam = p_val; }
	virtual void setDefaultParameters();

	void setFramesPerSecond(Real p_val) { m_frameDurationTime = (Real) 1.0 / p_val; }
	void setFrameDurationTime(Real p_val) { m_frameDurationTime = p_val; }
	void setTrappedAirMinimumTreshhold(Real p_val) { m_trappedAirMinimumTreshhold = p_val; }
	void setTrappedAirMaximumTreshhold(Real p_val) { m_trappedAirMaximumTreshhold = p_val; }
	void setWaveCrestMinimumTreshhold(Real p_val) { m_waveCrestMinimumTreshhold = p_val; }
	void setWaveCrestMaximumTreshhold(Real p_val) { m_waveCrestMaximumTreshhold = p_val; }
	void setKineticEnergyMinimumTreshhold(Real p_val) { m_kineticEnergyMinimumTreshhold = p_val; }
	void setKineticEnergyMaximumTreshhold(Real p_val) { m_kineticEnergyMaximumTreshhold = p_val; }
	
	void setTrappedAirCoefficient(Real p_val) { m_trappedAirCoefficient = p_val; }
	void setWaveCrestCoefficient(Real p_val) { m_waveCrestCoefficient = p_val; }
	void setBubbleBuoyancyCoefficient(Real p_val) { m_bubbleBuoyancyCoefficient = p_val; }
	void setBubbleDragCoefficient(Real p_val) { m_bubbleDragCoefficient = p_val;}

	void setTypeSprayNeighborhoodTreshhold(unsigned int p_val) { m_typeSprayNeighborhoodTreshhold = p_val; }
	void setTypeFoamNeighborhoodTreshhold(unsigned int p_val) { m_typeFoamNeighborhoodTreshhold = p_val; }

	void setMinimumLifetime(Real p_val) { m_minimumLifetime = p_val; }
	void setMaximumLifetime(Real p_val) { m_maximumLifetime = p_val; }

	bool getCreateFoam() { return m_createFoam; }
	Real getFramesPerSecond() { return (Real) 1.0 / m_frameDurationTime; }
	Real getFrameDurationTime() { return m_frameDurationTime; }

	Real getTrappedAirMinimumTreshhold() { return m_trappedAirMinimumTreshhold; }
	Real getTrappedAirMaximumTreshhold() { return m_trappedAirMaximumTreshhold; }
	Real getWaveCrestMinimumTreshhold() { return m_waveCrestMinimumTreshhold; }
	Real getWaveCrestMaximumTreshhold() { return m_waveCrestMaximumTreshhold; }
	Real getKineticEnergyMinimumTreshhold() { return m_kineticEnergyMinimumTreshhold; }
	Real getKineticEnergyMaximumTreshhold() { return m_kineticEnergyMaximumTreshhold; }
	
	Real getTrappedAirCoefficient() { return m_trappedAirCoefficient; }
	Real getWaveCrestCoefficient() { return m_waveCrestCoefficient; }
	Real getBubbleBuoyancyCoefficient() { return m_bubbleBuoyancyCoefficient; }
	Real getBubbleDragCoefficient() { return m_bubbleDragCoefficient; }

	unsigned int getTypeSprayNeighborhoodTreshhold() { return m_typeSprayNeighborhoodTreshhold; }
	unsigned int getTypeFoamNeighborhoodTreshhold() { return m_typeFoamNeighborhoodTreshhold; }

	Real getMinimumLifetime() { return m_minimumLifetime; }
	Real getMaximumLifetime() { return m_maximumLifetime; }

protected: // variables
	bool				m_createFoam;
	Real				m_frameDurationTime;

	Real				m_trappedAirMinimumTreshhold;
	Real				m_trappedAirMaximumTreshhold;
	Real				m_waveCrestMinimumTreshhold;
	Real				m_waveCrestMaximumTreshhold;
	Real				m_kineticEnergyMinimumTreshhold;
	Real				m_kineticEnergyMaximumTreshhold;

	Real				m_trappedAirCoefficient;
	Real				m_waveCrestCoefficient;
	Real				m_bubbleBuoyancyCoefficient;
	Real				m_bubbleDragCoefficient;

	unsigned int		m_typeSprayNeighborhoodTreshhold;
	unsigned int		m_typeFoamNeighborhoodTreshhold;

	Real				m_minimumLifetime;
	Real				m_maximumLifetime;
};

#endif // !__FoamParameters_Ihmsen12_h__