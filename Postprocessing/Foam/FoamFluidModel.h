#pragma once

#ifndef __FoamFluidModel_h__
#define __FoamFluidModel_h__

#include "SPlisHSPlasH\FluidModel.h"

#ifndef FLUID_PARTICLE_SET
#define FLUID_PARTICLE_SET 0u
#endif

#ifndef FOAM_PARTICLE_SET
#define FOAM_PARTICLE_SET 1u
#endif

using namespace SPH;

class FoamFluidModel : public FluidModel
{

public:
	FoamFluidModel();
	virtual ~FoamFluidModel();

protected:
	unsigned int RBO_POSITION_POINTER = 2;

	Real m_foamMass;
	Real m_foamRadius;

	std::vector<Vector3r> m_vFoam0;

	unsigned int m_numFoamParticles;
	unsigned int m_numFoamParticles0;

	/** Release the arrays containing the particle data.
	*/
	virtual void releaseFluidParticles();
	virtual void releaseFoamParticles();

public:
	virtual void cleanupModel();
	virtual void reset();

	/** Use only new initModel to guarantee correct functionality.
	*/
	//void initModel(const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles);
	virtual void initModel(const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles,
		const unsigned int nFoamParticles, Vector3r* foamParticles, Vector3r* foamVelocities);
		
	virtual void changeFluidParticles(const unsigned int nFluidParticles, unsigned int* p_identified, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles);
	virtual void resizeFluidParticles(const unsigned int newSize);
	virtual void resizeFoamParticles(const unsigned int newSize);


	/** Delete particles with corresponding indizies
	*/
	virtual void deleteFluidParticles(std::vector<unsigned int> indizes);
	virtual void deleteFoamParticles(std::vector<unsigned int> indizes);

	void updateBoundaryPsi();
	void computeBoundaryPsi(const unsigned int body);

	virtual RigidBodyParticleObject *getRigidBodyParticleObject(const unsigned int index) { return (FluidModel::RigidBodyParticleObject*) m_particleObjects[index + RBO_POSITION_POINTER]; }
	virtual const unsigned int numberOfRigidBodyParticleObjects() const { return static_cast<unsigned int>(m_particleObjects.size() - RBO_POSITION_POINTER); }

	Real getFoamRadius() const { return m_foamRadius; }
	void setFoamRadius(Real val) { m_foamRadius = val; }
	Real getFoamMass() const { return m_foamMass; }
	void setFoamMass(Real val) { m_foamMass = val; }
	unsigned int numFoamParticles() const { return static_cast<unsigned int>(m_numFoamParticles); }
	void setNumFoamParticles(const unsigned int num) { m_numFoamParticles = num; }
	unsigned int getNumFoamParticles0() const { return m_numFoamParticles0; }
	void setNumFoamParticles0(unsigned int num) { m_numFoamParticles0 = num; }

	virtual void performNeighborhoodSearchSort();

	FORCE_INLINE Vector3r &getFoamVelocity0(const unsigned int i)
	{
		return m_vFoam0[i];
	}

	FORCE_INLINE const Vector3r &getFoamVelocity0(const unsigned int i) const
	{
		return m_vFoam0[i];
	}

	FORCE_INLINE void setFoamVelocity0(const unsigned int i, const Vector3r &vel)
	{
		m_vFoam0[i] = vel;
	}

};


#endif // !__FoamFluidModel_h__