#pragma once

#ifndef __AdvectionController_Kobsik18_h__
#define __AdvectionController_Kobsik18_h__

// type: BO

#include "Postprocessing\Foam\Ihmsen12\AdvectionController_Ihmsen12.h"

#include "FoamFluidModel_Kobsik18.h"
#include "FoamParameters_Kobsik18.h"

class AdvectionController_Kobsik18 : public AdvectionController_Ihmsen12
{
public: // constructors, destructors
	AdvectionController_Kobsik18();
	virtual ~AdvectionController_Kobsik18();

public: // methods
	virtual void advectParticles(FoamFluidModel* p_model, FoamParameters* p_parameters);

protected: // methods
	virtual void computeMistVelocities(FoamFluidModel_Kobsik18* p_model, FoamParameters_Kobsik18* p_parameters);
	virtual void computeMistPositions(FoamFluidModel_Kobsik18* p_model, FoamParameters_Kobsik18* p_parameters);

	virtual Vector3r computeFoamVelocitySpray(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters);

protected: // variables



};


#endif // !__AdvectionController_Kobsik18_h__