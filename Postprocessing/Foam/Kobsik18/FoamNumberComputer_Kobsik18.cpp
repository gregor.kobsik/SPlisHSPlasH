#include "FoamNumberComputer_Kobsik18.h"

#include "FoamFluidModel_Kobsik18.h"
#include "FoamNumberComputer_Kobsik18.h"
#include "FoamParameters_Kobsik18.h"

FoamNumberComputer_Kobsik18::FoamNumberComputer_Kobsik18() :
	FoamNumberComputer_Ihmsen12()
{
	reset();
}

FoamNumberComputer_Kobsik18::~FoamNumberComputer_Kobsik18()
{
	reset();
}

void FoamNumberComputer_Kobsik18::computeNumberOfNewParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	FoamFluidModel_Kobsik18 *model = reinterpret_cast<FoamFluidModel_Kobsik18*>(p_model);
	FoamParameters_Kobsik18 *parameters = reinterpret_cast<FoamParameters_Kobsik18*>(p_parameters);

	this->reset();
	
	model->computeColorField();
	model->computeRelativeVelocity();
	model->computeFluidAcceleration(parameters->getFrameDurationTime());
	model->computeRelativeAcceleration();
	model->computeScaledAccelerationDifference();
	model->computeScaledVelocityDifference();
	model->computeFluidNormals();
	model->computeSurfaceCurvature();
	model->computeWaveCrest();
	model->computeWeberNumber(parameters->getAirDensity(), parameters->getSurfaceTension(), parameters->getSurfaceColorThreshold());

	if (parameters->getCreateFoam())
	{
		this->transferAttributeToFoam(model, parameters);
	}

	if (parameters->getCreateMist())
	{
		this->transferAttributeToMist(model, parameters);
	}
}

void FoamNumberComputer_Kobsik18::reset()
{
	FoamNumberComputer_Ihmsen12::reset();
}

void FoamNumberComputer_Kobsik18::transferAttributeToFoam(FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters)
{
	Real foamCoefficient = p_parameters->getFoamCoefficient();
	Real foamThreshold = p_parameters->getFoamThreshold();

	this->m_numberOfNewFoamParticlesPerFluidParticle.resize(p_model->numActiveParticles());

	this->m_numberOfNewFoamParticles = 0;

	for (unsigned int indexFluid = 0; indexFluid < this->m_numberOfNewFoamParticlesPerFluidParticle.size(); indexFluid++)
	{
		Real velocityAbsolute = p_model->getVelocity(FLUID_PARTICLE_SET, indexFluid).norm();
		Real velocityRelative = p_model->getRelativeVelocity(indexFluid).norm();
		Real threshold = std::sqrt(velocityAbsolute * velocityAbsolute * velocityRelative * velocityRelative);
		if (threshold > foamThreshold)
		{
			this->m_numberOfNewFoamParticlesPerFluidParticle[indexFluid] = (threshold - foamThreshold) * foamCoefficient;
			this->m_numberOfNewFoamParticles += (int) this->m_numberOfNewFoamParticlesPerFluidParticle[indexFluid];
		}
	}
}

void FoamNumberComputer_Kobsik18::transferAttributeToMist(FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters)
{
	Real mistCoefficient = p_parameters->getMistCoefficient();
	Real mistThreshold = p_parameters->getMistThreshold();

	this->m_numberOfNewMistParticlesPerFluidParticle.resize(p_model->numActiveParticles());

	this->m_numberOfNewMistParticles = 0;

	for (unsigned int indexFluid = 0; indexFluid < this->m_numberOfNewMistParticlesPerFluidParticle.size(); indexFluid++)
	{
		if (p_model->getWeberNumber(indexFluid) > mistThreshold)
		{
			this->m_numberOfNewMistParticlesPerFluidParticle[indexFluid] = (p_model->getWeberNumber(indexFluid) - mistThreshold) * mistCoefficient;
			this->m_numberOfNewMistParticles += (int) this->m_numberOfNewMistParticlesPerFluidParticle[indexFluid];
		}
	}
}