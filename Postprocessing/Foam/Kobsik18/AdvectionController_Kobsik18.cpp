#include "AdvectionController_Kobsik18.h"

#include "FoamFluidModel_Kobsik18.h"
#include "FoamParameters_Kobsik18.h"

AdvectionController_Kobsik18::AdvectionController_Kobsik18() : AdvectionController_Ihmsen12()
{

}

AdvectionController_Kobsik18::~AdvectionController_Kobsik18()
{

}

void AdvectionController_Kobsik18::advectParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	AdvectionController_Ihmsen12::advectParticles(p_model, p_parameters);

	FoamFluidModel_Kobsik18 *model = reinterpret_cast<FoamFluidModel_Kobsik18*>(p_model);
	FoamParameters_Kobsik18 *parameters = reinterpret_cast<FoamParameters_Kobsik18*>(p_parameters);

	if (parameters->getCreateMist())
	{
		this->computeMistVelocities(model, parameters);
		this->computeMistPositions(model, parameters);
	}

}

void AdvectionController_Kobsik18::computeMistVelocities(FoamFluidModel_Kobsik18* p_model, FoamParameters_Kobsik18* p_parameters)
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int i = 0; (unsigned int)i < p_model->numMistParticles(); i++)
		{
			Vector3r mistVelocity = p_model->getVelocity(MIST_PARTICLE_SET, i);
			Real mistArea = p_model->getMistRadius() * p_model->getMistRadius() * M_PI;
			Vector3r dragForce = 0.5 * p_parameters->getAirDensity() * mistVelocity.norm() * -mistVelocity * mistArea;
			Vector3r newMistVelocity = mistVelocity + (p_model->getGravitation() + dragForce) * p_parameters->getFrameDurationTime();
			p_model->setVelocity(MIST_PARTICLE_SET, i, newMistVelocity);
		}
	}
}

void AdvectionController_Kobsik18::computeMistPositions(FoamFluidModel_Kobsik18* p_model, FoamParameters_Kobsik18* p_parameters)
{
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int i = 0; (unsigned int)i < p_model->numMistParticles(); i++)
		{
			Vector3r oldMistPosition = p_model->getPosition(MIST_PARTICLE_SET, i);
			Vector3r newMistPosition = oldMistPosition + p_model->getVelocity(MIST_PARTICLE_SET, i) * p_parameters->getFrameDurationTime();
			p_model->setPosition(MIST_PARTICLE_SET, i, newMistPosition);
			p_model->setPosition0(MIST_PARTICLE_SET, i, newMistPosition);
		}
	}
}

Vector3r AdvectionController_Kobsik18::computeFoamVelocitySpray(unsigned int p_index, FoamFluidModel_Ihmsen12 *p_model, FoamParameters_Ihmsen12 *p_parameters)
{
	FoamParameters_Kobsik18* parameters = reinterpret_cast<FoamParameters_Kobsik18*>(p_parameters);
	Vector3r sprayVelocity = p_model->getVelocity(FOAM_PARTICLE_SET, p_index);
	Real sprayArea = p_model->getFoamRadius() * p_model->getFoamRadius() * M_PI;
	Vector3r dragForce = parameters->getAirDensity() * sprayVelocity.norm() * -sprayVelocity * sprayArea;
	return  sprayVelocity + (p_model->getGravitation() + dragForce) * p_parameters->getFrameDurationTime();
}