#pragma once

#ifndef __FoamNumberComputer_Kobsik18_h__
#define __FoamNumberComputer_Kobsik18_h__

// type: BO

#include "SPlisHSPlasH\Common.h"
#include <vector>

#include "Postprocessing\Foam\Ihmsen12\FoamNumberComputer_Ihmsen12.h"

class FoamFluidModel_Kobsik18;
class FoamParameters_Kobsik18;

class FoamNumberComputer_Kobsik18 : public FoamNumberComputer_Ihmsen12
{
public: // constructor, destructor
	FoamNumberComputer_Kobsik18();
	virtual ~FoamNumberComputer_Kobsik18();

public: // methods
	virtual void computeNumberOfNewParticles(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter
	unsigned int getNumberOfNewMistParticles() { return m_numberOfNewMistParticles; }
	std::vector<Real> getNumberOfNewMistParticlesPerFluidParticle() { return m_numberOfNewMistParticlesPerFluidParticle; }
	
protected: //methods
	virtual void reset();

	void transferAttributeToFoam(FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters);
	void transferAttributeToMist(FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters);

protected: // variables
	unsigned int										m_numberOfNewMistParticles;
	std::vector<Real>									m_numberOfNewMistParticlesPerFluidParticle;


};

#endif // !__FoamNumberComputer_Kobsik18_h__