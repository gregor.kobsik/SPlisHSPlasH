#pragma once

#ifndef __FoamBehaviourController_Kobsik18_h__
#define __FoamBehaviourController_Kobsik18_h__

// type: BO

#include "Postprocessing\Foam\Ihmsen12\FoamBehaviourController_Ihmsen12.h"

class FoamBehaviourController_Kobsik18 : public FoamBehaviourController_Ihmsen12
{

public: // constructors, destructors
	FoamBehaviourController_Kobsik18();
	virtual ~FoamBehaviourController_Kobsik18();

protected: // methods

protected: // methods

protected: //variables

};

#endif // !__FoamBehaviourController_Kobsik18_h__