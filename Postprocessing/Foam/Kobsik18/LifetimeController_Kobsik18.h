#pragma once

#ifndef __LifetimeController_Kobsik18_h__
#define __LifetimeController_Kobsik18_h__

// type: BO

#include "Postprocessing\Foam\Ihmsen12\LifetimeController_Ihmsen12.h"

class LifetimeController_Kobsik18 : public LifetimeController_Ihmsen12 {

public: // constructors, destructors
	LifetimeController_Kobsik18();
	virtual ~LifetimeController_Kobsik18();

public: // methods
	virtual void processLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter
	unsigned int getNumberOfDestroyedFoamParticles() { return m_numberDestroyedFoamParticles; }
	unsigned int getNumberOfDestroyedMistParticles() { return m_numberDestroyedMistParticles; }

protected: // methods

protected: // variables
	unsigned int						m_numberDestroyedFoamParticles = 0;
	unsigned int						m_numberDestroyedMistParticles = 0;

};

#endif // !__LifetimeController_Kobsik18_h__