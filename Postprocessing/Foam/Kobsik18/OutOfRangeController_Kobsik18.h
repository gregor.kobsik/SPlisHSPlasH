#pragma once

#ifndef __OutOfRangeController_Kobsik18_h__
#define __OutOfRangeController_Kobsik18_h__

// type: BO

#include "Postprocessing\Foam\Ihmsen12\OutOfRangeController_Ihmsen12.h"

#include "SPlisHSPlasH\Common.h"

class OutOfRangeController_Kobsik18 : public OutOfRangeController_Ihmsen12
{

public: // constructors, destructors
	OutOfRangeController_Kobsik18();
	virtual ~OutOfRangeController_Kobsik18();

public: // methods
	virtual void processOutOfRangeParticles(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter

protected: // methods

protected: // variables
	unsigned int											m_numberOutOfRangeFoamParticles = 0;
	unsigned int											m_numberOutOfRangeMistParticles = 0;

};

#endif // !__OutOfRangeController_Kobsik18_h__