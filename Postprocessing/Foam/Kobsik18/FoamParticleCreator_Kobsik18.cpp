#include "FoamParticleCreator_Kobsik18.h"

#include "FoamFluidModel_Kobsik18.h"
#include "FoamParameters_Kobsik18.h"
#include "FoamNumberComputer_Kobsik18.h"

FoamParticleCreator_Kobsik18::FoamParticleCreator_Kobsik18()
{
	this->m_foamNumberComputer = std::unique_ptr<FoamNumberComputer_Kobsik18>(new FoamNumberComputer_Kobsik18());
	this->reset();
}

FoamParticleCreator_Kobsik18::~FoamParticleCreator_Kobsik18()
{
	this->reset();
}

void FoamParticleCreator_Kobsik18::reset()
{
	FoamParticleCreator_Ihmsen12::reset();

	this->m_newMistParticlesPosition.clear();
	this->m_newMistParticlesVelocity.clear();
	this->m_newMistParticlesLifetime.clear();
}

void FoamParticleCreator_Kobsik18::createParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	
	FoamFluidModel_Kobsik18 *model = reinterpret_cast<FoamFluidModel_Kobsik18*>(p_model);
	FoamParameters_Kobsik18 *parameters = reinterpret_cast<FoamParameters_Kobsik18*>(p_parameters);

	FoamFluidModel_Ihmsen12 *model_Ihmsen12 = reinterpret_cast<FoamFluidModel_Ihmsen12*>(p_model);
	FoamParameters_Ihmsen12 *parameters_Ihmsen12 = reinterpret_cast<FoamParameters_Ihmsen12*>(p_parameters);
	FoamNumberComputer_Ihmsen12 *number_Ihmsen12 = reinterpret_cast<FoamNumberComputer_Ihmsen12*>(this->m_foamNumberComputer.get());

	this->reset();

	this->m_foamNumberComputer->computeNumberOfNewParticles(p_model, p_parameters);

	if (parameters->getCreateFoam())
	{
		this->createNewFoamParticles(model_Ihmsen12, parameters_Ihmsen12, number_Ihmsen12);
	}
	if (parameters->getCreateMist())
	{
		this->createNewMistParticles(model, parameters, this->m_foamNumberComputer.get());
	}
	
}

void FoamParticleCreator_Kobsik18::createNewMistParticles(FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters, FoamNumberComputer_Kobsik18 *p_number)
{
	int numberOfNewParticles = p_number->getNumberOfNewMistParticles();
	std::vector<Real> numberOfNewParticlesPerFluidParticle = p_number->getNumberOfNewMistParticlesPerFluidParticle();

	std::vector<unsigned int> parentIndex;

	#pragma omp parallel default(shared)
	{
		std::vector<unsigned int> parendIndexThread;

		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int)p_model->numActiveParticles(); indexFluid++)
		{
			for (unsigned int currentNumberOfNewParticlesPerFluid = 0; currentNumberOfNewParticlesPerFluid < (unsigned int)numberOfNewParticlesPerFluidParticle[indexFluid]; currentNumberOfNewParticlesPerFluid++)
			{
				parendIndexThread.push_back(indexFluid);
			}
		}

		#pragma omp critical
		{
			parentIndex.insert(parentIndex.end(), parendIndexThread.begin(), parendIndexThread.end());
		}
	}

	numberOfNewParticles = (int)parentIndex.size();

	this->m_newMistParticlesPosition.resize(numberOfNewParticles);
	this->m_newMistParticlesVelocity.resize(numberOfNewParticles);
	this->m_newMistParticlesLifetime.resize(numberOfNewParticles);

	unsigned int oldNumberOfMistParticles = p_model->numMistParticles();
	p_model->resizeMistParticles(oldNumberOfMistParticles + numberOfNewParticles);

	#pragma omp parallel default(shared)
	{
	#pragma omp for schedule(static)
		for (int indexMist = 0; indexMist < (int)numberOfNewParticles; indexMist++)
		{
			this->createNewRandomizedMistParticle(indexMist, &parentIndex[0], p_model, p_parameters);

			unsigned int currentMistIndex = oldNumberOfMistParticles + indexMist;
			p_model->setId(MIST_PARTICLE_SET, currentMistIndex, currentMistIndex);
			p_model->setPosition(MIST_PARTICLE_SET, currentMistIndex, this->m_newMistParticlesPosition[indexMist]);
			p_model->setPosition0(MIST_PARTICLE_SET, currentMistIndex, this->m_newMistParticlesPosition[indexMist]);
			p_model->setVelocity(MIST_PARTICLE_SET, currentMistIndex, this->m_newMistParticlesVelocity[indexMist]);
			p_model->setMistLifetime(currentMistIndex, this->m_newMistParticlesLifetime[indexMist]);
		}
	}
}

void FoamParticleCreator_Kobsik18::createNewRandomizedMistParticle(unsigned int p_indexFoam, unsigned int *p_parentIndizes, FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters)
{
	unsigned int indexFluid = p_parentIndizes[p_indexFoam];

	Vector3r parentPosition = p_model->getPosition(FLUID_PARTICLE_SET, indexFluid);
	Vector3r parentVelocity = p_model->getPosition(FLUID_PARTICLE_SET, indexFluid);
	Vector3r parentAcceleration = p_model->getAcceleration(indexFluid);

	Real randomVar1 = (Real)rand() - (Real)RAND_MAX / 2.0;
	Real randomVar2 = (Real)rand() - (Real)RAND_MAX / 2.0;
	Real randomVar3 = (Real)rand() - (Real)RAND_MAX / 2.0;
	Real randomVar4 = (Real)rand() / (Real)RAND_MAX;

	Real particleRadius = p_model->getParticleRadius();
	Vector3r randomDirection = Vector3r(randomVar1, randomVar2, randomVar3).normalized();

	Vector3r randomizedMistPosition =
		parentPosition +
		randomDirection * particleRadius;

	Vector3r randomizedMistVelocity =
		parentVelocity +
		p_parameters->getMistOriginAccelerationCoefficient() * p_parameters->getFrameDurationTime() * parentAcceleration;

	Real randomizedMistLifetime = p_parameters->getMistMinimumLifetime() + (p_parameters->getMistMaximumLifetime() - p_parameters->getMistMinimumLifetime()) * randomVar4;

	this->m_newMistParticlesPosition[p_indexFoam] = randomizedMistPosition;
	this->m_newMistParticlesVelocity[p_indexFoam] = randomizedMistVelocity;
	this->m_newMistParticlesLifetime[p_indexFoam] = randomizedMistLifetime;
}