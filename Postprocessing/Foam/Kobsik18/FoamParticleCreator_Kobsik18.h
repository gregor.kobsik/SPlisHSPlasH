#pragma once

#ifndef __FoamParticleCreator_Kobsik18_h__
#define __FoamParticleCreator_Kobsik18_h__

// type: BO

#include "Postprocessing\Foam\Ihmsen12\FoamParticleCreator_Ihmsen12.h"

class FoamNumberComputer_Kobsik18;
class FoamFluidModel_Kobsik18;
class FoamParameters_Kobsik18;

class FoamParticleCreator_Kobsik18 : public FoamParticleCreator_Ihmsen12
{
public: // constructor, destructor
	FoamParticleCreator_Kobsik18();
	virtual ~FoamParticleCreator_Kobsik18();

public: // methods
	virtual void createParticles(FoamFluidModel *p_model, FoamParameters *p_parameters);

public: // setter, getter

protected: //methods
	virtual void reset();

	virtual void createNewMistParticles(FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters, FoamNumberComputer_Kobsik18 *p_number);
	virtual void createNewRandomizedMistParticle(unsigned int p_indexFoam, unsigned int *p_parentIndizes, FoamFluidModel_Kobsik18 *p_model, FoamParameters_Kobsik18 *p_parameters);

protected: // variables
	std::unique_ptr<FoamNumberComputer_Kobsik18>						m_foamNumberComputer = nullptr;
	
	std::vector<Vector3r>												m_newMistParticlesPosition;
	std::vector<Vector3r>												m_newMistParticlesVelocity;
	std::vector<Real>													m_newMistParticlesLifetime;
};

#endif // !__FoamParticleCreator_Kobsik18_h__