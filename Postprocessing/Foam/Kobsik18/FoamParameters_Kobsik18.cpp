#include "FoamParameters_Kobsik18.h"

FoamParameters_Kobsik18::FoamParameters_Kobsik18() : FoamParameters_Ihmsen12()
{
	setDefaultParameters();
}

FoamParameters_Kobsik18::~FoamParameters_Kobsik18()
{

}

void FoamParameters_Kobsik18::setDefaultParameters()
{
	FoamParameters_Ihmsen12::setDefaultParameters();

	this->setCreateFoam(false);
	this->setFoamThreshold(5.0);
	this->setFoamCoefficient(1.0);	

	this->setCreateMist(false);
	this->setMistThreshold(200.0);
	this->setMistCoefficient(0.01);
	this->setMistOriginAccelerationCoefficient(0.1);
	this->setMistMinimumLifetime(0.2);
	this->setMistMaximumLifetime(1.5);

	this->setSurfaceTension(0.0724);
	this->setAirDensity(1.2041);
	this->setSurfaceColorThreshold(10000);
}