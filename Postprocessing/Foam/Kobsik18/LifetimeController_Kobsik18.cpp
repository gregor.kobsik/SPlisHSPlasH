#include "LifetimeController_Kobsik18.h"

#include "FoamFluidModel_Kobsik18.h"
#include "FoamParameters_Kobsik18.h"

LifetimeController_Kobsik18::LifetimeController_Kobsik18() : LifetimeController_Ihmsen12()
{

}

LifetimeController_Kobsik18::~LifetimeController_Kobsik18()
{

}

void LifetimeController_Kobsik18::processLifetime(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	LifetimeController_Ihmsen12::processLifetime(p_model, p_parameters);

	FoamFluidModel_Kobsik18* model = reinterpret_cast<FoamFluidModel_Kobsik18*>(p_model);
	FoamParameters_Kobsik18* parameters = reinterpret_cast<FoamParameters_Kobsik18*>(p_parameters);

	this->m_numberDestroyedFoamParticles = this->getNumberOfDestroyedParticles();

	if (parameters->getCreateMist())
	{
		std::vector<unsigned int> destroyedIndizies;

		#pragma omp parallel default(shared)
		{
			std::vector<unsigned int> destroyedIndiziesThread;

			#pragma omp for schedule(static)  
			for (int i = 0; (unsigned int)i < model->numMistParticles(); i++)
			{
				Real newMistLifetime = model->getMistLifetime(i) - parameters->getFrameDurationTime();
				model->setMistLifetime(i, newMistLifetime);

				if (model->getMistLifetime(i) <= 0.0)
				{
					destroyedIndiziesThread.push_back(i);
				}
			}

			#pragma omp critical
			{
				destroyedIndizies.insert(destroyedIndizies.end(), destroyedIndiziesThread.begin(), destroyedIndiziesThread.end());
			}
		}

		this->m_numberDestroyedMistParticles = (unsigned int)destroyedIndizies.size();
		this->m_numberDestroyedParticles += this->m_numberDestroyedMistParticles;
		model->deleteMistParticles(destroyedIndizies);
	}
}