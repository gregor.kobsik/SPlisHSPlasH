#pragma once
#ifndef __FoamFluidModel_Kobsik18_h__
#define __FoamFluidModel_Kobsik18_h__

// type: DO

#include "Postprocessing\Foam\Ihmsen12\FoamFluidModel_Ihmsen12.h"

#ifndef MIST_PARTICLE_SET
#define MIST_PARTICLE_SET 2u
#endif

class FoamFluidModel_Kobsik18 : public FoamFluidModel_Ihmsen12
{
public: // constructors, destructors
	FoamFluidModel_Kobsik18();
	virtual ~FoamFluidModel_Kobsik18();

public: // methods
	virtual void reset();

	virtual void changeFluidParticles(const unsigned int nFluidParticles, unsigned int *p_identifier, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles);
	virtual void deleteFoamParticles(std::vector<unsigned int> indizies);
	virtual void resizeFluidParticles(const unsigned int newSize);

	virtual void deleteMistParticles(std::vector<unsigned int> indizies);
	virtual void resizeMistParticles(const unsigned int newSize);

	virtual void performNeighborhoodSearchSort();

	void computeRelativeVelocity();
	void computeFluidAcceleration(Real p_frameDurationTime);
	void computeRelativeAcceleration();
	void computeScaledAccelerationDifference();
	void computeWeberNumber(Real airDensity, Real surfaceTension, Real surfaceColorTreshold);

	unsigned int numMistParticles() { return m_numMistParticles; }

	Real getMistRadius() const { return m_mistRadius; }
	void setMistRadius(Real val) { m_mistRadius = val; }

protected: // methods
	virtual void releaseFluidParticles();

protected: //variables

	Real m_mistRadius;

	std::vector<Vector3r>		m_velocityDifference;
	std::vector<Vector3r>		m_relativeVelocity;
	std::vector<Vector3r>		m_relativeAcceleration;
	std::vector<Real>			m_scaledAccelerationDifference;
	std::vector<Real>			m_weberNumber;

	unsigned int				m_numMistParticles = 0;
	std::vector<Real>			m_mistLifetime;

public: // inline methods

	FORCE_INLINE Vector3r &getVelocityDifference(const unsigned int i)
	{
		return m_velocityDifference[i];
	}

	FORCE_INLINE const Vector3r &getVelocityDifference(const unsigned int i) const
	{
		return m_velocityDifference[i];
	}

	FORCE_INLINE void setVelocityDifference(const unsigned int i, const Vector3r &vel)
	{
		m_velocityDifference[i] = vel;
	}

	FORCE_INLINE Vector3r &getRelativeVelocity(const unsigned int i)
	{
		return m_relativeVelocity[i];
	}

	FORCE_INLINE const Vector3r &getRelativeVelocity(const unsigned int i) const
	{
		return m_relativeVelocity[i];
	}

	FORCE_INLINE void setRelativeVelocity(const unsigned int i, const Vector3r &vel)
	{
		m_relativeVelocity[i] = vel;
	}

	FORCE_INLINE Vector3r &getRelativeAcceleration(const unsigned int i)
	{
		return m_relativeAcceleration[i];
	}

	FORCE_INLINE const Vector3r &getRelativeAcceleration(const unsigned int i) const
	{
		return m_relativeAcceleration[i];
	}

	FORCE_INLINE void setRelativeAcceleration(const unsigned int i, const Vector3r &vel)
	{
		m_relativeAcceleration[i] = vel;
	}

	FORCE_INLINE Real &getScaledAccelerationDifference(const unsigned int i)
	{
		return m_scaledAccelerationDifference[i];
	}

	FORCE_INLINE const Real &getScaledAccelerationDifference(const unsigned int i) const
	{
		return m_scaledAccelerationDifference[i];
	}

	FORCE_INLINE void setScaledAccelerationDifference(const unsigned int i, const Real &vel)
	{
		m_scaledAccelerationDifference[i] = vel;
	}

	FORCE_INLINE const Real getWeberNumber(const unsigned int i) const
	{
		return m_weberNumber[i];
	}

	FORCE_INLINE Real& getWeberNumber(const unsigned int i)
	{
		return m_weberNumber[i];
	}

	FORCE_INLINE void setWeberNumber(const unsigned int i, const Real &val)
	{
		m_weberNumber[i] = val;
	}

	FORCE_INLINE const Real getMistLifetime(const unsigned int i) const
	{
		return m_mistLifetime[i];
	}

	FORCE_INLINE Real& getMistLifetime(const unsigned int i)
	{
		return m_mistLifetime[i];
	}

	FORCE_INLINE void setMistLifetime(const unsigned int i, const Real &val)
	{
		m_mistLifetime[i] = val;
	}

	
	
};

#endif // !__FoamFluidModel_Kobsik18_h__