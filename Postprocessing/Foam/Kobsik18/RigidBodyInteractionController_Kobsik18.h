#pragma once

#ifndef __RigidBodyInteractionController_Kobsik18_h__
#define __RigidBodyInteractionController_Kobsik18_h__

// type: BO

#include "Postprocessing/Foam/Ihmsen12/RigidBodyInteractionController_Ihmsen12.h"

class RigidBodyInteractionController_Kobsik18 : public RigidBodyInteractionController_Ihmsen12 {

public: // constructors, destructors
	RigidBodyInteractionController_Kobsik18();
	virtual ~RigidBodyInteractionController_Kobsik18();

public: // methods

public: // setter, getter

protected: // methods

protected: // variables

};

#endif // !__RigidBodyInteractionController_Kobsik18_h__