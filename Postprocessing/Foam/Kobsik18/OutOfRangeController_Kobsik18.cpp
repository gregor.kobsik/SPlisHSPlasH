#include "OutOfRangeController_Kobsik18.h"

#include "FoamFluidModel_Kobsik18.h"
#include "FoamParameters_Kobsik18.h"


OutOfRangeController_Kobsik18::OutOfRangeController_Kobsik18() : OutOfRangeController_Ihmsen12()
{
	this->setDefaultValues();
}

OutOfRangeController_Kobsik18::~OutOfRangeController_Kobsik18()
{

}

void OutOfRangeController_Kobsik18::processOutOfRangeParticles(FoamFluidModel *p_model, FoamParameters *p_parameters)
{
	OutOfRangeController_Ihmsen12::processOutOfRangeParticles(p_model, p_parameters);

	FoamFluidModel_Kobsik18* model = reinterpret_cast<FoamFluidModel_Kobsik18*>(p_model);
	FoamParameters_Kobsik18* parameters = reinterpret_cast<FoamParameters_Kobsik18*>(p_parameters);


	std::vector<unsigned int> outOfRangeIndizies;
	#pragma omp parallel default(shared)
	{
		std::vector<unsigned int> outOfRangeIndiziesThread;
		#pragma omp for schedule(static)  
		for (int indexMist = 0; indexMist < (int)model->numMistParticles(); indexMist++)
		{
			SPH::Vector3r mistPosition = model->getPosition(MIST_PARTICLE_SET, indexMist);

			if (mistPosition.x() < this->getMinXvalue() ||
				mistPosition.x() > this->getMaxXvalue() ||
				mistPosition.y() < this->getMinYvalue() ||
				mistPosition.y() > this->getMaxYvalue() ||
				mistPosition.z() < this->getMinZvalue() ||
				mistPosition.z() > this->getMaxZvalue())
			{
				outOfRangeIndiziesThread.push_back(indexMist);
			}
		}

		#pragma omp critical
		{
			outOfRangeIndizies.insert(outOfRangeIndizies.end(), outOfRangeIndiziesThread.begin(), outOfRangeIndiziesThread.end());
		}
	}

	this->m_numberOutOfRangeMistParticles = (unsigned int)outOfRangeIndizies.size();
	this->m_numberOutOfRangeParticles += this->m_numberOutOfRangeMistParticles;
	model->deleteMistParticles(outOfRangeIndizies);
	outOfRangeIndizies.clear();
	
	
}