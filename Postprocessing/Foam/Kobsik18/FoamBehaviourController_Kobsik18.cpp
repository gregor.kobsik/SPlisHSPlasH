#include "FoamBehaviourController_Kobsik18.h"

#include "AdvectionController_Kobsik18.h"
#include "FoamFluidModel_Kobsik18.h"
#include "FoamParameters_Kobsik18.h"
#include "LifetimeController_Kobsik18.h"
#include "OutOfRangeController_Kobsik18.h"
#include "RigidBodyInteractionController_Kobsik18.h"

FoamBehaviourController_Kobsik18::FoamBehaviourController_Kobsik18() : FoamBehaviourController_Ihmsen12()
{
	this->m_advectionController = std::make_unique<AdvectionController_Kobsik18>(AdvectionController_Kobsik18());
	this->m_lifetimeController = std::make_unique<LifetimeController_Kobsik18>(LifetimeController_Kobsik18());
	this->m_outOfRangeController = std::make_unique<OutOfRangeController_Kobsik18>(OutOfRangeController_Kobsik18());
	this->m_rigidBodyInteractionController = std::make_unique<RigidBodyInteractionController_Kobsik18>(RigidBodyInteractionController_Kobsik18());
}

FoamBehaviourController_Kobsik18::~FoamBehaviourController_Kobsik18()
{
	
}