#include "FoamFluidModel_Kobsik18.h"

FoamFluidModel_Kobsik18::FoamFluidModel_Kobsik18() :
	FoamFluidModel_Ihmsen12(),
	m_velocityDifference(),
	m_relativeVelocity(),
	m_relativeAcceleration(),
	m_scaledAccelerationDifference(),
	m_weberNumber(),
	m_mistLifetime()
{
	ParticleObject *mistParticles = new ParticleObject();
	m_particleObjects.push_back(mistParticles);

	this->RBO_POSITION_POINTER = m_particleObjects.size();

	this->setMistRadius(this->getFoamRadius());
}

FoamFluidModel_Kobsik18::~FoamFluidModel_Kobsik18()
{
	
}

void FoamFluidModel_Kobsik18::reset()
{
	FoamFluidModel::reset();

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int i = 0; i < (int)numActiveParticles(); i++)
		{
			this->getVelocityDifference(i) = Vector3r(0.0, 0.0, 0.0);
		}
	}
}

void FoamFluidModel_Kobsik18::changeFluidParticles(const unsigned int nFluidParticles, unsigned int *p_identifier, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles)
{

	std::vector<Vector3r> old_velocity;
	if (this->numActiveParticles() == nFluidParticles) {
		old_velocity.resize(numActiveParticles());
		#pragma omp parallel default(shared)
		{
			#pragma omp for schedule(static)  
			for (int i = 0; i < (int) this->numActiveParticles(); i++)
			{
				unsigned int oldId = getId(FLUID_PARTICLE_SET, i);
				old_velocity[oldId] = getVelocity(FLUID_PARTICLE_SET, i);
			}
		}
	}

	FoamFluidModel_Ihmsen12::changeFluidParticles(nFluidParticles, p_identifier, fluidParticles, fluidVelocities, nMaxEmitterParticles);

	#pragma omp parallel default(shared)
	{
		if (old_velocity.size() != 0)
		{
			#pragma omp for schedule(static)  
			for (int i = 0; i < (int)nFluidParticles; i++)
			{
				Vector3r velocityDifference = getVelocity(FLUID_PARTICLE_SET, i) - old_velocity[getId(FLUID_PARTICLE_SET, i)];
				if (velocityDifference.norm() < 1e-100) //check for nummerical errors
				{
					velocityDifference = Vector3r(0, 0, 0);
				}
				this->getVelocityDifference(i) = velocityDifference;
			}
		}
		else
		{
			#pragma omp for schedule(static)  
			for (int i = 0; i < (int)nFluidParticles; i++)
			{
				this->getVelocityDifference(i).setZero();
			}
		}	
	}

	old_velocity.clear();
}

void FoamFluidModel_Kobsik18::deleteFoamParticles(std::vector<unsigned int> indizies)
{
	unsigned int lastElement = getNumFoamParticles0() - 1;

	for (unsigned int i = 0; i < indizies.size(); i++) {
		unsigned int indexFoam = indizies[i];
		if (indexFoam <= lastElement)
		{
			m_particleObjects[FOAM_PARTICLE_SET]->m_x0[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_x0[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_x[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_x[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_v[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_v[lastElement];
			m_particleObjects[FOAM_PARTICLE_SET]->m_id[indexFoam] = m_particleObjects[FOAM_PARTICLE_SET]->m_id[lastElement];
			m_vFoam0[indexFoam] = m_vFoam0[lastElement];
			m_foamLifetime[indexFoam] = m_foamLifetime[lastElement];
			m_foamType[indexFoam] = m_foamType[lastElement];

			lastElement--;
		}
	}
	resizeFoamParticles(lastElement + 1);
}

void FoamFluidModel_Kobsik18::resizeFluidParticles(const unsigned int newSize)
{
	FoamFluidModel_Ihmsen12::resizeFluidParticles(newSize);
	this->m_velocityDifference.resize(newSize);
	this->m_relativeVelocity.resize(newSize);
	this->m_relativeAcceleration.resize(newSize);
	this->m_scaledAccelerationDifference.resize(newSize);
	this->m_weberNumber.resize(newSize);
}

void FoamFluidModel_Kobsik18::deleteMistParticles(std::vector<unsigned int> indizies)
{
	unsigned int lastElement = numMistParticles() - 1;

	for (unsigned int i = 0; i < indizies.size(); i++) {
		unsigned int indexMist = indizies[i];
		if (indexMist <= lastElement)
		{
			m_particleObjects[MIST_PARTICLE_SET]->m_x0[indexMist] = m_particleObjects[MIST_PARTICLE_SET]->m_x0[lastElement];
			m_particleObjects[MIST_PARTICLE_SET]->m_x[indexMist] = m_particleObjects[MIST_PARTICLE_SET]->m_x[lastElement];
			m_particleObjects[MIST_PARTICLE_SET]->m_v[indexMist] = m_particleObjects[MIST_PARTICLE_SET]->m_v[lastElement];
			m_particleObjects[MIST_PARTICLE_SET]->m_id[indexMist] = m_particleObjects[MIST_PARTICLE_SET]->m_id[lastElement];
			m_mistLifetime[indexMist] = m_mistLifetime[lastElement];

			lastElement--;
		}
	}
	resizeMistParticles(lastElement + 1);
}

void FoamFluidModel_Kobsik18::resizeMistParticles(const unsigned int newSize)
{
	this->m_numMistParticles = newSize;

	this->m_particleObjects[MIST_PARTICLE_SET]->m_id.resize(newSize);
	this->m_particleObjects[MIST_PARTICLE_SET]->m_x.resize(newSize);
	this->m_particleObjects[MIST_PARTICLE_SET]->m_x0.resize(newSize);
	this->m_particleObjects[MIST_PARTICLE_SET]->m_v.resize(newSize);
	this->m_mistLifetime.resize(newSize);
}

void FoamFluidModel_Kobsik18::performNeighborhoodSearchSort()
{
	FoamFluidModel_Ihmsen12::performNeighborhoodSearchSort();
	// Fluid
	auto const& d = this->m_neighborhoodSearch->point_set(FLUID_PARTICLE_SET);
	if (d.n_points() > 0)
	{
		d.sort_field(&this->m_velocityDifference[0]);
		d.sort_field(&this->m_relativeVelocity[0]);
		d.sort_field(&this->m_relativeAcceleration[0]);
		d.sort_field(&this->m_scaledAccelerationDifference[0]);
		d.sort_field(&this->m_weberNumber[0]);
	}
}

void FoamFluidModel_Kobsik18::computeRelativeVelocity()
{
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) this->numActiveParticles(); indexFluid++)
		{
			Vector3r particlePosition = this->getPosition(FLUID_PARTICLE_SET, indexFluid);
			Vector3r particleVelocity = this->getVelocity(FLUID_PARTICLE_SET, indexFluid);

			Vector3r relativeVelocity = Vector3r(0, 0, 0);
			Vector3r weightedNeighbourVelocity = Vector3r(0, 0, 0);
			Real weightedNeighbourMass = 0;

			for (unsigned int counterNeighbour = 0; counterNeighbour < this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++)
			{
				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Vector3r neighbourVelocity = this->getVelocity(FLUID_PARTICLE_SET, indexNeighbour);
				Real neighbourMass = this->getMass(indexNeighbour);

				Vector3r particleNeighbourPositionDifference = particlePosition - neighbourPosition;

				Vector3r normalizedPositionDifference = particleNeighbourPositionDifference.normalized();

				weightedNeighbourVelocity += neighbourVelocity * neighbourMass
					* this->radialSymmetricWeightingFunction(particleNeighbourPositionDifference);
				weightedNeighbourMass += neighbourMass
					* this->radialSymmetricWeightingFunction(particleNeighbourPositionDifference);
			}

			relativeVelocity = particleVelocity - (weightedNeighbourVelocity / weightedNeighbourMass);
			this->setRelativeVelocity(indexFluid, relativeVelocity);
		}
	}
}

void FoamFluidModel_Kobsik18::computeFluidAcceleration(Real p_frameDurationTime)
{

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)numActiveParticles(); i++)
		{
			Vector3r acceleration = this->getVelocityDifference(i) / p_frameDurationTime;
			if (acceleration.norm() < 1e-100) // check for nummerical errors
			{
				acceleration = Vector3r(0, 0, 0);
			}
			this->getAcceleration(i) = acceleration;
		}
	}
}

void FoamFluidModel_Kobsik18::computeRelativeAcceleration()
{
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) this->numActiveParticles(); indexFluid++)
		{
			Vector3r particlePosition = this->getPosition(FLUID_PARTICLE_SET, indexFluid);
			Vector3r particleAcceleration = this->getAcceleration(indexFluid);

			Vector3r relativeAcceleration = Vector3r(0, 0, 0);
			Vector3r weightedNeighbourAcceleration = Vector3r(0, 0, 0);
			Real weightedNeighbourMass = 0;

			for (unsigned int counterNeighbour = 0; counterNeighbour < this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++)
			{
				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Vector3r neighbourAcceleration = this->getAcceleration(indexNeighbour);
				Real neighbourMass = this->getMass(indexNeighbour); 

				Vector3r particleNeighbourPositionDifference = particlePosition - neighbourPosition;

				Vector3r normalizedPositionDifference = particleNeighbourPositionDifference.normalized();

				weightedNeighbourAcceleration += neighbourAcceleration * neighbourMass
					* this->radialSymmetricWeightingFunction(particleNeighbourPositionDifference);
				weightedNeighbourMass += neighbourMass
					* this->radialSymmetricWeightingFunction(particleNeighbourPositionDifference);
			}

			relativeAcceleration = particleAcceleration - (weightedNeighbourAcceleration / weightedNeighbourMass);
			this->setRelativeAcceleration(indexFluid, relativeAcceleration);
		}
	}
}

void FoamFluidModel_Kobsik18::computeScaledAccelerationDifference()
{
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) this->numActiveParticles(); indexFluid++)
		{
			Vector3r particlePosition = this->getPosition(FLUID_PARTICLE_SET, indexFluid);
			Vector3r particleAcceleration = this->getAcceleration(indexFluid);
			Real scaledAccelerationDifference = 0;

			for (unsigned int counterNeighbour = 0; counterNeighbour < this->numberOfNeighbors(FLUID_PARTICLE_SET, indexFluid); counterNeighbour++)
			{
				unsigned int indexNeighbour = this->getNeighbor(FLUID_PARTICLE_SET, indexFluid, counterNeighbour);
				Vector3r neighbourPosition = this->getPosition(FLUID_PARTICLE_SET, indexNeighbour);
				Vector3r neighbourAcceleration = this->getAcceleration(indexNeighbour);

				Vector3r particleNeighbourPositionDifference = particlePosition - neighbourPosition;
				Vector3r particleNeighbourAccelerationDifference = particleAcceleration - neighbourAcceleration;

				Vector3r normalizedPositionDifference = particleNeighbourPositionDifference.normalized();
				Vector3r normalizedAccelerationDifference = particleNeighbourAccelerationDifference.normalized();
				Real magnitudeAccelerationDifference = particleNeighbourAccelerationDifference.norm();

				scaledAccelerationDifference += magnitudeAccelerationDifference * (1.0 - normalizedAccelerationDifference.dot(normalizedPositionDifference))
					* this->radialSymmetricWeightingFunction(particleNeighbourPositionDifference);
			}

			this->setScaledAccelerationDifference(indexFluid, scaledAccelerationDifference);
		}
	}
}

void FoamFluidModel_Kobsik18::computeWeberNumber(Real airDensity, Real surfaceTension, Real surfaceColorTreshold) 
{
	this->m_weberNumber.resize(this->numActiveParticles());

	Real characteristicLength = this->getParticleRadius();

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int indexFluid = 0; indexFluid < (int) this->numActiveParticles(); indexFluid++)
		{
			Vector3r fluidVelocity = this->getVelocity(FLUID_PARTICLE_SET, indexFluid);
			Vector3r airVelocity = Vector3r(0, 0, 0);
			Vector3r airFluidVelocityDifference = Vector3r(0, 0, 0);
			if (this->getGradColorField(indexFluid).norm() >= surfaceColorTreshold)
				airFluidVelocityDifference = (airVelocity - fluidVelocity); //p_model->getRelativeAcceleration(indexFluid);
			Vector3r fluidRelativeAccelerationSqr = (airFluidVelocityDifference).norm() * airFluidVelocityDifference;

			Real weberNumber = 0;
			weberNumber = airDensity * fluidRelativeAccelerationSqr.norm() * characteristicLength / surfaceTension;

			this->m_weberNumber[indexFluid] = weberNumber;

		}
	}
}

void FoamFluidModel_Kobsik18::releaseFluidParticles()
{
	FoamFluidModel_Ihmsen12::releaseFluidParticles();
	this->m_velocityDifference.clear();
	this->m_relativeVelocity.clear();
	this->m_relativeAcceleration.clear();
	this->m_scaledAccelerationDifference.clear();
	this->m_weberNumber.clear();
}