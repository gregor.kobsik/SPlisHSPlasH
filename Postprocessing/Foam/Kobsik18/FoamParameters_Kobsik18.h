#pragma once

#ifndef __FoamParameters_Kobsik18_h__
#define __FoamParameters_Kobsik18_h__

// type: DO

#include "Postprocessing\Foam\Ihmsen12\FoamParameters_Ihmsen12.h"

class FoamParameters_Kobsik18 : public FoamParameters_Ihmsen12
{
public: // constructors, destructors
	FoamParameters_Kobsik18();
	virtual ~FoamParameters_Kobsik18();

public: // setter, getter
	virtual void setDefaultParameters();

	void setFoamCoefficient(Real p_val) { m_foamCoefficient = p_val; }
	void setMistCoefficient(Real p_val) { m_mistCoefficient = p_val; }
	void setFoamThreshold(Real p_val) { m_foamThreshold = p_val; }
	void setMistThreshold(Real p_val) { m_mistThreshold = p_val; }
	void setCreateMist(bool p_val) { m_createMist = p_val; }
	void setMistOriginAccelerationCoefficient(Real p_val) { m_mistOriginAccelerationCoefficient = p_val; }
	void setMistMinimumLifetime(Real p_val) { m_mistMinimumLifetime = p_val; }
	void setMistMaximumLifetime(Real p_val) { m_mistMaximumLifetime = p_val; }

	void setSurfaceTension(Real p_val) { m_surfaceTension = p_val; }
	void setAirDensity(Real p_val) { m_airDensity = p_val; }
	void setSurfaceColorThreshold(Real p_val) { m_surfaceColorThreshold = p_val; }
	
	Real getFoamCoefficient() { return m_foamCoefficient; }
	Real getMistCoefficient() { return m_mistCoefficient; }
	Real getFoamThreshold() { return m_foamThreshold; }
	Real getMistThreshold() { return m_mistThreshold; }
	bool getCreateMist() { return m_createMist; }
	Real getMistOriginAccelerationCoefficient() { return m_mistOriginAccelerationCoefficient; }
	Real getMistMinimumLifetime() { return m_mistMinimumLifetime; }
	Real getMistMaximumLifetime() { return m_mistMaximumLifetime; }

	Real getSurfaceTension() { return m_surfaceTension; }
	Real getAirDensity() { return m_airDensity; }
	Real getSurfaceColorThreshold() { return m_surfaceColorThreshold; }

protected: // variables
	Real											m_foamCoefficient;
	Real											m_mistCoefficient;
	Real											m_foamThreshold;
	Real											m_mistThreshold;

	bool											m_createMist;
	Real											m_mistOriginAccelerationCoefficient;
	Real											m_mistMinimumLifetime;
	Real											m_mistMaximumLifetime;		

	Real											m_surfaceTension;
	Real											m_airDensity;
	Real											m_surfaceColorThreshold;

};

#endif // !__FoamParameters_Kobsik18_h__