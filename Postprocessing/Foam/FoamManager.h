#pragma once

#ifndef __FoamManager_h__
#define __FoamManager_h__

// type: BO

#include "SPlisHSPlasH\Common.h"
#include <vector>
#include <memory>

class FoamBehaviourController;
class FoamFactory;
class FoamFluidModel;
class FoamParameters;
class FoamParticleCreator;

enum FoamCreationMethod;

class FoamManager {

public: // constructor, destructor
	/*
	Creates an object to manage the foam creation. Parameters:
	0 - Ihmsen et al. 2012
	1 - Kim et al. 2016
	2 - Kobsik 2018
	*/
	FoamManager(unsigned int p_method, Real p_fluidRadius);
	~FoamManager();

public: // methods
	void processNextTimestep();	
	void reset();
	void exportParticleSet();

protected: // methods
	void init(Real p_fluidRadius);
	void performNeighbourhoodSearch();

	void saveParticleData(unsigned int p_size, unsigned int *p_id, SPH::Vector3r *p_pos, SPH::Vector3r *p_data, Real p_radius, std::string p_name);

public: // getter, setter
	void setFluidParticleRadius(Real p_radius);
	Real getFluidParticleRadius();

	void setFoamCreationMethod(unsigned int p_method);
	unsigned int getFoamCreationMethode() { return (unsigned int) m_currentFoamCreationMethod; }

	void setExportFoam(bool p_val) { m_exportFoam = p_val; }
	bool getExportFoam() { return m_exportFoam; }
	void setExportMist(bool p_val) { m_exportMist = p_val; }
	bool getExportMist() { return m_exportMist; }

	void setFluidParticleSet(unsigned int *p_identifier, SPH::Vector3r *p_position, SPH::Vector3r *p_velocity, const unsigned int p_number);
	void getFoamParticleSet(std::vector<SPH::Vector3r> &p_position, std::vector<SPH::Vector3r> &p_velocity, unsigned int &p_number);
	void getMistParticleSet(std::vector<SPH::Vector3r> &p_position, std::vector<SPH::Vector3r> &p_velocity, unsigned int &p_number);
	void getFoamParticleSet(std::vector<unsigned int> &p_identifier, std::vector<SPH::Vector3r> &p_position, std::vector<SPH::Vector3r> &p_velocity, unsigned int &p_number);

protected: // variables
	std::unique_ptr<FoamFactory>																	m_foamFactory = nullptr;

	std::unique_ptr<FoamBehaviourController>														m_foamBehaviourController	= nullptr;
	std::unique_ptr<FoamFluidModel>																	m_foamFluidModel			= nullptr;
	std::unique_ptr<FoamParameters>																	m_foamParameters			= nullptr;
	std::unique_ptr<FoamParticleCreator>															m_foamParticleCreator		= nullptr;

	FoamCreationMethod																				m_currentFoamCreationMethod;
	unsigned int																					m_currentParticleSet;

	bool																							m_exportFoam = true;
	bool																							m_exportMist = false;

private: // variables
	

};

#endif // !__FoamManager_h__