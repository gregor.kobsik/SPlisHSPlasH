#include "SPlisHSPlasH/Common.h"
#include <Eigen/Dense>
#include <iostream>
#include "GL/glew.h"
#include "Visualization/MiniGL.h"
#include "GL/glut.h"
#include "SPlisHSPlasH/Utilities/Timing.h"
#include "Utilities/PartioReaderWriter.h"
#include "Utilities/OBJLoader.h"
#include "Utilities/StringTools.h"
#include "SPlisHSPlasH/Utilities/PoissonDiskSampling.h"
#include "Utilities/FileSystem.h"
#include "SprayFoamBubbles\SFB\SFB.h"
#include <cfloat>
#include <chrono>

// Enable memory leak detection
#ifdef _DEBUG
#ifndef EIGEN_ALIGN
	#define new DEBUG_NEW 
#endif
#endif

using namespace SPH;
using namespace Eigen;
using namespace std;



/** The wall is defined by the minimum and maximum position where
* one coordinate must have the same value.
*/
struct AABB
{
	Vector3r m_minX;
	Vector3r m_maxX;

	AABB()
	{
		m_minX.setZero(); m_maxX.setZero();
	}

	bool contains(const Vector3r &x)
	{
		if ((m_minX[0] <= x[0]) &&
			(m_minX[1] <= x[1]) &&
			(m_minX[2] <= x[2]) &&
			(m_maxX[0] >= x[0]) &&
			(m_maxX[1] >= x[1]) &&
			(m_maxX[2] >= x[2]))
			return true;
		return false;
	}
};


void initShader();
void render();
void pointShaderBegin(const float *col, Real radius);
void pointShaderEnd();
void timeStep();
void updateBoundingBox();
void partioExport();


string inputFile = "";
string exePath, dataPath;
std::chrono::milliseconds timeOld;

std::vector<Vector3r> x;
std::vector<Vector3r> v;
std::vector<std::vector<Vector3r>> xFluid;
std::vector<std::vector<Vector3r>> vFluid;
std::vector<std::vector<Vector3r>> xFoam;
std::vector<std::vector<Vector3r>> vFoam;
AABB fluidBoundingBox;
Shader shader;
GLint context_major_version;
GLint context_minor_version;

SFB* m_SFB;
Vector3r* x_foam;
Vector3r* v_foam;
Vector3r* n_fluid;
unsigned int foamSize;
bool foamResetet = true;

//General
bool isPause = true;
int currentParticleSet = 0;
int FPS = 25;
int skipFrame = 0;
int firstFrame = 0;
int lastFrame = 0;
bool exportFoam = false;

//SprayFoamBubbles
bool createFoam = true;
Real tau_min_ta = 5; //5
Real tau_max_ta = 20; //20
Real tau_min_wc = 2; //2
Real tau_max_wc = 8; //8
Real tau_min_k = 5; //5
Real tau_max_k = 50; //50
Real h = 1;
Real k_ta = 100;
Real k_wc = 100;
Real k_d = 0.5;
Real k_b = 0.5;

//Rendering
Real particleRadiusFluid = 0.025;
Real particleRadiusFoam = 0.0125;
Real maxVel = 50.0;
bool renderFluid = true;
bool renderFoamSprayBubbles = true;

// main 
int main( int argc, char **argv )
{
	REPORT_MEMORY_LEAKS;

	exePath = FileSystem::getProgramPath();
	dataPath = FileSystem::normalizePath(exePath + "/" + std::string(SPH_DATA_PATH));


	if (argc < 2)
	{
		std::cerr << "Not enough parameters!\n";
		std::cerr << "Usage: SprayFoamBubbles.exe [-r radius] particles.bgeo\n";
		inputFile = "DamBreak6k5sec25fps";
		//return -1;
	}

	bool particleRadiusParam = false;
	std::string foamFolder = "/Foam";

	for (int i=1; i < argc; i++)
	{
		string argStr = argv[i];
		string type_str = argStr.substr(0, 2);
		if ((type_str == "-r") && (i + 1 < argc))
		{
			particleRadiusFluid = stof(argv[++i]);
			particleRadiusParam = true;
		}
		else if ((type_str == "-n")) {
			foamFolder = "/nFoam";
		}
		else
			inputFile = argv[i];
	}

	std::vector<std::string> fluidFileList;
	std::vector<std::string> foamFileList;
	std::string path = FileSystem::getProgramPath() + "/" + inputFile;
	std::string fluidPath = path + "/Potential";
	std::string foamPath = path + foamFolder;
	fluidFileList = FileSystem::getAllFileNamesWithinFolder(fluidPath, "/*.bgeo");
	foamFileList = FileSystem::getAllFileNamesWithinFolder(foamPath, "/*.bgeo");
	std::sort(fluidFileList.begin(), fluidFileList.end(), StringTools::compareNat);
	std::sort(foamFileList.begin(), foamFileList.end(), StringTools::compareNat);

	if (!particleRadiusParam)
	{
		particleRadiusFluid = 0.025;
	}
	for (int i = 0; i < fluidFileList.size(); i++) {
		x.clear();
		v.clear();
		PartioReaderWriter::readParticles(fluidPath + "/" + fluidFileList[i], Vector3r::Zero(), Matrix3r::Identity(), 1.0, x, v, particleRadiusFluid);
		xFluid.push_back(x);
		vFluid.push_back(v);
	}
	for (int i = 0; i < foamFileList.size(); i++) {
		x.clear();
		v.clear();
		PartioReaderWriter::readParticles(foamPath + "/" + foamFileList[i], Vector3r::Zero(), Matrix3r::Identity(), 1.0, x, v, particleRadiusFoam);
		xFoam.push_back(x);
		vFoam.push_back(v);
	}
	xFoam.resize(xFluid.size());
	vFoam.resize(xFluid.size());

	lastFrame = fluidFileList.size() -1;
	

	for (unsigned int i = 0; i < v.size(); i++)
		maxVel = std::max(maxVel, v[i].norm());

	updateBoundingBox();

	m_SFB = new SFB(FPS, particleRadiusFluid);
	x_foam = m_SFB->getPositionData();
	v_foam = m_SFB->getVelocityData();
	n_fluid = m_SFB->getFluidNormalsData();

	// OpenGL
	MiniGL::init(argc, argv, 1024, 768, 0, 0, "Spray Foam Bubbles");
	MiniGL::initLights();
	MiniGL::initTweakBarParameters();
	MiniGL::getOpenGLVersion(context_major_version, context_minor_version);
	MiniGL::setViewport(40.0, 0.1f, 500.0, Vector3r(0.0, 3.0, 10.0), Vector3r(0.0, 0.0, 0.0));

	//General
	TwAddVarRW(MiniGL::getTweakBar(), "isPause", TW_TYPE_BOOL8, &isPause, "label = 'Paused' group = General");
	TwAddVarRW(MiniGL::getTweakBar(), "currentParticleSet", TW_TYPE_INT16, &currentParticleSet, " label='Particle set' min = 0 group=General");
	TwAddVarRW(MiniGL::getTweakBar(), "FPS", TW_TYPE_INT16, &FPS, "label = 'FPS' min = 1 max = 120 group = General");
	TwAddVarRW(MiniGL::getTweakBar(), "skipFrames", TW_TYPE_INT16, &skipFrame, "label = 'Skip frame' min = 0 max = 50 group=General");
	TwAddVarRW(MiniGL::getTweakBar(), "firstFrame", TW_TYPE_INT16, &firstFrame, "label = 'First frame' min = 0 group=General");
	TwAddVarRW(MiniGL::getTweakBar(), "lastFrame", TW_TYPE_INT16, &lastFrame, "label = 'Last frame' min = 0 group=General");
	TwAddVarRW(MiniGL::getTweakBar(), "exportFoam", TW_TYPE_BOOL8, &exportFoam, "label = 'Export foam' group = General");

	//FoamSprayBubbles
	TwAddVarRW(MiniGL::getTweakBar(), "createFoam", TW_TYPE_BOOL8, &createFoam, "label = 'Create foam' group = 'Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "tau_min_ta", TW_TYPE_REAL, &tau_min_ta, "label = 'tau_min_ta' min = 0 max = 100 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "tau_max_ta", TW_TYPE_REAL, &tau_max_ta, "label = 'tau_max_ta' min = 0 max = 100 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "tau_min_wc", TW_TYPE_REAL, &tau_min_wc, "label = 'tau_min_wc' min = 0 max = 100 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "tau_max_wc", TW_TYPE_REAL, &tau_max_wc, "label = 'tau_max_wc' min = 0 max = 100 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "tau_min_k", TW_TYPE_REAL, &tau_min_k, "label = 'tau_min_k' min = 0 max = 100 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "tau_max_k", TW_TYPE_REAL, &tau_max_k, "label = 'tau_max_k' min = 0 max = 100 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "k_ta", TW_TYPE_REAL, &k_ta, "label = 'k_ta' min = 0 max = 10000 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "k_wc", TW_TYPE_REAL, &k_wc, "label = 'k_wc' min = 0 max = 10000 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "k_d", TW_TYPE_REAL, &k_d, "label = 'k_d' min = 0 max = 1 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "k_b", TW_TYPE_REAL, &k_b, "label = 'k_b' min = 0 max = 1 group='Spray, Foam, Bubbles'");
	TwAddVarRW(MiniGL::getTweakBar(), "h", TW_TYPE_REAL, &h, "label = 'h' min = 0 max = 50 group='Spray, Foam, Bubbles'");

	//Rendering
	TwAddVarRW(MiniGL::getTweakBar(), "particleRadiusFluid", TW_TYPE_REAL, &particleRadiusFluid, " label='Particle radius fluid' min=0.00001 group=Rendering");
	TwAddVarRW(MiniGL::getTweakBar(), "particleRadiusFoam", TW_TYPE_REAL, &particleRadiusFoam, " label='Particle radius foam' min=0.000001 group=Rendering");
	TwAddVarRW(MiniGL::getTweakBar(), "maxVel", TW_TYPE_REAL, &maxVel, "label = 'Maximal velocity' min = 0 max = 1000 group=Rendering");
	TwAddVarRW(MiniGL::getTweakBar(), "renderFluid", TW_TYPE_BOOL8, &renderFluid, "label = 'renderFluid' group=Rendering");
	TwAddVarRW(MiniGL::getTweakBar(), "renderFoamSprayBubbles", TW_TYPE_BOOL8, &renderFoamSprayBubbles, "label = 'renderFoamSprayBubbles' group=Rendering");

	if (MiniGL::checkOpenGLVersion(3, 3))
		initShader();
	
	MiniGL::setClientSceneFunc(render);
	MiniGL::setClientIdleFunc(FPS, timeStep);

	glutMainLoop();


	Timing::printAverageTimes();
	Timing::printTimeSums();
	
	return 0;
}


void initShader()
{
	string vertFile = dataPath + "/shaders/vs_points.glsl";
	string fragFile = dataPath + "/shaders/fs_points.glsl";
	shader.compileShaderFile(GL_VERTEX_SHADER, vertFile);
	shader.compileShaderFile(GL_FRAGMENT_SHADER, fragFile);
	shader.createAndLinkProgram();
	shader.begin();
	shader.addUniform("modelview_matrix");
	shader.addUniform("projection_matrix");
	shader.addUniform("radius");
	shader.addUniform("viewport_width");
	shader.addUniform("color");
	shader.addUniform("projection_radius");
	shader.addUniform("max_velocity");
	shader.end();
}

void renderAABB(const AABB &aabb, float *color)
{
	Vector3r a = aabb.m_minX;
	Vector3r b(aabb.m_maxX[0], aabb.m_minX[1], aabb.m_minX[2]);
	Vector3r c(aabb.m_maxX[0], aabb.m_maxX[1], aabb.m_minX[2]);
	Vector3r d(aabb.m_minX[0], aabb.m_maxX[1], aabb.m_minX[2]);
	Vector3r e(aabb.m_minX[0], aabb.m_minX[1], aabb.m_maxX[2]);
	Vector3r f(aabb.m_maxX[0], aabb.m_minX[1], aabb.m_maxX[2]);
	Vector3r g(aabb.m_maxX[0], aabb.m_maxX[1], aabb.m_maxX[2]);
	Vector3r h(aabb.m_minX[0], aabb.m_maxX[1], aabb.m_maxX[2]);

	const float w = 2.0;
	MiniGL::drawVector(a, b, w, color);
	MiniGL::drawVector(b, c, w, color);
	MiniGL::drawVector(c, d, w, color);
	MiniGL::drawVector(d, a, w, color);

	MiniGL::drawVector(e, f, w, color);
	MiniGL::drawVector(f, g, w, color);
	MiniGL::drawVector(g, h, w, color);
	MiniGL::drawVector(h, e, w, color);

	MiniGL::drawVector(a, e, w, color);
	MiniGL::drawVector(b, f, w, color);
	MiniGL::drawVector(c, g, w, color);
	MiniGL::drawVector(d, h, w, color);
}

void render()
{	

	MiniGL::coordinateSystem();

	x = xFluid[currentParticleSet];
	v = vFluid[currentParticleSet];

	// Draw simulation model
	x.resize(1);
	const unsigned int nParticles = (unsigned int) x.size();
	const unsigned int nFoam = foamSize;// (unsigned int)x_foam.size();

	if (MiniGL::checkOpenGLVersion(3, 3))
	{		
		float fluidColor[4] = { 0.3f, 0.5f, 0.9f, 0.1f };
		pointShaderBegin(&fluidColor[0], particleRadiusFluid);
		
		if (nParticles > 0 && renderFluid)
		{
			glUniform1f(shader.getUniform("max_velocity"), (GLfloat) maxVel);

			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 0, x.data());
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, 0, v.data());
			glDrawArrays(GL_POINTS, 0, nParticles);


			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);
			
		}
		pointShaderEnd();


		float foamColor[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
		pointShaderBegin(&foamColor[0], particleRadiusFoam);
		if (nFoam > 0 && renderFoamSprayBubbles) {

			glUniform1f(shader.getUniform("max_velocity"), (GLfloat)0.0);

			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, 0, x_foam);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, 0, v_foam);
			glDrawArrays(GL_POINTS, 0, nFoam);

			glDisableVertexAttribArray(0);
		}
		pointShaderEnd();

	}
	else
	{
		float fluidColor[4] = { 0.1f, 0.2f, 0.6f, 1.0f };

		glPointSize(4.0);
		glDisable(GL_LIGHTING);
		glBegin(GL_POINTS);
		for (unsigned int i = 0; i < nParticles; i++)
		{
			// modify color according to the velocity
			Eigen::Vector3f hsv;
			MiniGL::hsvToRgb(fluidColor[0], fluidColor[1], fluidColor[2], &hsv[0]);
			Real vl = v[i].norm();
			vl = std::min((1.0 / maxVel)*vl, 1.0);
			float finalColor[3];
			MiniGL::hsvToRgb(hsv[0], std::max(1.0f - (float) vl, 0.0f), 1.0f, finalColor);
			glColor3fv(finalColor);
			glVertex3v(&x[i][0]);
		}
		glEnd();
		glEnable(GL_LIGHTING);
	}

	// Render bounding box - fluid
	float col[4] = { 0.3,0.3,0.3,1 };
	renderAABB(fluidBoundingBox, col);
}

void pointShaderBegin(const float *col, Real radius)
{
	shader.begin();

	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	glUniform1f(shader.getUniform("viewport_width"), (float)viewport[2]);
	glUniform1f(shader.getUniform("radius"), (float)radius);
	glUniform3fv(shader.getUniform("color"), 1, col);

	GLfloat matrix[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
	glUniformMatrix4fv(shader.getUniform("modelview_matrix"), 1, GL_FALSE, matrix);
	GLfloat pmatrix[16];
	glGetFloatv(GL_PROJECTION_MATRIX, pmatrix);
	glUniformMatrix4fv(shader.getUniform("projection_matrix"), 1, GL_FALSE, pmatrix);

	glEnable(GL_DEPTH_TEST);
	// Point sprites do not have to be explicitly enabled since OpenGL 3.2 where
	// they are enabled by default. Moreover GL_POINT_SPRITE is deprecate and only
	// supported before OpenGL 3.2 or with compatibility profile enabled.
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glPointParameterf(GL_POINT_SPRITE_COORD_ORIGIN, GL_LOWER_LEFT);
}

void pointShaderEnd()
{
	shader.end();
}

void timeStep() {
	
	using namespace std::chrono;
	milliseconds ms = duration_cast< milliseconds >(
		system_clock::now().time_since_epoch()
		);

	Real diff = (ms.count() - timeOld.count());

	if (!isPause && diff > 1000.0 / FPS) {
		currentParticleSet += 1 + skipFrame;
		if (currentParticleSet > lastFrame) {
			m_SFB->reset();
			foamResetet = true;
			currentParticleSet = firstFrame;
		}
		timeOld = ms;

		if (createFoam) {
			if (!foamResetet) {
				m_SFB->reset();
				foamResetet = true;
				currentParticleSet = firstFrame;
			}

			m_SFB->initParameters(tau_min_ta, tau_max_ta, tau_min_wc, tau_max_wc, tau_min_k, tau_max_k, h, k_ta, k_wc, k_d, k_b);
			START_TIMING("simulation step");
			m_SFB->simulateNextStep(&x[0], &v[0], x.size());
			STOP_TIMING_AVG;

			foamSize = m_SFB->numberOfParticles();

			x_foam = m_SFB->getPositionData();
			v_foam = m_SFB->getVelocityData();
			n_fluid = m_SFB->getFluidNormalsData();

			xFoam[currentParticleSet] = std::vector<Vector3r>(x_foam, x_foam + foamSize);
			vFoam[currentParticleSet] = std::vector<Vector3r>(v_foam, v_foam + foamSize);

		} else {

			foamResetet = false;
			x_foam = xFoam[currentParticleSet].data();
			v_foam = vFoam[currentParticleSet].data();
			foamSize = xFoam[currentParticleSet].size();
		}	

		if (exportFoam) {
			partioExport();
		}

		std::cout << "Number of foam particles in scene: " << foamSize << std::endl;

		

	}
}

void updateBoundingBox()
{
	fluidBoundingBox.m_minX = Vector3r(REAL_MAX, REAL_MAX, REAL_MAX);
	fluidBoundingBox.m_maxX = Vector3r(-REAL_MAX, -REAL_MAX, -REAL_MAX);
	const Real r2 = particleRadiusFluid*0.5;

	for (unsigned int i = 0; i < x.size(); i++)
	{
		fluidBoundingBox.m_minX[0] = min(fluidBoundingBox.m_minX[0], x[i][0] - r2);
		fluidBoundingBox.m_minX[1] = min(fluidBoundingBox.m_minX[1], x[i][1] - r2);
		fluidBoundingBox.m_minX[2] = min(fluidBoundingBox.m_minX[2], x[i][2] - r2);

		fluidBoundingBox.m_maxX[0] = max(fluidBoundingBox.m_maxX[0], x[i][0] + r2);
		fluidBoundingBox.m_maxX[1] = max(fluidBoundingBox.m_maxX[1], x[i][1] + r2);
		fluidBoundingBox.m_maxX[2] = max(fluidBoundingBox.m_maxX[2], x[i][2] + r2);
	}
}


void partioExport() {
	std::string exportPath = FileSystem::normalizePath(FileSystem::getProgramPath() + "/PartioExport");
	FileSystem::makeDirs(exportPath);

	std::string fileName = "ParticleData";
	fileName = fileName + std::to_string(currentParticleSet+1) + ".bgeo";
	std::string exportFileName = FileSystem::normalizePath(exportPath + "/" + fileName);

	PartioReaderWriter::writeParticles(exportFileName, foamSize, x_foam, v_foam, particleRadiusFoam);
}