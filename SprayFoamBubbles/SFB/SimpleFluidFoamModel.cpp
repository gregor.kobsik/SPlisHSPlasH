#include "SimpleFluidFoamModel.h"

using namespace SFB;

SimpleFluidFoamModel::SimpleFluidFoamModel() :
	m_particleObjects(),
	m_masses(),
	m_a(),
	m_density()
{
	m_density0 = 1000.0;
	setFluidParticleRadius(0.025);
	m_neighborhoodSearch = NULL;
	m_gravitation = Vector3r(0.0, -9.81, 0.0);

	ParticleObject *fluidParticles = new ParticleObject();
	ParticleObject *foamParticles = new ParticleObject();
	m_particleObjects.push_back(fluidParticles);
	m_particleObjects.push_back(foamParticles);

	setKernel(3);
	setGradKernel(3);
}


SimpleFluidFoamModel::~SimpleFluidFoamModel()
{
	cleanupModel();
}


void SimpleFluidFoamModel::cleanupModel()
{
	releaseFluidParticles();
	releaseFoamParticles();
	for (unsigned int i = 0; i < m_particleObjects.size(); i++)
	{
		if (m_particleObjects[i] != NULL)
			delete m_particleObjects[i];
	}
	m_particleObjects.clear();

	delete m_neighborhoodSearch;
}

void SimpleFluidFoamModel::reset()
{
	m_emitterSystem.reset();
	setNumActiveParticles(m_numActiveParticles0);
	const unsigned int nPoints = numActiveParticles();

	if (m_neighborhoodSearch->point_set(0).n_points() != nPoints)
		m_neighborhoodSearch->resize_point_set(0, &getPosition(0, 0)[0], nPoints);

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)
		for (int i = 0; i < m_particleObjects.size(); i++) 
		{
			for (int j = 0; j < (int)m_particleObjects[i]->m_x.size(); j++) {
				const Vector3r& x0 = getPosition0(i, j);
				getPosition(i, j) = x0;
				const Vector3r& v0 = getVelocity0(i, j);
				getVelocity(i, j) = v0;
			}
		}
	}
}

void SimpleFluidFoamModel::initMasses()
{
	const int nParticles = (int)numParticles();
	const Real diam = 2.0*m_fluidParticleRadius;

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < nParticles; i++)
		{
			setMass(i, 0.8 * diam*diam*diam * m_density0);		// each particle represents a cube with a side length of r		
																// mass is slightly reduced to prevent pressure at the beginning of the simulation
		}
	}
}

void SimpleFluidFoamModel::resizeFluidParticles(const unsigned int newSize)
{
	m_particleObjects[0]->m_x0.resize(newSize);
	m_particleObjects[0]->m_x.resize(newSize);
	m_particleObjects[0]->m_v0.resize(newSize);
	m_particleObjects[0]->m_v.resize(newSize);
	m_a.resize(newSize);
	m_masses.resize(newSize);
	m_density.resize(newSize);
}

void SimpleFluidFoamModel::resizeFoamParticles(const unsigned int newSize)
{
	m_particleObjects[1]->m_x0.resize(newSize);
	m_particleObjects[1]->m_x.resize(newSize);
	m_particleObjects[1]->m_v0.resize(newSize);
	m_particleObjects[1]->m_v.resize(newSize);
}

void SimpleFluidFoamModel::releaseFluidParticles()
{
	m_particleObjects[0]->m_x0.clear();
	m_particleObjects[0]->m_x.clear();
	m_particleObjects[0]->m_v0.clear();
	m_particleObjects[0]->m_v.clear();
	m_a.clear();
	m_masses.clear();
	m_density.clear();
}

void SimpleFluidFoamModel::releaseFoamParticles()
{
	m_particleObjects[1]->m_x0.clear();
	m_particleObjects[1]->m_x.clear();
	m_particleObjects[1]->m_v0.clear();
	m_particleObjects[1]->m_v.clear();
}

void SimpleFluidFoamModel::initModel(const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles,
										  const unsigned int nFoamParticles, Vector3r* foamParticles, Vector3r* foamVelocities)
{
	releaseFluidParticles();
	releaseFoamParticles();
	resizeFluidParticles(nFluidParticles + nMaxEmitterParticles);
	resizeFoamParticles(nFoamParticles);

	// init kernel
	setFluidParticleRadius(this->m_fluidParticleRadius);

	// make sure that m_W_zero is set correctly for the new particle radius
	setKernel(getKernel());

	// copy fluid positions
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)nFluidParticles; i++)
		{
			getPosition0(0, i) = fluidParticles[i];
			getVelocity0(0, i) = fluidVelocities[i];
		}

		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)nFoamParticles; i++)
		{
			getPosition0(1, i) = fluidParticles[i];
			getVelocity0(1, i) = fluidVelocities[i];
		}

	}

	// initialize masses
	initMasses();

	// Initialize neighborhood search
	if (m_neighborhoodSearch == NULL)
		m_neighborhoodSearch = new CompactNSearch::NeighborhoodSearch(this->m_supportRadius, false);
	m_neighborhoodSearch->set_radius(this->m_supportRadius);

	// Fluids 
	m_neighborhoodSearch->add_point_set(&getPosition(0, 0)[0], nFluidParticles, true, true);

	// Foam
	m_neighborhoodSearch->add_point_set(&getPosition(1, 0)[0], nFoamParticles, true, true);

	// other point sets
	for (unsigned int i = 0; i < numberOfParticleObjects(); i++)
	{
		ParticleObject *object = getParticleObject(i);
		m_neighborhoodSearch->add_point_set(&object->m_x[0][0], object->m_x.size(), true, true);
	}

	this->m_numActiveParticles0 = nFluidParticles;
	this->m_numActiveParticles = m_numActiveParticles0;

	reset();
}

void SimpleFluidFoamModel::performNeighborhoodSearchSort()
{
	const unsigned int numPart = numActiveParticles();
	if (numPart == 0)
		return;

	m_neighborhoodSearch->z_sort();

	for (unsigned int i = 0; i < m_neighborhoodSearch->point_sets().size(); i++)
	{

		auto const& d = m_neighborhoodSearch->point_set(i);
		d.sort_field(&m_particleObjects[i]->m_x[0]);
		d.sort_field(&m_particleObjects[i]->m_v[0]);
		if (i == 0) {
			d.sort_field(&m_a[0]);
			d.sort_field(&m_masses[0]);
			d.sort_field(&m_density[0]);
		}
	}

}

void SimpleFluidFoamModel::setDensity0(const Real v)
{
	m_density0 = v;
	initMasses();
}

void SFB::SimpleFluidFoamModel::setFluidParticleRadius(Real val)
{
	m_fluidParticleRadius = val;
	m_supportRadius = 4.0*m_fluidParticleRadius;

	// init kernel
	Poly6Kernel::setRadius(m_supportRadius);
	SpikyKernel::setRadius(m_supportRadius);
	CubicKernel::setRadius(m_supportRadius);
	PrecomputedCubicKernel::setRadius(m_supportRadius);
	CohesionKernel::setRadius(m_supportRadius);
	AdhesionKernel::setRadius(m_supportRadius);
}


void SimpleFluidFoamModel::setGradKernel(unsigned int val)
{
	m_gradKernelMethod = val;
	if (m_gradKernelMethod == 0)
		m_gradKernelFct = CubicKernel::gradW;
	else if (m_gradKernelMethod == 1)
		m_gradKernelFct = Poly6Kernel::gradW;
	else if (m_gradKernelMethod == 2)
		m_gradKernelFct = SpikyKernel::gradW;
	else if (m_gradKernelMethod == 3)
		m_gradKernelFct = PrecomputedCubicKernel::gradW;
}

void SimpleFluidFoamModel::setKernel(unsigned int val)
{
	m_kernelMethod = val;
	if (m_kernelMethod == 0)
	{
		m_W_zero = CubicKernel::W_zero();
		m_kernelFct = CubicKernel::W;
	}
	else if (m_kernelMethod == 1)
	{
		m_W_zero = Poly6Kernel::W_zero();
		m_kernelFct = Poly6Kernel::W;
	}
	else if (m_kernelMethod == 2)
	{
		m_W_zero = SpikyKernel::W_zero();
		m_kernelFct = SpikyKernel::W;
	}
	else if (m_kernelMethod == 3)
	{
		m_W_zero = PrecomputedCubicKernel::W_zero();
		m_kernelFct = PrecomputedCubicKernel::W;
	}
}

void SimpleFluidFoamModel::setNumActiveParticles(const unsigned int num)
{
	m_numActiveParticles = num;
}

unsigned int SimpleFluidFoamModel::numActiveParticles() const
{
	return m_numActiveParticles;
}