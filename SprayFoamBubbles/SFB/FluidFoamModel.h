#pragma once

#ifndef __FluidFoamModel_h__
#define __FluidFoamModel_h__

#include "SPlisHSPlasH\FluidModel.h"

namespace SPH
{
	/** Holds fluid and foam data.
		objectIndex:
		0 : fluid particles
		1 : foam particles
	*/
	class FluidFoamModel : public FluidModel
	{
		
		public:
			FluidFoamModel();
			virtual ~FluidFoamModel();
			
		protected:

			Real m_foamMass;
			Real m_foamRadius;

			std::vector<Vector3r> m_vFoam0;
			std::vector<Vector3r> m_n;

			unsigned int m_numFoamParticles;
			unsigned int m_numFoamParticles0;

			/** Resize the arrays containing the particle data.
			*/
			virtual void resizeFluidParticles(const unsigned int newSize);
			virtual void resizeFoamParticles(const unsigned int newSize);
			

			/** Release the arrays containing the particle data.
			*/
			virtual void releaseFluidParticles();
			virtual void releaseFoamParticles();

		public:
			virtual void cleanupModel();
			virtual void reset();

			/** Use only new initModel to guarantee correct functionality.
			*/			
			//void initModel(const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles);
			void initModel(const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles,
						   const unsigned int nFoamParticles, Vector3r* foamParticles, Vector3r* foamVelocities);
			
			void updateBoundaryPsi();
			void computeBoundaryPsi(const unsigned int body);

			void computeFluidNormals();

			RigidBodyParticleObject *getRigidBodyParticleObject(const unsigned int index) { return (FluidModel::RigidBodyParticleObject*) m_particleObjects[index + 2]; } // changed + 1 to + 2
			const int numberOfRigidBodyParticleObjects() const { return static_cast<int>(m_particleObjects.size() - 2); } // changed - 1 to - 2
			
			
			Real getFoamRadius() const { return m_foamRadius; }
			void setFoamRadius(Real val) { m_foamRadius = val; }
			Real getFoamMass() const { return m_foamMass; }
			void setFoamMass(Real val) { m_foamMass = val; }
			unsigned int numFoamParticles() const { return static_cast<unsigned int>(m_numFoamParticles); }
			void setNumFoamParticles(const unsigned int num) { m_numFoamParticles = num; }
			unsigned int getNumFoamParticles0() const { return m_numFoamParticles0; }
			void setNumFoamParticles0(unsigned int num) { m_numFoamParticles0 = num; }
			
			void performNeighborhoodSearchSort();
			
			FORCE_INLINE const Vector3r getFluidNormal(const unsigned int i) const
			{
				return m_n[i];
			}

			FORCE_INLINE Vector3r& getFluidNormal(const unsigned int i)
			{
				return m_n[i];
			}

			FORCE_INLINE void setFluidNormal(const unsigned int i, const Vector3r &val)
			{
				m_n[i] = val;
			}

			FORCE_INLINE Vector3r &getFoamVelocity0(const unsigned int i)
			{
				return m_vFoam0[i];
			}

			FORCE_INLINE const Vector3r &getFoamVelocity0(const unsigned int i) const
			{
				return m_vFoam0[i];
			}

			FORCE_INLINE void setFoamVelocity0(const unsigned int i, const Vector3r &vel)
			{
				m_vFoam0[i] = vel;
			}
			
	};

}


#endif // !__FluidFoamModel_h__
