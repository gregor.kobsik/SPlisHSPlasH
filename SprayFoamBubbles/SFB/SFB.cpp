﻿#include "SFB.h"
#include "SPlisHSPlasH\SPHKernels.h"

#include <iostream>
#include <PointSet.h>

#include "SPlisHSPlasH/Utilities/Timing.h"

Real debug1, debug2, debug3;

SFB::SFB(Real p_FPS, Real p_radius) :
	m_x(),
	m_v(),
	m_lifetime(),
	m_type(),
	m_nFluid()
{
	this->FPS = p_FPS;
	this->m_fluidParticleRadius = p_radius;
	this->m_model = new SPH::FluidFoamModel(); 
	this->m_model->setParticleRadius(p_radius);
}


SFB::~SFB()
{
	if (this->m_model != NULL)
		delete this->m_model;

	reset();
}

std::vector<Vector3r> const& SFB::getCurrentFoamParticles() {
	return this->m_x;
}

std::vector<Vector3r> const& SFB::getCurrentFoamVelocities() {
	return this->m_v;
}

std::vector<Vector3r> const& SFB::getCurrentFluidNormals() {
	return this->m_nFluid;
}

Vector3r* SFB::getPositionData() {
	return this->m_x.data();
}

Vector3r* SFB::getVelocityData() {
	return this->m_v.data();
}

Vector3r* SFB::getFluidNormalsData() {
	return this->m_nFluid.data();
}

void SFB::initParameters(Real tau_min_ta, Real tau_max_ta, Real tau_min_wc,
	Real tau_max_wc, Real tau_min_k, Real tau_max_k, Real h,
	Real k_ta, Real k_wc, Real k_d, Real k_b) {

	this->tau_min_ta = tau_min_ta;
	this->tau_max_ta = tau_max_ta;
	this->tau_min_wc = tau_min_wc;
	this->tau_max_wc = tau_max_wc;
	this->tau_min_k = tau_min_k;
	this->tau_max_k = tau_max_k;
	this->h = h;
	this->k_ta = k_ta;
	this->k_wc = k_wc;
	this->k_d = k_d;
	this->k_b = k_b;
}

void SFB::initParameters(Real vel_min, Real vel_max, Real k, Real h, Real k_d, Real k_b) {
	this->vel_min = vel_min;
	this->vel_max = vel_max;
	this->k = k;
	this->h = h;
	this->k_d = k_d;
	this->k_b = k_b;
}

/**
*	Simulates the next step of the foam postprocessing. Updates old particles and creates new particles according to input values.
*   @param x Pointer to the point position data. Must point to contingous data of 3 * n real values.
*	@param v Pointer to the point velocity data. Must point to contingous data of 3 * n real values.
*	@param n Number of points.
*/
void SFB::simulateNextStep(Vector3r* x_fluid, Vector3r* v_fluid, unsigned int n_fluid) {	
	
#ifdef _DEBUG // HACK: in debug mode - cannot pass pointer to an vector that does not exist
	if (m_x.size() <= 0)
		createRandomParticle(Vector3r(0, 0, 0), Vector3r(0, 0, 0), 1); // TODO: cannot pass pointer to empty vector in debug mode...
#endif
	START_TIMING("Init model");
	this->m_model->initModel(n_fluid, x_fluid, v_fluid, 0, m_x.size(), &m_x[0], &m_v[0]);
	STOP_TIMING_AVG;
	START_TIMING("Neighborhood search sort");
	performNeighbourhoodSearchSort(m_model);
	STOP_TIMING_AVG;
	this->m_model->computeFluidNormals();
	copyFluidNormals();
	START_TIMING("update existing particles");
	updateParticleType(this->m_model);
	updateParticleLifetimes();
	updateParticleVelocities(this->m_model, g, h, k_d, k_b);
	updateParticlePositions();
	deleteParticlesOutOfRange();
	STOP_TIMING_AVG;
	START_TIMING("create new particles");
	createParticles(this->m_model, tau_min_ta, tau_max_ta, tau_min_wc, tau_max_wc, tau_min_k, tau_max_k, h, k_ta, k_wc);
	STOP_TIMING_AVG;
	
}

/**
*	Simulates the next step of the foam postprocessing. Updates old particles and creates new particles according to input values.
*   @param x Pointer to the point position data. Must point to contingous data of 3 * n real values.
	@param v Pointer to the point velocity data. Must point to contingous data of 3 * n real values.
*	@param p Pointer to the point potential data. Must point to contingous data of 3* n real values.
*	@param n Number of points.
*/
void SFB::simulateNextStep(Vector3r* x_fluid, Vector3r* v_fluid, Vector3r* p_fluid, unsigned int n_fluid) {

#ifdef _DEBUG // HACK: in debug mode - cannot pass pointer to an vector that does not exist
	if (m_x.size() <= 0)
		createRandomParticle(Vector3r(0, 0, 0), Vector3r(0, 0, 0), 1); // TODO: cannot pass pointer to empty vector in debug mode...
#endif
	
	START_TIMING("Init model");
	this->m_model->initModel(n_fluid, x_fluid, v_fluid, 0, m_x.size(), &m_x[0], &m_v[0]);
	STOP_TIMING_AVG;

	//needs only foam-fluid neighbourhood
	this->m_model->getNeighborhoodSearch()->set_active(false);
	this->m_model->getNeighborhoodSearch()->set_active(1u, 0u, true);
	START_TIMING("Neighborhood search sort");
	performNeighbourhoodSearchSort(m_model);
	STOP_TIMING_AVG;

	START_TIMING("update existing particles");
	updateParticleType(this->m_model);
	updateParticleLifetimes();
	updateParticleVelocities(this->m_model, g, h, k_d, k_b);
	updateParticlePositions();
	deleteParticlesOutOfRange();
	STOP_TIMING_AVG;
	START_TIMING("create new particles");
	createParticles(this->m_model, p_fluid, tau_min_ta, tau_max_ta, tau_min_wc, tau_max_wc, tau_min_k, tau_max_k, h, k_ta, k_wc);
	STOP_TIMING_AVG;

}

/**
*	Simulates the next step of the foam postprocessing. Updates old particles and creates new particles according to input values.
*   @param x Pointer to the point position data. Must point to contingous data of 3 * n real values.
*	@param v Pointer to the point velocity data. Must point to contingous data of 3 * n real values.
*	@param n Number of points.
*/
void SFB::simulateNextStepV2(Vector3r* x_fluid, Vector3r* v_fluid, unsigned int n_fluid) {

#ifdef _DEBUG // HACK: in debug mode - cannot pass pointer to an vector that does not exist
	if (m_x.size() <= 0)
		createRandomParticle(Vector3r(0, 0, 0), Vector3r(0, 0, 0), 1); // TODO: cannot pass pointer to empty vector in debug mode...
#endif
	START_TIMING("Init model");
	this->m_model->initModel(n_fluid, x_fluid, v_fluid, 0, m_x.size(), &m_x[0], &m_v[0]);
	STOP_TIMING_AVG;
	START_TIMING("Neighborhood search sort");
	performNeighbourhoodSearchSort(m_model);
	STOP_TIMING_AVG;
	this->m_model->computeFluidNormals();
	copyFluidNormals();
	START_TIMING("update existing particles");
	updateParticleType(this->m_model);
	updateParticleLifetimes();
	updateParticleVelocities(this->m_model, g, h, k_d, k_b);
	updateParticlePositions();
	deleteParticlesOutOfRange();
	STOP_TIMING_AVG;
	START_TIMING("create new particles");
	createParticles(this->m_model, this->vel_min, this->vel_max, this->h, this->k);
	STOP_TIMING_AVG;
	deleteParticlesOutOfRange();
}

/**
	Computes the potentials for trapped air, wave crest and kinetic energy for the current particles.
	@param x Pointer to the point position data. Must point to contingous data of 3 * n real values.
	@param v Pointer to the point velocity data. Must point to contingous data of 3 * n real values.
	@param p Pointer to the point potential data. Must point to contingous data of 3* n real values.
	@param n Number of points.
*/
void SFB::computeNextFluidPotential(Vector3r* x_fluid, Vector3r* v_fluid, Vector3r* p_fluid, unsigned int n_fluid) {
	#ifdef _DEBUG // HACK: in debug mode - cannot pass pointer to an vector that does not exist
	if (m_x.size() <= 0)
		createRandomParticle(Vector3r(0, 0, 0), Vector3r(0, 0, 0), 1); // TODO: cannot pass pointer to empty vector in debug mode...
	#endif
	START_TIMING("Init model");
	this->m_model->initModel(n_fluid, x_fluid, v_fluid, 0, m_x.size(), &m_x[0], &m_v[0]); 
	STOP_TIMING_AVG;
	//needs only fluid-fluid neighbourhood
	this->m_model->getNeighborhoodSearch()->set_active(false);
	this->m_model->getNeighborhoodSearch()->set_active(0u, 0u, true);
	START_TIMING("Neighborhood search sort");
	performNeighbourhoodSearchSort(m_model);
	STOP_TIMING_AVG;
	this->m_model->computeFluidNormals();
	copyFluidNormals();

	START_TIMING("compute fluid potentials");
	computePotentials(this->m_model, x_fluid, p_fluid, n_fluid);
	STOP_TIMING_AVG;
	
}

void SFB::reset() {
	this->m_x.clear();
	this->m_v.clear();
	this->m_lifetime.clear();
	this->m_type.clear();
	this->m_nFluid.clear();
}

/*
	When diffuse particles are created, their
	lifetime is initialized with a predetermined value. In
	each simulation step, the time step is subtracted from
	the lifetime of foam particles, whereas the lifetime of
	air bubbles and spray particles is not reduced.
*/
void SFB::updateParticleLifetimes() {
	
	for (int i = this->m_x.size() - 1; i >= 0; i--) {
		if (this->m_lifetime[i] <= 0) {
			deleteParticle(i);
		}
		else {
			if (this->m_type[i] == FOAM) {
				this->m_lifetime[i] -= 1.0 / FPS;
			}
		}
	}
	
}

/*
	As long as diffuse material is not influenced by water,
	its motion can be assumed to be ballistic, whereas under
	water, diffuse material is highly influenced by the
	water phase.
*/
void SFB::updateParticleVelocities(FluidFoamModel* p_model, Vector3r g, Real h, Real k_d, Real k_b) {
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int i = 0; i < this->m_x.size(); i++) {
			if (this->m_type[i] == SPRAY) { // spray only affected by gravity
				//INFO: add external forces here if needed, no external forces at the moment
				this->m_v[i] += g / FPS;
			}
			else {
				Vector3r v_f = computeAveragedLocalFluidVelocity(i, p_model, h);
				if (this->m_type[i] == FOAM) {
					this->m_v[i] = v_f;
				}
				else if (this->m_type[i] == BUBBLE) {
					Vector3r bubbleVelocityDelta = k_d * (v_f - this->m_v[i]) * FPS;
					bubbleVelocityDelta += -k_b * g;
					this->m_v[i] += bubbleVelocityDelta / FPS;
				}

			}
		}
	}
}

void SFB::updateParticlePositions() {
	

	Real xBorder = 2 - this->m_fluidParticleRadius * 2;
	Real yBorder = 0 - this->m_fluidParticleRadius * 2;
	Real zBorder = 0.75 - this->m_fluidParticleRadius * 2;

	
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int i = this->m_x.size() - 1; i >= 0; i--) {
			// update position accroding to velocity
			this->m_x[i] += this->m_v[i] / FPS;

			//check if within boundry
			if (this->m_x[i].x() < -xBorder) {
				this->m_x[i].x() = -xBorder;
				this->m_v[i].x() = 0;
			}
			if (this->m_x[i].x() > xBorder) {
				this->m_x[i].x() = xBorder;
				this->m_v[i].x() = 0;
			}

			if (this->m_x[i].y() > 10) {
				this->m_x[i].y() = 10;
				this->m_v[i].y() = 0;
			}

			if (this->m_x[i].z() < -zBorder) {
				this->m_x[i].z() = -zBorder;
				this->m_v[i].z() = 0;
			}
			if (this->m_x[i].z() > zBorder) {
				this->m_x[i].z() = zBorder;
				this->m_v[i].z() = 0;
			}

		}
	}
	
}

void SFB::updateParticleType(FluidFoamModel* p_model) {
	
	CompactNSearch::NeighborhoodSearch* nSearch = p_model->getNeighborhoodSearch();
	CompactNSearch::PointSet& ps1 = nSearch->point_set(1);

	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int i = this->m_x.size() - 1; i >= 0; i--) {
			// new type classification
			int nNeighbours = ps1.n_neighbors(0, i);

			if (nNeighbours > 20) {
				this->m_type[i] = BUBBLE;
			}
			else if (nNeighbours > 6) {
				this->m_type[i] = FOAM;
			}
			else {
				this->m_type[i] = SPRAY;
			}
		}
	}
}

void SFB::copyFluidNormals() {
	//get fluid normals
	unsigned int nNormals = this->m_model->numActiveParticles();
	this->m_nFluid.resize(nNormals);
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int i = 0; i < nNormals; i++) {
			this->m_nFluid[i] = this->m_model->getFluidNormal(i);
		}
	}
}

void SFB::deleteParticlesOutOfRange() {
	Real yBorder = 0 + this->m_fluidParticleRadius * 2;
	unsigned int lastElement = m_x.size() - 1;

	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int i = 0; i < m_x.size(); i++) {
			if (this->m_x[i].y() < yBorder) {
				copyParticleFromTo(lastElement, i);
				lastElement--;
			}
		}
	}

	lastElement++;

	m_x.erase(m_x.begin() + lastElement, m_x.end());
	m_v.erase(m_v.begin() + lastElement, m_v.end());
	m_lifetime.erase(m_lifetime.begin() + lastElement, m_lifetime.end());
	m_type.erase(m_type.begin() + lastElement, m_type.end());

}

void SFB::deleteParticle(unsigned int i) {
	m_x.erase(m_x.begin() + i);
	m_v.erase(m_v.begin() + i);
	m_lifetime.erase(m_lifetime.begin() + i);
	m_type.erase(m_type.begin() + i);
}

void SFB::copyParticleFromTo(unsigned int i, unsigned int j) {
	m_x[j] = m_x[i];
	m_v[j] = m_v[i];
	m_lifetime[j] = m_lifetime[i];
	m_type[j] = m_type[i];
}

void SFB::performNeighbourhoodSearchSort(FluidFoamModel* p_model) {
	
	CompactNSearch::NeighborhoodSearch* nSearch = this->m_model->getNeighborhoodSearch();
	this->m_model->performNeighborhoodSearchSort();

	if (m_x.size() > 0) {
		auto const& d = nSearch->point_set(1);
		d.sort_field(&m_x[0]);
		d.sort_field(&m_v[0]);
		d.sort_field(&m_lifetime[0]);
		d.sort_field(&m_type[0]);
	}

	nSearch->find_neighbors();
}

/*
	 In order
	to scale the amount of newly generated diffuse material
	and, thus, the level of detail, the user can provide
	minimum τ
	min and maximum τ
	max thresholds for each
	criterion. According to these thresholds, the potential I
	for the generation of diffuse material is mapped to the
	range between zero and one using a clamping function
	Φ which is defined as
*/
Real SFB::phi(Real I, Real tau_min, Real tau_max) {
	return (std::min(I, tau_max) - std::min(I, tau_min)) / (tau_max - tau_min);
}

/*
	We further assume that the amount of
trapped air is larger, if the fluid particles move towards
each other which can be measured by (1 − vˆij · xˆij )
with vˆij = vi−vj / ||vi−vj||
denoting the normalized relative
velocity between two particles and xˆij = xi−xj  / ||xi−xj|| 
their normalized distance vector. This term is zero for particles
that move away from each other while it is two
for particles that move towards each other. 
*/
Real SFB::scaledVelocityDifference(Vector3r& x_i, Vector3r& v_i,
												std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j,
												Real h) {
	Real v_diff_i = 0;
	for (int i = 0; i < x_j.size(); i++) {
		Vector3r x_ij = x_i - x_j[i];
		Vector3r v_ij = v_i - v_j[i];
		
		Vector3r nx_ij = x_ij.normalized();
		Vector3r nv_ij = v_ij.normalized();
		
		v_diff_i += v_ij.norm() * (1.0 - nv_ij.dot(nx_ij)) * W(x_ij, h); 
	}
	
	return v_diff_i;
}

/*
	W(xij , h) is a radially symmetric
	weighting function defined as
*/
Real SFB::W(Vector3r& x_ij, Real h) {
	if (x_ij.norm() <= h) {
		return (1.0 - x_ij.norm() / h);
	}
	else {
		return 0.0;
	}
}
/*
	For a set of points, the surface curvature κ can be approximated with
	κi = X j κij = X j (1 − nˆi · nˆj )W(xij , h), 

	Accordingly, wave crests are identified using
	κ˜i =	X j κ˜ij (5)
	with
	κ˜ij =
		0, xˆji · nˆi ≥ 0
		κij, xˆji · nˆi < 0.
*/
Real SFB::surfaceCurvature(Vector3r& x_i, Vector3r&  n_i,
										std::vector<Vector3r>& x_j, std::vector<Vector3r>& n_j,
										Real h) {
	Real kappa_i = 0;

	Vector3r nn_i = n_i.normalized();

	for (int i = 0; i < n_j.size(); i++) {
		
		Vector3r x_ji = x_j[i] - x_i;
		Vector3r nx_ji = x_ji.normalized();
		if (nx_ji.dot(nn_i) < 0) { 
			Vector3r nn_j = n_j[i].normalized();
			Vector3r x_ij = x_i - x_j[i];
			kappa_i += (1 - (nn_i.dot(nn_j))) * W(x_ij, h);
		}	
	}

	return kappa_i;
}

Vector3r SFB::minmax(Vector3r x, Real min, Real max) {
	if (x.norm() > max || x.norm() < min) {
		Vector3r returnX = x.normalized();
		if (x.norm() > max) {
			return (returnX * max);
		}
		else {
			return Vector3r(0,0,0);
		}
	}
	else {
		return x;
	}
}

std::vector<Vector3r> SFB::minmax(std::vector<Vector3r> x, Real min, Real max) {
	std::vector<Vector3r> returnX;
	returnX.resize(x.size());
	for (int i = 0; i < x.size(); i++) {
		returnX[i] = minmax(x[i], min, max);
	}
	return returnX;
}

/*
	The scaled velocity difference is then used to compute the trapped-air potential as
	Ita = Φ(vdiff_i, τmin_ta , τmax_ta ).
*/
Real SFB::trappedAirPotential(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
											std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
											Real tau_min, Real tau_max, Real h) {
	return this->phi(scaledVelocityDifference(x_i, v_i, x_j, v_j, h), tau_min, tau_max);
}

/*
	Therefore,	we propose to additionally check, if the fluid particle	moves in normal direction using
	δ vn i =
		0,vˆi·nˆi < 0.6
		1, vˆi· nˆi ≥ 0.6.

	Finally, the likelihood of a particle to be at the crest of a wave is computed as 
	Iwc = Φ( ˜κi· δvni, τminwc , τmaxwc ).
*/
Real SFB::waveCrestPotential(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
											std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
											Real tau_min, Real tau_max, Real h) {
	Vector3r nn_i = n_i.normalized();
	Vector3r nv_i = v_i.normalized();

	Real delta_vn_i = nv_i.dot(nn_i) < 0.6 ? 0.0 : 1.0;

	return this->phi(surfaceCurvature(x_i,n_i,x_j,n_j,h) * delta_vn_i, tau_min, tau_max);
}

/*
	As changes in the surface tension are not considered, only the kinetic energy 
	Ek,i = 0.5miv2i
	is used as a measurement for air entrainment. Therefore, we relate
	the amount of diffuse material generated by a fluid particle
	to its kinetic energy. Accordingly, the potential to
	generate diffuse particles due to kinetic energy is computed
	as Ik = Φ(Ek,i, τmink, τmaxk), with user-defined τmink and τmaxk.
*/
Real SFB::kineticEnergyPotential(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
												std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
												Real tau_min, Real tau_max, Real h) {
	return this->phi(0.5 * particleMass * v_i.squaredNorm(), tau_min, tau_max);
}

Real SFB::velocityPotential(Vector3r& x_i, Vector3r& v_i,
	std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j,
	Real vel_min, Real vel_max, Real h) {

	Vector3r mv_i = minmax(v_i, vel_min, vel_max);
	std::vector<Vector3r> mv_j = minmax(v_j, vel_min, vel_max);

	return scaledVelocityDifference(x_i, mv_i, x_j, mv_j, h);
}

unsigned int SFB::computeNumberOfParticles(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
												std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
												Real tau_min_ta, Real tau_max_ta, Real tau_min_wc, Real tau_max_wc, Real tau_min_k, Real tau_max_k,
												Real h, Real k_ta, Real k_wc) {
	Real numberOfParticles = 0;
	Real trappedAirPotential = this->trappedAirPotential(x_i, v_i, n_i, x_j, v_j, n_j, tau_min_ta, tau_max_ta, h);
	Real waveCrestPotential = this->waveCrestPotential(x_i, v_i, n_i, x_j, v_j, n_j, tau_min_wc, tau_max_wc, h);
	Real kineticEnergyPotential = this->kineticEnergyPotential(x_i, v_i, n_i, x_j, v_j, n_j, tau_min_k, tau_max_k, h);

	numberOfParticles += k_ta * trappedAirPotential;
	numberOfParticles += k_wc * waveCrestPotential;
	numberOfParticles *= kineticEnergyPotential;
	numberOfParticles = numberOfParticles / FPS;

	return (unsigned int) (numberOfParticles + 0.5);
}

unsigned int SFB::computeNumberOfParticles(Vector3r& x_i, Vector3r v_i, std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j,
											Real vel_min, Real vel_max, Real k, Real h)  
{
	Real numberOfParticles = 0;
	Real velocityPotential = this->phi(this->velocityPotential(x_i, v_i, x_j, v_j, vel_min, vel_max, h), vel_min*vel_min, vel_max*vel_max);
	Real kineticEnergyPotential = this->phi(0.5 * particleMass * v_i.squaredNorm(), vel_min, vel_max);

	numberOfParticles += k * velocityPotential;
	numberOfParticles *= kineticEnergyPotential;
	numberOfParticles = numberOfParticles / FPS;

	return (unsigned int)(numberOfParticles + 0.5);
}

unsigned int SFB::computeNumberOfParticles(Vector3r& p_potentials,
	Real tau_min_ta, Real tau_max_ta, Real tau_min_wc, Real tau_max_wc, Real tau_min_k, Real tau_max_k,
	Real h, Real k_ta, Real k_wc) {
	Real numberOfParticles = 0;
	Real trappedAirPotential = this->phi(p_potentials.x() , tau_min_ta, tau_max_ta);
	Real waveCrestPotential = this->phi(p_potentials.y() , tau_min_wc, tau_max_wc);
	Real kineticEnergyPotential = this->phi(p_potentials.z() , tau_min_k, tau_max_k);

	numberOfParticles += k_ta * trappedAirPotential;
	numberOfParticles += k_wc * waveCrestPotential;
	numberOfParticles *= kineticEnergyPotential;
	numberOfParticles = numberOfParticles / FPS;

	return (unsigned int)(numberOfParticles + 0.5);
}

void SFB::createParticles(FluidFoamModel* p_model, Real tau_min_ta, Real tau_max_ta, Real tau_min_wc, Real tau_max_wc, Real tau_min_k, Real tau_max_k, Real h, Real k_ta, Real k_wc)
{
	int sumCreatedParticles = 0;

	//adding elements to array, change parallelity!
	#pragma omp parallel default(shared) 
	{
		std::vector<SFB::ParticleObject> newParticles;
		unsigned int numberOfParticlesInThread = 0;
		#pragma omp for schedule(static) 
		for (int i = 0; i < p_model->numParticles(); i++) {
			Vector3r x_i = p_model->getPosition(0, i);
			Vector3r v_i = p_model->getVelocity(0, i);
			Vector3r n_i = p_model->getFluidNormal(i);
			// find neighbours of x_i
			std::vector<Vector3r> x_j;
			std::vector<Vector3r> v_j;
			std::vector<Vector3r> n_j;
			// create lists with neighbours of x_i
			
			for (int j = 0; j < p_model->numberOfNeighbors(0, i); j++) {
				int k = p_model->getNeighbor(0, i, j);
				x_j.push_back(p_model->getPosition(0, k));
				v_j.push_back(p_model->getVelocity(0, k));
				n_j.push_back(p_model->getFluidNormal(k));
			}
			//compute the number of particles that need to be created in this simulation step
			unsigned int number = computeNumberOfParticles(x_i, v_i, n_i, x_j, v_j, n_j,
				tau_min_ta, tau_max_ta, tau_min_wc, tau_max_wc, tau_min_k, tau_max_k, h, k_ta, k_wc);
			
			numberOfParticlesInThread += number;
			for (int k = 0; k < number; k++) {
				newParticles.push_back(createRandomParticle(x_i, v_i, x_j.size()));
			}
		}	

		std::vector<Vector3r> newX;
		newX.resize(numberOfParticlesInThread);
		std::vector<Vector3r> newV;
		newV.resize(numberOfParticlesInThread);
		std::vector<Real> newLifetime(numberOfParticlesInThread);
		std::vector<FoamType> newType;
		newType.resize(numberOfParticlesInThread);

		for (int i = 0; i < numberOfParticlesInThread; i++) {
			newX[i] = newParticles[i].m_x;
			newV[i] = newParticles[i].m_v;
			newLifetime[i] = newParticles[i].m_lifetime;
			newType[i] = newParticles[i].m_type;
		}

		#pragma omp critical
		{
			sumCreatedParticles += numberOfParticlesInThread;
			m_x.insert(m_x.end(), newX.begin(), newX.end());
			m_v.insert(m_v.end(), newV.begin(), newV.end());
			m_lifetime.insert(m_lifetime.end(), newLifetime.begin(), newLifetime.end());
			m_type.insert(m_type.end(), newType.begin(), newType.end());
		}
	}
}

void SFB::createParticles(	FluidFoamModel* p_model, Real vel_min, Real vel_max, Real h, Real k)
{
	int sumCreatedParticles = 0;
	//adding elements to array, change parallelity!
	#pragma omp parallel default(shared) 
	{
		std::vector<SFB::ParticleObject> newParticles;
		unsigned int numberOfParticlesInThread = 0;
		#pragma omp for schedule(static) 
		for (int i = 0; i < p_model->numParticles(); i++) {
			Vector3r x_i = p_model->getPosition(0, i);
			Vector3r v_i = p_model->getVelocity(0, i);
			Vector3r n_i = p_model->getFluidNormal(i);
			// find neighbours of x_i
			std::vector<Vector3r> x_j;
			std::vector<Vector3r> v_j;
			std::vector<Vector3r> n_j;
			// create lists with neighbours of x_i

			for (int j = 0; j < p_model->numberOfNeighbors(0, i); j++) {
				int k = p_model->getNeighbor(0, i, j);
				x_j.push_back(p_model->getPosition(0, k));
				v_j.push_back(p_model->getVelocity(0, k));
				n_j.push_back(p_model->getFluidNormal(k));
			}
			//compute the number of particles that need to be created in this simulation step
			unsigned int number = computeNumberOfParticles(x_i, v_i, x_j, v_j,
				vel_min, vel_max, k, h);

			numberOfParticlesInThread += number;
			for (int k = 0; k < number; k++) {
				newParticles.push_back(createRandomParticle(x_i, v_i, x_j.size()));
			}
		}

		std::vector<Vector3r> newX;
		newX.resize(numberOfParticlesInThread);
		std::vector<Vector3r> newV;
		newV.resize(numberOfParticlesInThread);
		std::vector<Real> newLifetime(numberOfParticlesInThread);
		std::vector<FoamType> newType;
		newType.resize(numberOfParticlesInThread);

		for (int i = 0; i < numberOfParticlesInThread; i++) {
			newX[i] = newParticles[i].m_x;
			newV[i] = newParticles[i].m_v;
			newLifetime[i] = newParticles[i].m_lifetime;
			newType[i] = newParticles[i].m_type;
		}

		#pragma omp critical
		{
			sumCreatedParticles += numberOfParticlesInThread;
			m_x.insert(m_x.end(), newX.begin(), newX.end());
			m_v.insert(m_v.end(), newV.begin(), newV.end());
			m_lifetime.insert(m_lifetime.end(), newLifetime.begin(), newLifetime.end());
			m_type.insert(m_type.end(), newType.begin(), newType.end());
		}
	}
}

void SFB::createParticles(FluidFoamModel* p_model, Vector3r* p_potential, Real tau_min_ta, Real tau_max_ta, Real tau_min_wc, Real tau_max_wc, Real tau_min_k, Real tau_max_k, Real h, Real k_ta, Real k_wc)
{
	int sumCreatedParticles = 0;

	//adding elements to array, change parallelity!
	#pragma omp parallel default(shared) 
	{
		std::vector<SFB::ParticleObject> newParticles;
		unsigned int numberOfParticlesInThread = 0;
		#pragma omp for schedule(static) 
		for (int i = 0; i < p_model->numParticles(); i++) {
			
			//compute the number of particles that need to be created in this simulation step
			unsigned int number = computeNumberOfParticles(p_potential[i],
				tau_min_ta, tau_max_ta, tau_min_wc, tau_max_wc, tau_min_k, tau_max_k, h, k_ta, k_wc);

			numberOfParticlesInThread += number;

			Vector3r x_i = p_model->getPosition(0, i);
			Vector3r v_i = p_model->getVelocity(0, i);

			for (int k = 0; k < number; k++) {
				newParticles.push_back(createRandomParticle(x_i, v_i, p_model->numberOfNeighbors(0, i)));
			}
		}

		std::vector<Vector3r> newX;
		newX.resize(numberOfParticlesInThread);
		std::vector<Vector3r> newV;
		newV.resize(numberOfParticlesInThread);
		std::vector<Real> newLifetime(numberOfParticlesInThread);
		std::vector<FoamType> newType;
		newType.resize(numberOfParticlesInThread);

		for (int i = 0; i < numberOfParticlesInThread; i++) {
			newX[i] = newParticles[i].m_x;
			newV[i] = newParticles[i].m_v;
			newLifetime[i] = newParticles[i].m_lifetime;
			newType[i] = newParticles[i].m_type;
		}

		#pragma omp critical
		{
			sumCreatedParticles += numberOfParticlesInThread;
			m_x.insert(m_x.end(), newX.begin(), newX.end());
			m_v.insert(m_v.end(), newV.begin(), newV.end());
			m_lifetime.insert(m_lifetime.end(), newLifetime.begin(), newLifetime.end());
			m_type.insert(m_type.end(), newType.begin(), newType.end());
		}
	}
}

void SFB::computePotentials(FluidFoamModel* p_model, Vector3r* p_positions, Vector3r* p_potentials, const unsigned int nFluid) {
	//adding elements to array, change parallelity!
	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int i = 0; i < p_model->numParticles(); i++) {
			Vector3r x_i = p_model->getPosition(0, i);
			Vector3r v_i = p_model->getVelocity(0, i);
			Vector3r n_i = p_model->getFluidNormal(i);
			// find neighbours of x_i
			std::vector<Vector3r> x_j;
			std::vector<Vector3r> v_j;
			std::vector<Vector3r> n_j;
			// create lists with neighbours of x_i

			for (int j = 0; j < p_model->numberOfNeighbors(0, i); j++) {
				int k = p_model->getNeighbor(0, i, j);
				x_j.push_back(p_model->getPosition(0, k));
				v_j.push_back(p_model->getVelocity(0, k));
				n_j.push_back(p_model->getFluidNormal(k));
			}

			Vector3r nn_i = n_i.normalized();
			Vector3r nv_i = v_i.normalized();

			Real delta_vn_i = nv_i.dot(nn_i) < 0.6 ? 0.0 : 1.0;

			Real trappedAirPotential = scaledVelocityDifference(x_i, v_i, x_j, v_j, h);
			Real waveCrestPotential = surfaceCurvature(x_i, n_i, x_j, n_j, h) * delta_vn_i;
			Real kineticEnergyPotential = 0.5 * particleMass * v_i.squaredNorm();

			Vector3r potentialVector = Vector3r(trappedAirPotential, waveCrestPotential, kineticEnergyPotential);
			p_potentials[i] = potentialVector;	
			p_positions[i] = x_i;
		}
	}
}

/*

	We propose to sample the generated particles in a
	cylinder spanned by the volume radius rV of a fluid
	particle and its velocity vf . Therefore, we compute a
	reference plane orthogonal to vf , spanned by e	′1 and	e′2 
	with the fluid particle position xf as the origin.
	The position of a diffuse particle xd is computed according
	to three uniformly distributed random variables
	Xr, Xθ, Xh ∈ [0..1]. They determine the distance to
	the cylinder axis r = rV	√	Xr, the azimuth θ = Xθ2π
	and the distance h = Xh · k∆tvf k from the reference
	plane (see Fig. 3). Accordingly, xd is computed as xd =
	x + r cos θe′1 + r sin θe′2 + hvˆf . Thereby, particles are
	uniformly sampled in the cylinder.

	In order to simulate splashes into different directions,
	the velocities of newly created diffuse particles
	are initialized as vd = r cos θe′1 + r sin θe′2 + vf where
	e′1 and e′2 act as velocities.

	we set the
	lifetime [of a particle] in relation to the generation potentials. Thus,
	the lifetime is not constant, but in the range between a
	user-defined minimum and maximum value.
*/
SFB::ParticleObject SFB::createRandomParticle(Vector3r& x_i, Vector3r& v_i, int p_numberOfNeighbours)
{

	// TODO: compute zylinder correctly!
	ParticleObject newParticle;
	
	Vector3r e1;
	Vector3r e2;
	if (v_i != Vector3r(1, 1, 1)) {
		e1 = v_i.cross(Vector3r(1, 1, 1));
	}
	else {
		e1 = v_i.cross(Vector3r(1, 2, 1));
	}
	e1 = v_i.cross(e1);
	e2 = v_i.cross(e1);

	e1.normalize();
	e2.normalize();

	Real X_r = ((Real)rand() / (Real)RAND_MAX);
	Real X_theta = ((Real)rand() / (Real)RAND_MAX);
	Real X_h = ((Real)rand() / (Real)RAND_MAX);

	Real r = 10 * this->m_fluidParticleRadius * std::sqrt(X_r); // [0 .. Radius]
	Real theta = X_theta * 2 * E_PI; // [0 .. 2pi]
	Real h = X_h * v_i.norm() / FPS; // [0 .. Height] h is to big, particles are created in strange/unrealistic positions

	Vector3r x_d = x_i;// +r*(std::cos(theta)*e1 + std::sin(theta)*e2) + h*v_i; // TODO:
	Vector3r v_d = r*std::cos(theta)*e1 + r*sin(theta)*e2 + v_i; 

	newParticle.m_x = x_d;
	newParticle.m_v = v_d;
	
	if (p_numberOfNeighbours < 6) {
		newParticle.m_type = SPRAY;
	}
	else if (p_numberOfNeighbours < 20) {
		newParticle.m_type = FOAM;
	}
	else {
		newParticle.m_type = BUBBLE;
	}

	newParticle.m_lifetime = (std::rand() % 1000) / 200 * p_numberOfNeighbours; // TODO: implement, not clear which values are used!
	
	return newParticle;
	
}

Vector3r SFB::computeAveragedLocalFluidVelocity(unsigned int d, FluidFoamModel* p_model, Real h)
{
	CompactNSearch::PointSet& ps1 = p_model->getNeighborhoodSearch()->point_set(1);
	unsigned int nNeighbors = ps1.n_neighbors(0, d);
	Vector3r& currentPos = p_model->getPosition(1, d);

	Real kernelSum = 0;
	Vector3r kernelSumVelocity = Vector3r(0,0,0); 
	for (int i = 0; i < nNeighbors; i++) {
		Vector3r diffPos = (currentPos - p_model->getPosition(0, ps1.neighbor(0, d, i)));
		Real w = p_model->W(diffPos);
		kernelSum += w;
		kernelSumVelocity += p_model->getVelocity(0, ps1.neighbor(0, d, i)) * w;
	}
	
	Vector3r averagedVelocity = kernelSumVelocity / kernelSum;

	//std::cout << "kernel sum vel: " << kernelSumVelocity.x() << ", " << kernelSumVelocity.y() << ", " << kernelSumVelocity.z() << std::endl;
	//std::cout << "kernel sum: " << kernelSum << std::endl;

	return averagedVelocity;
	
}
