#include "Evaluator.h"

#include <sstream>
#include <fstream>
#include <iostream>


Evaluator::Evaluator()
{
}


Evaluator::~Evaluator()
{
}

void Evaluator::appendAllDataToFile(Vector3r* p, unsigned int size, std::string p_fullFilePath) {
	Vector3r minPot = getMinPotential(p, size);
	Vector3r maxPot = getMaxPotential(p, size);
	Vector3r avgPot = getAvgPotential(p, size);
	Vector3r stdDivPot = getStdDivPotential(p, size, avgPot);
	std::vector<float> quantils = { 0.25, 0.5, 0.75 };	
	std::vector<Vector3r> quantilPots = getQuantilPotential(p, size, quantils);

	std::ofstream file;
	file.open(p_fullFilePath,std::ofstream::app);
	file << "x," << minPot.x() << "," << quantilPots[0].x() << "," << quantilPots[1].x() << "," << quantilPots[2].x() << "," << maxPot.x() << "," << avgPot.x() << "," << stdDivPot.x() << ",,";
	file << "y," << minPot.y() << "," << quantilPots[0].y() << "," << quantilPots[1].y() << "," << quantilPots[2].y() << "," << maxPot.y() << "," << avgPot.y() << "," << stdDivPot.y() << ",,";
	file << "z," << minPot.z() << "," << quantilPots[0].z() << "," << quantilPots[1].z() << "," << quantilPots[2].z() << "," << maxPot.z() << "," << avgPot.z() << "," << stdDivPot.z() << ",,";
	file << "size," << size << ",,";
	file << "\n";
	file.close();

}

Vector3r Evaluator::getMinPotential(Vector3r* p, unsigned int size)
{
	Vector3r minPotential = Vector3r(0, 0, 0);
	for (int i = 0; i < size; i++) {
		if (p[i].x() < minPotential.x())
			minPotential.x() = p[i].x();
		if (p[i].y() < minPotential.y())
			minPotential.y() = p[i].y();
		if (p[i].z() < minPotential.z())
			minPotential.z() = p[i].z();
	}
	return minPotential;
}

Vector3r Evaluator::getMaxPotential(Vector3r* p, unsigned int size)
{
	Vector3r maxPotential = Vector3r(0, 0, 0);
	for (int i = 0; i < size; i++) {
		if (p[i].x() > maxPotential.x())
			maxPotential.x() = p[i].x();
		if (p[i].y() > maxPotential.y())
			maxPotential.y() = p[i].y();
		if (p[i].z() > maxPotential.z())
			maxPotential.z() = p[i].z();
	}
	return maxPotential;
}

Vector3r Evaluator::getAvgPotential(Vector3r* p, unsigned int size)
{
	Vector3r avgPotential = Vector3r(0, 0, 0);
	for (int i = 0; i < size; i++) {
		avgPotential += p[i];
	}
	avgPotential = avgPotential / size;
	return avgPotential;
}

Vector3r Evaluator::getStdDivPotential(Vector3r *p, unsigned int size, Vector3r avg) {
	Vector3r stddivPotential = Vector3r(0, 0, 0);
	for (int i = 0; i < size; i++) {
		stddivPotential += (p[i] - avg).cwiseProduct(p[i] - avg);
	}
	stddivPotential /= size;
	return stddivPotential;
}

Vector3r Evaluator::getQuantilPotential(Vector3r *p, unsigned int size, float percent) {
	std::vector<Real> pot_x, pot_y, pot_z;
	pot_x.resize(size);
	pot_y.resize(size);
	pot_z.resize(size);
	for (int i = 0; i < size; i++) {
		pot_x[i] = p[i].x();
		pot_y[i] = p[i].y();
		pot_z[i] = p[i].z();
	}

	std::sort(&pot_x[0], &pot_x[0] + size);
	std::sort(&pot_y[0], &pot_y[0] + size);
	std::sort(&pot_z[0], &pot_z[0] + size);
	if (percent > 1.0)
		percent = 1.0;
	if (percent < 0.0)
		percent = 0.0;

	int objectNum = std::round(size * percent);
	Vector3r quantilPotential = Vector3r(0, 0, 0);
	quantilPotential.x() = p[objectNum].x();
	quantilPotential.y() = p[objectNum].y();
	quantilPotential.z() = p[objectNum].z();

	return quantilPotential;
}

std::vector<Vector3r> Evaluator::getQuantilPotential(Vector3r *p, unsigned int size, std::vector<float> percent) {
	std::vector<Real> pot_x, pot_y, pot_z;
	pot_x.resize(size);
	pot_y.resize(size);
	pot_z.resize(size);
	for (int i = 0; i < size; i++) {
		pot_x[i] = p[i].x();
		pot_y[i] = p[i].y();
		pot_z[i] = p[i].z();
	}

	for (int i = 0; i < percent.size(); i++) {
		if (percent[i] > 1.0)
			percent[i] = 1.0;
		if (percent[i] < 0.0)
			percent[i] = 0.0;
	}

	std::vector<Vector3r> quantilPotential;
	quantilPotential.resize(percent.size());

	std::sort(&pot_x[0], &pot_x[0] + size);
	std::sort(&pot_y[0], &pot_y[0] + size);
	std::sort(&pot_z[0], &pot_z[0] + size);

	for (int i = 0; i < percent.size(); i++) {
		int objectNum = std::round((float) size * percent[i]);

		quantilPotential[i].x() = pot_x[objectNum];
		quantilPotential[i].y() = pot_y[objectNum];
		quantilPotential[i].z() = pot_z[objectNum];
	}

	return quantilPotential;
}