#pragma once

#include "SPlisHSPlasH\Common.h"
#include <vector>
#include <string>

using namespace Eigen;
using namespace SPH;

class Evaluator
{
public:
	Evaluator();
	~Evaluator();

	static void appendAllDataToFile(Vector3r* p, unsigned int size, std::string p_fullFilePath);

	static Vector3r getMinPotential(Vector3r *p, unsigned int size);
	static Vector3r getMaxPotential(Vector3r *p, unsigned int size);
	static Vector3r getAvgPotential(Vector3r *p, unsigned int size);	
	static Vector3r getStdDivPotential(Vector3r *p, unsigned int size, Vector3r avg);
	static Vector3r getQuantilPotential(Vector3r *p, unsigned int size, float percent);
	static std::vector<Vector3r> getQuantilPotential(Vector3r *p, unsigned int size, std::vector<float> percent);
};

