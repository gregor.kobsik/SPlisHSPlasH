#pragma once

#include "FluidFoamModel.h"

#include "SPlisHSPlasH/Utilities/Timing.h"

using namespace SPH;

FluidFoamModel::FluidFoamModel() :
	FluidModel(),
	m_vFoam0(),
	m_n()
{
	ParticleObject *foamParticles = new ParticleObject();
	m_particleObjects.push_back(foamParticles);

	setParticleRadius(0.025);
	setFoamRadius(0.013); //TODO: choose good value here, 1/10 particleRadius is proposed by paper
	setFoamMass(1.0); //TODO: choose realistic value here
}

FluidFoamModel::~FluidFoamModel(void) 
{
	cleanupModel();
}

void FluidFoamModel::cleanupModel()
{
	releaseFoamParticles();
	// delete foam particles and rbo's, fluid will be deleted in FluidModel destructor.
	for (unsigned int i = 1; i < m_particleObjects.size(); i++)
	{
		if (i > 1)
		{
			RigidBodyParticleObject *rbpo = ((RigidBodyParticleObject*)m_particleObjects[i]);
			rbpo->m_boundaryPsi.clear();
			rbpo->m_f.clear();
			delete rbpo->m_rigidBody;
			delete rbpo;
		}
		else
			delete m_particleObjects[i];
	}
	m_particleObjects.resize(1); //only fluid particles left to be deleted by ~FluidModel();
}

void FluidFoamModel::reset() 
{
	m_emitterSystem.reset();
	setNumActiveParticles(m_numActiveParticles0);
	setNumFoamParticles(m_numFoamParticles0);
	const unsigned int nPoints = numActiveParticles();
	const unsigned int nFoam = numFoamParticles();

	// reset velocities and accelerations
	for (unsigned int i = 2; i < m_particleObjects.size(); i++)
	{
		for (int j = 0; j < (int)m_particleObjects[i]->m_x.size(); j++)
		{
			RigidBodyParticleObject *rbpo = ((RigidBodyParticleObject*)m_particleObjects[i]);
			rbpo->m_f[j].setZero();
			rbpo->m_v[j].setZero();
		}
	}

	// Fluid
	if (m_neighborhoodSearch->point_set(0).n_points() != nPoints)
		m_neighborhoodSearch->resize_point_set(0, &getPosition(0, 0)[0], nPoints);

	for (unsigned int i = 0; i < nPoints; i++)
	{
		const Vector3r& x0 = getPosition0(0, i);
		getPosition(0, i) = x0;
		getVelocity(0, i) = getVelocity0(i);
		getAcceleration(i).setZero();
		m_density[i] = 0.0;
	}

	// Foam
	if (m_neighborhoodSearch->point_set(1).n_points() != nFoam)
		m_neighborhoodSearch->resize_point_set(1, &getPosition(1, 0)[0], nFoam);	

	for (unsigned int i = 0; i < nFoam; i++) {
		const Vector3r& x0 = getPosition0(1, i);
		getPosition(1, i) = x0;
		getVelocity(1, i) = getFoamVelocity0(i);
	}
	
	//updateBoundaryPsi();	
		
	m_neighborhoodSearch->set_active(false);
	m_neighborhoodSearch->set_active(0u, 0u, true);
	m_neighborhoodSearch->set_active(1u, 0u, true); // only fluid particles are found when searching for neighbors of foam particles
	for (unsigned int i = 2; i < m_neighborhoodSearch->point_sets().size(); i++) //changed i = 1 to i = 2.
		m_neighborhoodSearch->set_active(0u, i, true);
}

void FluidFoamModel::resizeFluidParticles(const unsigned int newSize)
{
	m_particleObjects[0]->m_x0.resize(newSize);
	m_particleObjects[0]->m_x.resize(newSize);
	m_particleObjects[0]->m_v.resize(newSize);
	m_v0.resize(newSize);
	m_a.resize(newSize);
	m_masses.resize(newSize);
	m_density.resize(newSize);
	m_n.resize(newSize);
}

void FluidFoamModel::releaseFluidParticles()
{
	m_particleObjects[0]->m_x0.clear();
	m_particleObjects[0]->m_x.clear();
	m_particleObjects[0]->m_v.clear();
	m_v0.clear();
	m_a.clear();
	m_masses.clear();
	m_density.clear();
	m_n.clear();
}

void FluidFoamModel::resizeFoamParticles(const unsigned int newSize)
{
	m_particleObjects[1]->m_x0.resize(newSize);
	m_particleObjects[1]->m_x.resize(newSize);
	m_particleObjects[1]->m_v.resize(newSize);
	m_vFoam0.resize(newSize);
}

void FluidFoamModel::releaseFoamParticles()
{
	m_particleObjects[1]->m_x0.clear();
	m_particleObjects[1]->m_x.clear();
	m_particleObjects[1]->m_v.clear();
	m_vFoam0.clear();
}

void FluidFoamModel::initModel(const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles, unsigned const int nFoamParticles, Vector3r* foamParticles, Vector3r* foamVelocities)
{
	releaseFluidParticles();
	resizeFluidParticles(nFluidParticles + nMaxEmitterParticles);

	releaseFoamParticles();
	resizeFoamParticles(nFoamParticles);

	// init kernel
	setParticleRadius(m_particleRadius);

	// make sure that m_W_zero is set correctly for the new particle radius
	setKernel(getKernel());
	// copy fluid positions
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)nFluidParticles; i++)
		{
			getPosition0(0, i) = fluidParticles[i];
			getVelocity0(i) = fluidVelocities[i];
		}
	}
	
	// copy foam positions
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)nFoamParticles; i++)
		{
			getPosition0(1, i) = foamParticles[i];
			getFoamVelocity0(i) = foamVelocities[i];
		}
	}

	// initialize masses
	initMasses();

	// Initialize neighborhood search
	if (m_neighborhoodSearch == NULL)
		m_neighborhoodSearch = new CompactNSearch::NeighborhoodSearch(m_supportRadius, false);
	else
	{
		delete m_neighborhoodSearch;
		m_neighborhoodSearch = new CompactNSearch::NeighborhoodSearch(m_supportRadius, false);
	}
	m_neighborhoodSearch->set_radius(m_supportRadius);

	// Fluids 
	m_neighborhoodSearch->add_point_set(&getPosition(0, 0)[0], nFluidParticles, false, true); // made it static, probably speedup in neighborhood search
	
	// Foam
	m_neighborhoodSearch->add_point_set(&getPosition(1, 0)[0], nFoamParticles, false, true); // made it static, probably speedup in neighborhood search

	// Boundary
	for (unsigned int i = 0; i < numberOfRigidBodyParticleObjects(); i++)
	{
		RigidBodyParticleObject *rb = getRigidBodyParticleObject(i);
		m_neighborhoodSearch->add_point_set(&rb->m_x[0][0], rb->m_x.size(), rb->m_rigidBody->isDynamic(), false);
	}

	m_numActiveParticles0 = nFluidParticles;
	m_numActiveParticles = m_numActiveParticles0;

	m_numFoamParticles0 = nFoamParticles;
	m_numFoamParticles = m_numFoamParticles0;

	reset();
}

void FluidFoamModel::updateBoundaryPsi()
{
	//////////////////////////////////////////////////////////////////////////
	// Compute value psi for boundary particles (boundary handling)
	// (see Akinci et al. "Versatile rigid - fluid coupling for incompressible SPH", Siggraph 2012
	//////////////////////////////////////////////////////////////////////////

	// Search boundary neighborhood

	// Activate only static boundaries
	std::cout << "Initialize boundary psi\n";
	m_neighborhoodSearch->set_active(false);
	for (unsigned int i = 0; i < numberOfRigidBodyParticleObjects(); i++)
	{
		if (!getRigidBodyParticleObject(i)->m_rigidBody->isDynamic())
			m_neighborhoodSearch->set_active(i + 2, true, true); //changed + 1 to + 2.
	}

	m_neighborhoodSearch->find_neighbors();

	// Boundary objects
	for (unsigned int body = 0; body < numberOfRigidBodyParticleObjects(); body++)
	{
		if (!getRigidBodyParticleObject(body)->m_rigidBody->isDynamic())
			computeBoundaryPsi(body);
	}

	////////////////////////////////////////////////////////////////////////// 
	// Compute boundary psi for all dynamic bodies
	//////////////////////////////////////////////////////////////////////////
	for (unsigned int body = 0; body < numberOfRigidBodyParticleObjects(); body++)
	{
		// Deactivate all
		m_neighborhoodSearch->set_active(false);

		// Only activate next dynamic body
		if (getRigidBodyParticleObject(body)->m_rigidBody->isDynamic())
		{
			m_neighborhoodSearch->set_active(body + 2, true, true); //changed + 1 to + 2.
			m_neighborhoodSearch->find_neighbors();
			computeBoundaryPsi(body);
		}
	}

}

void FluidFoamModel::computeBoundaryPsi(const unsigned int body)
{
	const Real density0 = getDensity0();

	RigidBodyParticleObject *rb = getRigidBodyParticleObject(body);
	const unsigned int numBoundaryParticles = rb->numberOfParticles();

	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)numBoundaryParticles; i++)
		{
			Real delta = m_W_zero;
			for (unsigned int pid = 2; pid < numberOfPointSets(); pid++) // changed pid = 1 to pid = 2
			{
				for (unsigned int j = 0; j < m_neighborhoodSearch->point_set(body + 2).n_neighbors(pid, i); j++) // changed + 1 to + 2
				{
					const unsigned int neighborIndex = m_neighborhoodSearch->point_set(body + 2).neighbor(pid, i, j); // changed + 1 to + 2
					delta += W(getPosition(body + 2, i) - getPosition(pid, neighborIndex)); // changed + 1 to + 2
				}
			}
			const Real volume = 1.0 / delta;
			rb->m_boundaryPsi[i] = density0 * volume;
		}
	}
}

// neighbourhood search has to be done before!
void FluidFoamModel::computeFluidNormals() {

	#pragma omp parallel default(shared) 
	{
		#pragma omp for schedule(static) 
		for (int i = 0; i < numActiveParticles(); i++) {
			unsigned int nNeighbors = m_neighborhoodSearch->point_set(0).n_neighbors(0, i);
			Vector3r n_i = Vector3r(0, 0, 0);


			for (int j = 0; j < nNeighbors; j++) {
				unsigned int k = m_neighborhoodSearch->point_set(0).neighbor(0, i, j);

				/**
				* TODO: ask for help, if correct
				*/

				// ERROR: density = 0 at beginning of simulation.
				// correct formula: 
				// Vector3r c_ij = getMass(k) / getDensity(k) * gradW(getPosition(0, i) - getPosition(0, k));
				Vector3r c_ij = getMass(k) * gradW(getPosition(0, i) - getPosition(0, k));
				n_i -= c_ij;
			}
			n_i.normalize();
			setFluidNormal(i, n_i);
		}
	}
}

void FluidFoamModel::performNeighborhoodSearchSort()
{
	const unsigned int numPart = numActiveParticles();
	if (numPart == 0)
		return;

	m_neighborhoodSearch->z_sort();

	// Fluid
	auto const& d = m_neighborhoodSearch->point_set(0);
	if (d.n_points() > 0) {
		d.sort_field(&m_particleObjects[0]->m_x[0]);
		d.sort_field(&m_particleObjects[0]->m_v[0]);
		d.sort_field(&m_a[0]);
		d.sort_field(&m_masses[0]);
		d.sort_field(&m_density[0]);
		d.sort_field(&m_n[0]);
	}

	// Foam
	auto const& e = m_neighborhoodSearch->point_set(1);
	if (e.n_points() > 0) {
		e.sort_field(&m_particleObjects[1]->m_x[0]);
		e.sort_field(&m_particleObjects[1]->m_v[0]);
		e.sort_field(&m_vFoam0[0]);
	}

	//////////////////////////////////////////////////////////////////////////
	// Boundary
	//////////////////////////////////////////////////////////////////////////
	for (unsigned int i = 2; i < m_neighborhoodSearch->point_sets().size(); i++)
	{
		RigidBodyParticleObject *rb = getRigidBodyParticleObject(i - 1);
		if (rb->m_rigidBody->isDynamic())			// sort only dynamic boundaries
		{
			auto const& d = m_neighborhoodSearch->point_set(i);
			d.sort_field(&rb->m_x[0]);
			d.sort_field(&rb->m_v[0]);
			d.sort_field(&rb->m_f[0]);
			d.sort_field(&rb->m_boundaryPsi[0]);
		}
	}
}