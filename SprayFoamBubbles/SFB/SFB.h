#ifndef __SFB_h__
#define __SFB_h__
#pragma once

#include "SPlisHSPlasH\Common.h"
#include <vector>
#include "FluidFoamModel.h"

using namespace SPH;
using namespace Eigen;

class SFB
{

	const double E_PI = 3.14159265358979323846;

private:
	FluidFoamModel* m_model;
	
	Real m_fluidParticleRadius = 0.0025;
	Real m_foamParticleRadius = 0.00025;
	Real FPS = 25.0;
	const Real particleMass = 1.0;
	Real tau_min_ta = 5;
	Real tau_max_ta = 20;
	Real tau_min_wc = 2;
	Real tau_max_wc = 8;
	Real tau_min_k = 5;
	Real tau_max_k = 50;
	Real vel_min = 1;
	Real vel_max = 10;
	Real h = 1;

	Real k_ta = 1;
	Real k_wc = 1;
	Real k_d = 0.5;
	Real k_b = 0.5;
	Real k = 1;

	Vector3r g = Vector3r(0, -9.81, 0);

	enum FoamType { SPRAY, FOAM, BUBBLE };

	struct ParticleObject
	{
		Vector3r m_x;
		Vector3r m_v;
		Real m_lifetime;
		FoamType m_type;
	};

	std::vector<Vector3r> m_x;
	std::vector<Vector3r> m_v;
	std::vector<Real> m_lifetime;
	std::vector<FoamType> m_type;

	std::vector<Vector3r> m_nFluid;
	
public:
	SFB(Real p_FPS = 25.0, Real p_radius = 0.025);
	~SFB();

	void initParameters(Real tau_min_ta, Real tau_max_ta, Real tau_min_wc,
		Real tau_max_wc, Real tau_min_k, Real tau_max_k, Real h,
		Real k_ta, Real k_wc, Real k_d, Real k_b);
	void initParameters(Real vel_min, Real vel_max, Real k, Real h, Real k_d, Real k_b);
	void simulateNextStep(Vector3r* x, Vector3r* v, unsigned int n);
	void simulateNextStep(Vector3r* x, Vector3r* v, Vector3r* p, unsigned int n);
	void simulateNextStepV2(Vector3r* x_fluid, Vector3r* v_fluid, unsigned int n_fluid);
	void computeNextFluidPotential(Vector3r* x, Vector3r* v, Vector3r* p, unsigned int n);
	void reset();

	

	std::vector<Vector3r> const& getCurrentFoamParticles();
	std::vector<Vector3r> const& getCurrentFoamVelocities();
	std::vector<Vector3r> const& getCurrentFluidNormals();


	Vector3r* getPositionData();
	Vector3r* getVelocityData();
	Vector3r* getFluidNormalsData();

	unsigned int numberOfParticles() const { return static_cast<unsigned int>(m_x.size()); }

private:
	void updateParticleLifetimes();
	void updateParticleVelocities(FluidFoamModel* p_fluid, Vector3r g, Real h, Real k_d, Real k_b);
	void updateParticlePositions();
	void updateParticleType(FluidFoamModel* p_fluid);
	void copyFluidNormals();

	void deleteParticlesOutOfRange();
	void deleteParticle(unsigned int i);
	void copyParticleFromTo(unsigned int i, unsigned int j);

	void performNeighbourhoodSearchSort(FluidFoamModel* p_model);

	Real phi(Real I, Real tau_min, Real tau_max);
	Real scaledVelocityDifference(Vector3r& x_i, Vector3r& v_i, std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, Real h);
	Real W(Vector3r& x_ij, Real h);
	Real surfaceCurvature(Vector3r& x_i, Vector3r&  n_i, std::vector<Vector3r>& x_j, std::vector<Vector3r>& n_j, Real h);
	Vector3r minmax(Vector3r x, Real min, Real max);
	std::vector<Vector3r> minmax(std::vector<Vector3r> x, Real min, Real max);

	Real trappedAirPotential(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
		std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
		Real tau_min, Real tau_max, Real h);
	Real waveCrestPotential(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
		std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
		Real tau_min, Real tau_max, Real h);
	Real kineticEnergyPotential(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
		std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
		Real tau_min, Real tau_max, Real h);
	Real velocityPotential(Vector3r& x_i, Vector3r& v_i,
		std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j,
		Real vel_min, Real vel_max, Real h);

	unsigned int computeNumberOfParticles(Vector3r& x_i, Vector3r& v_i, Vector3r& n_i,
		std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j, std::vector<Vector3r>& n_j,
		Real tau_min_ta, Real tau_max_ta, Real tau_min_wc, Real tau_max_wc, Real tau_min_k, Real tau_max_k,
		Real h, Real k_ta, Real k_wc);
	unsigned int computeNumberOfParticles(Vector3r& p_potentials,
		Real tau_min_ta, Real tau_max_ta, Real tau_min_wc, Real tau_max_wc, Real tau_min_k, Real tau_max_k,
		Real h, Real k_ta, Real k_wc);
	unsigned int computeNumberOfParticles(Vector3r& x_i, Vector3r v_i, std::vector<Vector3r>& x_j, std::vector<Vector3r>& v_j,
		Real vel_min, Real vel_max, Real k, Real h);

	void createParticles(
		FluidFoamModel* p_model,
		Real tau_min_ta, Real tau_max_ta, 
		Real tau_min_wc, Real tau_max_wc, 
		Real tau_min_k, Real tau_max_k,
		Real h, Real k_ta, Real k_wc);
	void createParticles(
		FluidFoamModel* p_model,
		Real vel_min, Real vel_max,
		Real h, Real k);
	void createParticles(
		FluidFoamModel* p_model,
		Vector3r* p_potential,
		Real tau_min_ta, Real tau_max_ta,
		Real tau_min_wc, Real tau_max_wc,
		Real tau_min_k, Real tau_max_k,
		Real h, Real k_ta, Real k_wc);

	void computePotentials(
		FluidFoamModel* p_model,
		Vector3r* p_positions,
		Vector3r* p_potentials,
		const unsigned int nFluid);


	SFB::ParticleObject createRandomParticle(Vector3r& x_i, Vector3r& v_i, int p_numberOfNeighbours);
	Vector3r computeAveragedLocalFluidVelocity(unsigned int d, FluidFoamModel* p_fluid, Real h);

	FORCE_INLINE void setFoamPosition(const unsigned int i, const Vector3r &pos) {
		this->m_x[i] = pos;
	}

	FORCE_INLINE Vector3r &getFoamPosition(const unsigned int i) {
		return this->m_x[i];
	}
};

#endif // ! __SprayFoamBubbles_h__


