#include "SPlisHSPlasH/Common.h"
#include <Eigen/Dense>
#include <iostream>
#include "Utilities/PartioReaderWriter.h"
#include "Utilities/OBJLoader.h"
#include "Utilities/StringTools.h"
#include "SPlisHSPlasH/Utilities/Timing.h"
#include "Utilities/FileSystem.h"
#include "SprayFoamBubbles\SFB\DataCollection\Evaluator.h"
#include <cfloat>

#include "Postprocessing\Foam\FoamManager.h"

// Enable memory leak detection
#ifdef _DEBUG
#ifndef EIGEN_ALIGN
	#define new DEBUG_NEW 
#endif
#endif

using namespace SPH;
using namespace Eigen;
using namespace std;

void partioExport(Vector3r* p_first, Vector3r* p_second, const unsigned int p_size, const Real p_radius);
void partioExport(unsigned int* p_id, Vector3r* p_first, Vector3r* p_second, const unsigned int p_size, const Real p_radius);
void evaluationExport(Vector3r* p, const unsigned int p_size);

string inputFile = "";
string exePath, dataPath;

std::vector<Vector3r> x;
std::vector<Vector3r> v;
std::vector<unsigned int> id;

FoamManager *foamManager;

//General
int currentParticleSet = 0;

//Rendering
Real particleRadiusFluid = 0.025;
Real particleRadiusFoam = 0.0025;

// main 
int main( int argc, char **argv )
{
	REPORT_MEMORY_LEAKS;

	if (argc < 2)
	{
		std::cerr << "Not enough parameters!\n";
		std::cerr << "Usage: SFBcmd.exe [path to particle folder]\n";
		return -1;
	}

	exePath = FileSystem::getProgramPath();
	dataPath = FileSystem::normalizePath(exePath + "/" + std::string(SPH_DATA_PATH));	

	for (int i = 1; i < argc; i++)
	{
		inputFile = argv[i];
	}


	std::string path = FileSystem::getProgramPath() + "/" + inputFile;

	std::string fluidPath = path + "/Fluid/";
	
	std::vector<std::string> fluidFileList = FileSystem::getAllFileNamesWithinFolder(fluidPath, "/*.bgeo");
	std::sort(fluidFileList.begin(), fluidFileList.end(), StringTools::compareNat);

	PartioReaderWriter::readParticles(fluidPath + fluidFileList[0], Vector3r::Zero(), Matrix3r::Identity(), 1.0, x, v, particleRadiusFluid, id);

	std::cout << "Use foam manager to compute foam data." << std::endl;
	foamManager = new FoamManager(2, particleRadiusFluid);
	foamManager->setExportFoam(true);
	foamManager->setExportMist(true);

	START_TIMING("SIMULATION");
	for (int i = 0; i < fluidFileList.size(); i++) { //change later to: fluidFileList.size()
		currentParticleSet = i;
		
		std::cout << "Processing particle set: " << i << " ..." << std::endl;

		x.clear();
		v.clear();
		id.clear();

		std::cout << "read particles.";
		PartioReaderWriter::readParticles(fluidPath + fluidFileList[i], Vector3r::Zero(), Matrix3r::Identity(), 1.0, x, v, particleRadiusFluid, id);
		foamManager->setFluidParticleSet(&id[0], &x[0], &v[0], (unsigned int) x.size());
		std::cout << " process one timestep.";
		foamManager->processNextTimestep();

		unsigned int n_foam = 0, n_mist = 0;
		std::vector<Vector3r> x_foam, v_foam, x_mist, v_mist;
		std::vector<unsigned int> id_foam;
		foamManager->getFoamParticleSet(id_foam, x_foam, v_foam, n_foam);
		foamManager->getMistParticleSet(x_mist, v_mist, n_mist);

		#pragma message ("WARNING: hack for debug mode.")
		if (n_foam == 0)
		{
			id_foam.push_back(0);
			x_foam.push_back(Vector3r(0, 0, 0));
			v_foam.push_back(Vector3r(0, 0, 0));
		}

		std::cout << " save particles.";
		START_TIMING("save data");
		foamManager->exportParticleSet();
		STOP_TIMING_AVG;

		std::cout << endl;
		std::cout << "Finished processing set: " << currentParticleSet << "." 
			<< " #fluid particles: " << x.size() 
			<< " #foam particles: " << n_foam 
			<< " #mist particles: " << n_mist << std::endl;
	
	}
	STOP_TIMING_AVG;

	std::cout << "### FINISHED ###" << std::endl << std::endl;

	Timing::printAverageTimes();
	Timing::printTimeSums();

	delete foamManager;

	return 0;
}

void partioExport(Vector3r* p_first, Vector3r* p_second, const unsigned int p_size, const Real p_radius) {
	std::string exportPath = FileSystem::normalizePath(FileSystem::getProgramPath() + "/PartioExport");
	FileSystem::makeDirs(exportPath);

	std::string fileName = "ParticleData";
	fileName = fileName + std::to_string(currentParticleSet+1) + ".bgeo";
	std::string exportFileName = FileSystem::normalizePath(exportPath + "/" + fileName);

	PartioReaderWriter::writeParticles(exportFileName, p_size, p_first, p_second, p_radius);
}

void partioExport(unsigned int* p_id, Vector3r* p_first, Vector3r* p_second, const unsigned int p_size, const Real p_radius) {
	std::string exportPath = FileSystem::normalizePath(FileSystem::getProgramPath() + "/PartioExport");
	FileSystem::makeDirs(exportPath);

	std::string fileName = "ParticleData";
	fileName = fileName + std::to_string(currentParticleSet + 1) + ".bgeo";
	std::string exportFileName = FileSystem::normalizePath(exportPath + "/" + fileName);

	PartioReaderWriter::writeParticles(exportFileName, p_size, p_first, p_second, p_radius, p_id);
}

void evaluationExport(Vector3r* p, const unsigned int p_size) {
	std::string exportPath = FileSystem::normalizePath(FileSystem::getProgramPath() + "/EvaluatorExport");
	FileSystem::makeDirs(exportPath);

	std::string fileName = "EvaluationData.txt";
	std::string exportFileName = FileSystem::normalizePath(exportPath + "/" + fileName);
	
	if (currentParticleSet == 0) {
		std::remove(exportFileName.c_str());
	}

	Evaluator::appendAllDataToFile(p, p_size, exportFileName);
}